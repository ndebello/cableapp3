//
//  CS1CVViewController.h
//  CableApp
//
//  Created by Matteo Centro on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CS1ViewController.h"

@interface CS1CVViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *currentField;
@property (weak, nonatomic) IBOutlet UITextField *voltageField;
@property (weak, nonatomic) IBOutlet UITextField *otherVoltageField;
@property (weak, nonatomic) CS1ViewController *mainvc;

@end
