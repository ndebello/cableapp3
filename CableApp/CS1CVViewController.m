//
//  CS1CVViewController.m
//  CableApp
//
//  Created by Matteo Centro on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "CS1CVViewController.h"

@interface CS1CVViewController (){
    UIAlertController *alertController;
}


@end

@implementation CS1CVViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.otherVoltageField.hidden = YES;
    
    // Do any additional setup after loading the view.
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([textField isEqual:self.otherVoltageField]){
        
    } else if ([textField isEqual:self.currentField]) {
        [self createUIAlertController:@[@"Alternata Trifase", @"Alternata Monofase", @"Continua"] TextField:textField TitleAlert:@"Select Current " MessageAlert:@""];
        [self.view endEditing:YES];
    } else if ([textField isEqual:self.voltageField]) {
        [self createUIAlertController:@[@"127", @"132", @"220", @"230", @"Other"] TextField:textField TitleAlert:@"Select Voltage " MessageAlert:@""];
        [self.view endEditing:YES];
    }
}

-(void)createUIAlertController:(NSArray *)arrayAlert TextField:(UITextField *)textField TitleAlert:(NSString *)titleAlert MessageAlert:(NSString *)messageAlert{
    
    alertController = [UIAlertController alertControllerWithTitle:titleAlert
                                                          message:messageAlert
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i= 0; i< [arrayAlert count]; i++) {
        [alertController addAction:[UIAlertAction
                                    actionWithTitle: [arrayAlert objectAtIndex:i]
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        textField.text = [arrayAlert objectAtIndex:i];
                                        if ([textField isEqual:self.voltageField]){
                                            if ([textField.text isEqualToString:@"Other"]){
                                                self.otherVoltageField.hidden = NO;
                                            } else {
                                                self.otherVoltageField.hidden = YES;
                                            }
                                        }

                                    }]];
    }
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Annulla", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)backPressed:(id)sender {
    if (self.otherVoltageField.text.length >0){
        self.mainvc.voltage = self.otherVoltageField.text;
    } else {
        if (self.voltageField.text.length>0){
            self.mainvc.voltage = self.voltageField.text;
        }
    }
    if (self.currentField.text.length >0){
        self.mainvc.current = self.currentField.text;
    }
    [self.navigationController popViewControllerAnimated:YES];
}
@end
