//
//  CS1ViewController.h
//  CableApp
//
//  Created by Matteo Centro on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CS1ViewController : UIViewController
@property (nonatomic, strong) NSDictionary *cable;
@property (weak, nonatomic) IBOutlet UILabel *cableNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *currentAndVoltageField;
@property (weak, nonatomic) IBOutlet UITextField *otherField;
@property (nonatomic, strong) NSString *current;
@property (nonatomic, strong) NSString *voltage;
@end
