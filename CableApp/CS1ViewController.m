//
//  CS1ViewController.m
//  CableApp
//
//  Created by Matteo Centro on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "CS1ViewController.h"
#import "CS1CVViewController.h"


@interface CS1ViewController (){
    UIAlertController *alertController;
    BOOL isMotor;
}

@end

@implementation CS1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.current = @"";
    self.voltage = @"";
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.cable){
        self.cableNameLabel.text = [self.cable objectForKey:@"commercial_name"];
    }
    if (self.current.length >0){
        self.currentAndVoltageField.text = [NSString stringWithFormat:@"%@ %@", self.current, self.voltage];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([textField isEqual:self.currentAndVoltageField]){
        [self performSegueWithIdentifier:@"selectVoltage" sender:self];
    } else {
        [self createUIAlertController:@[@"Single Core", @"Multi Core"] TextField:textField TitleAlert:@"Select Cores " MessageAlert:@""];
    }
    [self.view endEditing:YES];
}

-(void)createUIAlertController:(NSArray *)arrayAlert TextField:(UITextField *)textField TitleAlert:(NSString *)titleAlert MessageAlert:(NSString *)messageAlert{
    
    alertController = [UIAlertController alertControllerWithTitle:titleAlert
                                                          message:messageAlert
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i= 0; i< [arrayAlert count]; i++) {
        [alertController addAction:[UIAlertAction
                                    actionWithTitle: [arrayAlert objectAtIndex:i]
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        textField.text = [arrayAlert objectAtIndex:i];
                                    }]];
    }
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Annulla", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"selectVoltage"])
    {
        CS1CVViewController *vc = [segue destinationViewController];
        vc.mainvc = self;
        if (self.current && self.current.length >0){
            vc.currentField.text = self.current;
            vc.voltageField.text = self.voltage;
        }
    }
}


@end
