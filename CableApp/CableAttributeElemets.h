//
//  CableAttributeElemets.h
//  CableApp
//
//  Created by Welington Barichello on 29/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CableAttributeElemets : NSObject

@property(retain,readwrite,nonatomic) NSDictionary * genType;
@property(retain,readwrite,nonatomic) NSDictionary * voltageCass;
@property(retain,readwrite,nonatomic) NSDictionary * application;
@property(retain,readwrite,nonatomic) NSDictionary * instMeth;
@property(retain,readwrite,nonatomic) NSDictionary * localStr;
@property(retain,readwrite,nonatomic) NSDictionary * cprClass;

@end
