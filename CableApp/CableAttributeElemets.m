//
//  CableAttributeElemets.m
//  CableApp
//
//  Created by Welington Barichello on 29/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "CableAttributeElemets.h"

@implementation CableAttributeElemets


+ (id)sharedManager {
    static CableAttributeElemets *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        return self;
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}


@end
