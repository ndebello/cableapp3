//
//  CableDetailViewController.h
//  CableApp
//
//  Created by Matteo Centro on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <UIKit/UIKit.h>

@interface CableDetailViewController : UIViewController<MFMailComposeViewControllerDelegate>
@property (nonatomic, strong)NSDictionary *cable;
@end
