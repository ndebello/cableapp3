//
//  CablesListTypesViewController.h
//  CableA
//
//  Created by Riccardo Costacurta on 03/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CablesListTypesViewController : UIViewController

@property (nonatomic, strong) NSArray *results;

@property (strong, nonatomic) IBOutlet UITextView *resultTxt;

@end
