//
//  CablesListTypesViewController.m
//  CableA
//
//  Created by Riccardo Costacurta on 03/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "CablesListTypesViewController.h"

@interface CablesListTypesViewController ()
// @property (nonatomic, strong) NSDictionary *results;

@end

@implementation CablesListTypesViewController


- (void)viewDidLoad
{
    NSMutableString *myText = [NSMutableString stringWithCapacity:1000];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    NSLog(@"%@",self.results);
    
    NSDictionary *myDic = self.results[0];
    
    // Verify object retrieved is dictionary
    if ([myDic isKindOfClass:[NSDictionary class]])
    {
        // To print out all key-value pairs in the NSDictionary myDict
        for (id object in myDic)
        {
            // do something with object
            NSLog(@"CABLES LIST TYPES1 %@", [myDic objectForKey:object]);
            [myText appendFormat:@"CABLES LIST TYPES2\n%@", [myDic objectForKey:object]];
        }
    }

    self.resultTxt.text = myText;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


@end
