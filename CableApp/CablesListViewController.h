//
//  CablesListViewController.h
//  CableApp
//
//  Created by Matteo Centro on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CablesListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
-(void)parseJson:(NSData *)data;
@end
