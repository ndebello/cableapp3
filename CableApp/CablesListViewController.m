//
//  CablesListViewController.m
//  CableApp
//
//  Created by Matteo Centro on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "CablesListViewController.h"
#import "CableDetailViewController.h"
#import "DataSelected.h"

@interface CablesListViewController (){
    DataSelected * sharedData;
}
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (nonatomic, strong) NSMutableArray * results;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (weak, nonatomic) IBOutlet UIView *panelBestSolution;
@property (weak, nonatomic) IBOutlet UIView *bkgBestSolution;
@property (weak, nonatomic) IBOutlet UIImageView *imgBestSolution;
@property (weak, nonatomic) IBOutlet UILabel *titleBestSolution;
@property (weak, nonatomic) IBOutlet UILabel *labelBestSoltuion;

@end

@implementation CablesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Cables Search";
    sharedData = [DataSelected sharedManager];
    [self.bkgBestSolution.layer setCornerRadius:7.0];
    
    // Do any additional setup after loading the view.
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!self.results){
        [self performSearch];
    }
}
- (IBAction)segmentDidChange:(id)sender {
    [self.tableView reloadData];
}

-(void)performSearch{
    NSString *urlTemplate = @"http://46.252.150.235:8085/api/cables/get/installation1_id:%@/installation2_id:%@/installation3_id:%@.json";
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:urlTemplate, sharedData.s1, sharedData.s2, sharedData.s3]];
    NSLog(@"CABLES: %@",url);
    
    //WOLF qua non viene giu' sempre tutto quindi hardcodo la url
    //url = [NSURL URLWithString:@"http://46.252.150.235:8085/api/cables/get/installation1_id:14/installation2_id:5/installation3_id:8.json"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             NSLog(@"Error,%@", [error localizedDescription]);
         }
         else
         {
             [self parseJson:data];
         }
     }];

}

-(void)parseJson:(NSData *)data{
    
    NSError* error;
    NSArray* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          options:kNilOptions
                          error:&error];
    
    for (NSDictionary * dict in json) {
        if (!self.results){
            self.results = [NSMutableArray new];
            sharedData.Cables = [NSMutableArray new];
        }
        [self.results addObject:[dict objectForKey:@"Cable"]];
        [sharedData.Cables addObject:[dict objectForKey:@"Cable"]];
    }
    [self.tableView performSelectorOnMainThread:@selector(reloadData)
                                     withObject:nil
                                  waitUntilDone:NO];
}

#pragma mark - Table view data source

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.results)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.results)
    {
        if (self.segmentedControl.selectedSegmentIndex==0)
        {
            self.tableView.hidden = YES;
            return 1;
        } else {
            self.tableView.hidden = NO;
            return self.results.count;
        }
    }
    else
    {
        return 0;
    }
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CableCell" forIndexPath:indexPath];
    NSDictionary * cable = [self.results objectAtIndex:indexPath.row];
    UILabel * nameLabel = (UILabel *) [cell viewWithTag:10];
    UILabel * descLabel = (UILabel *) [cell viewWithTag:11];
    UIImageView * imgCable  = (UIImageView *) [cell viewWithTag:12];
    
    //setting Style
    [imgCable.layer setBorderColor: [[self colorWithHexString:@"DE0B7E"] CGColor]];
    [imgCable.layer setBorderWidth: 1.0];
    [imgCable.layer setCornerRadius: 2.0];
    //*******
    
    
    nameLabel.text = [cable objectForKey:@"commercial_name"];
    descLabel.text = [cable objectForKey:@"description"];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [cable objectForKey:@"image_url"]]];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (imgCable)
                        imgCable.image = image;
                    if (indexPath.row ==0) {
                         self.imgBestSolution.image= imgCable.image;
                    }
                });
            }
        }
    }];
    if (indexPath.row ==0) {
        self.titleBestSolution.text = nameLabel.text;
        self.labelBestSoltuion.text = descLabel.text;
    }
    [task resume];
    
        
    return cell;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"simpleDetail"]){
        
    sharedData.CS = [self.results objectAtIndex:[self.tableView indexPathForCell:sender].row];
    
    CableDetailViewController * vc = [segue destinationViewController];
    
    vc.cable = sharedData.CS;
        
    }
}
-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (IBAction)selectedBestSolution:(id)sender {
    [self methodRequest];
}

- (void) methodRequest{
    [_activity startAnimating];
    sharedData.CS = [self.results objectAtIndex:0];
    if ([sharedData.Methods count] == 0 )
    {
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"http://46.252.150.235:8085/api/methods/index/installation1_id:%@/installation2_id:%@/installation3_id:%@/cable_id:%@.json", sharedData.s1, sharedData.s2, sharedData.s3, [sharedData.CS objectForKey:@"id"]]];
        NSLog(@"URL %@", url);
        NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
        NSOperationQueue * queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse * response, NSData * data, NSError * error)
         {
             if (error)
             {
                 NSLog(@"Error,%@", [error localizedDescription]);
             }
             else
             {
                 [self ParsingJsonmethodRequest:data];
             }
         }];
        
    }
}

- (void) ParsingJsonmethodRequest:(NSData *)data
{
    NSError * error;
    NSDictionary * json = [NSJSONSerialization
                           JSONObjectWithData:data // 1
                           options:kNilOptions
                           error:&error];
    
    for (NSDictionary * inst1 in json)
    {
        NSDictionary * method = [inst1 objectForKey:@"Method"];
        if ([method objectForKey:@"id"])
        {
            NSDictionary * p = @{
                                 [[inst1 objectForKey:@"Method"] objectForKey:@"id"]: [[inst1 objectForKey:@"Method"] objectForKey:@"code"]
                                 };
            [sharedData.Methods addObject:p];
        }
        else
        {
            NSLog(@"NESSUN METODO ARRIVA DAL WEBSERVICE");
        }
    }
    
    int quadr[500][7] = { 1,1,14,5,8,1,0,
        2,2,14,10,0,2,0,
        3,3,15,1,4,1,0,
        4,4,15,6,0,1,0,
        5,5,1,1,1,3,0,
        6,6,1,1,1,2,0,
        7,7,1,1,2,3,0,
        8,8,1,1,2,2,0,
        9,7,1,1,3,3,0,
        10,8,1,1,3,2,0,
        11,3,1,1,4,3,0,
        12,8,1,1,5,3,0,
        13,8,1,7,9,1,0,
        14,7,1,7,10,1,0,
        15,5,2,1,1,3,0,
        16,6,2,1,1,2,0,
        17,7,2,1,2,3,0,
        18,8,2,1,2,2,0,
        19,7,2,1,3,3,0,
        20,8,2,1,3,2,0,
        21,3,2,1,4,1,0,
        22,8,2,1,5,1,0,
        23,8,2,7,9,1,0,
        24,7,2,7,10,1,0,
        25,5,3,1,1,3,0,
        26,6,3,1,1,2,0,
        27,7,3,1,2,3,0,
        28,8,3,1,2,2,0,
        29,7,3,1,3,3,0,
        30,8,3,1,3,2,0,
        31,3,3,1,4,1,0,
        32,8,3,1,5,1,0,
        33,5,3,4,6,2,0,
        34,1,3,4,7,1,0,
        35,1,3,5,8,1,0,
        36,4,3,6,0,1,0,
        37,8,3,7,9,1,0,
        38,7,3,7,10,1,0,
        39,2,3,8,11,3,0,
        40,9,3,8,11,2,0,
        41,2,3,8,12,3,0,
        42,9,3,8,12,2,0,
        43,1,3,8,13,1,0,
        44,2,3,10,0,2,0,
        45,10,3,11,0,3,0,
        46,5,3,12,0,1,0,
        47,5,4,1,1,3,0,
        48,6,4,1,1,2,0,
        49,7,4,1,2,3,0,
        50,8,4,1,2,2,0,
        51,7,4,1,3,3,0,
        52,8,4,1,3,2,0,
        53,5,5,1,1,3,0,
        54,6,5,1,1,2,0,
        55,7,5,1,2,3,0,
        56,8,5,1,2,2,0,
        57,7,5,1,3,3,0,
        58,8,5,1,3,2,0,
        59,8,5,1,5,1,0,
        60,1,5,5,8,1,0,
        61,8,5,7,9,1,0,
        62,7,5,7,10,1,0,
        63,2,5,8,11,3,0,
        64,9,5,8,11,2,0,
        65,2,5,8,12,3,0,
        66,9,5,8,12,2,0,
        67,1,5,8,13,1,0,
        68,5,6,1,1,3,0,
        69,6,6,1,1,2,0,
        70,7,6,1,2,3,0,
        71,8,6,1,2,2,0,
        72,7,6,1,3,3,0,
        73,8,6,1,3,2,0,
        74,3,6,1,4,1,0,
        75,8,6,1,5,1,0,
        76,5,6,4,6,2,0,
        77,1,6,4,7,1,0,
        78,1,6,5,8,1,0,
        79,4,6,6,0,1,0,
        80,8,6,7,9,1,0,
        81,7,6,7,10,1,0,
        82,2,6,8,11,3,0,
        83,9,6,8,11,2,0,
        84,2,6,8,12,3,0,
        85,9,6,8,12,2,0,
        86,1,6,8,13,1,0,
        87,8,6,9,14,1,0,
        88,7,6,9,15,1,0,
        89,5,7,2,1,3,0,
        90,6,7,2,1,2,0,
        91,7,7,2,2,3,0,
        92,8,7,2,2,2,0,
        93,7,7,2,3,3,0,
        94,8,7,2,3,2,0,
        95,8,7,2,5,1,0,
        96,7,7,3,0,3,0,
        97,8,7,3,0,2,0,
        98,1,7,5,8,1,0,
        99,8,7,7,9,1,0,
        100,7,7,7,10,1,0,
        101,5,8,2,1,3,0,
        102,6,8,2,1,2,0,
        103,7,8,2,2,3,0,
        104,8,8,2,2,2,0,
        105,7,8,2,3,3,0,
        106,8,8,2,3,2,0,
        107,8,8,2,5,1,0,
        108,7,8,3,0,3,0,
        109,8,8,3,0,2,0,
        110,5,9,2,1,3,0,
        111,6,9,2,1,2,0,
        112,7,9,2,2,3,0,
        113,8,9,2,2,2,0,
        114,7,9,2,3,3,0,
        115,8,9,2,3,2,0,
        116,8,9,2,5,1,0,
        117,8,9,2,5,2,0,
        118,7,9,3,0,3,0,
        119,8,9,3,0,2,0,
        120,5,10,1,1,3,0,
        121,6,10,1,1,2,0,
        122,7,10,1,2,3,0,
        123,8,10,1,2,2,0,
        124,7,10,1,3,3,0,
        125,8,10,1,3,2,0,
        126,3,10,1,4,1,0,
        127,8,10,1,5,1,0,
        128,5,10,4,6,2,0,
        129,1,10,4,7,1,0,
        130,1,10,5,8,1,0,
        131,4,10,6,0,1,0,
        132,8,10,7,9,1,0,
        133,7,10,7,10,1,0,
        134,2,10,8,11,3,0,
        135,9,10,8,11,2,0,
        136,2,10,8,12,3,0,
        137,9,10,8,12,2,0,
        138,1,10,8,13,1,0,
        139,2,10,10,0,2,0,
        140,5,11,1,1,3,0,
        141,6,11,1,1,2,0,
        142,7,11,1,2,3,0,
        143,8,11,1,2,2,0,
        144,7,11,1,3,3,0,
        145,8,11,1,3,2,0,
        146,3,11,1,4,1,0,
        147,8,11,1,5,1,0,
        148,1,11,5,8,1,0,
        149,5,13,1,1,3,0,
        150,6,13,1,1,2,0,
        151,7,13,1,2,3,0,
        152,8,13,1,2,2,0,
        153,7,13,1,3,3,0,
        154,8,13,1,3,2,0,
        155,3,13,1,4,1,0,
        156,8,13,1,5,1,0,
        157,5,13,4,6,2,0,
        158,1,13,4,7,1,0,
        159,1,13,5,8,1,0,
        160,5,12,1,1,3,0,
        161,6,12,1,1,2,0,
        162,7,12,1,2,3,0,
        163,8,12,1,2,2,0,
        164,7,12,1,3,3,0,
        165,8,12,1,3,2,0,
        166,3,12,1,4,1,0,
        167,8,12,1,5,1,0,
        168,3,1,1,4,2,0,
        169,8,1,1,5,2,0,
        170,3,1,1,4,1,0,
        171,8,1,1,5,1,0,
        172,8,1,7,9,2,0,
        173,8,1,7,9,3,0,
        174,7,1,7,10,2,0,
        175,7,1,7,10,3,0,
        176,3,2,1,4,2,0,
        177,3,2,1,4,3,0,
        178,8,2,1,5,3,0,
        179,8,2,1,5,2,0,
        180,8,2,7,9,2,0,
        181,8,2,7,9,3,0,
        182,7,2,7,10,3,0,
        183,7,2,7,10,2,0,
        184,3,3,1,4,3,0,
        185,3,3,1,4,2,0,
        186,8,3,1,5,3,0,
        187,8,3,1,5,2,0,
        188,5,3,12,0,3,0,
        189,5,3,12,0,2,0,
        190,1,3,4,7,3,0,
        191,1,3,4,7,2,0,
        192,1,3,5,8,2,0,
        193,1,3,5,8,3,0,
        194,4,3,6,0,2,0,
        195,4,3,6,0,3,0,
        196,8,3,7,9,2,0,
        197,8,3,7,9,3,0,
        198,7,3,7,10,2,0,
        199,7,3,7,10,3,0,
        200,1,3,8,13,2,0,
        201,1,3,8,13,3,0,
        202,8,5,1,5,2,0,
        203,8,5,1,5,3,0,
        204,1,5,5,8,2,0,
        205,1,5,5,8,3,0,
        206,8,5,7,9,2,0,
        207,8,5,7,9,3,0,
        208,7,5,7,10,2,0,
        209,7,5,7,10,3,0,
        210,1,5,8,13,2,0,
        211,1,5,8,13,3,0,
        212,3,6,1,4,2,0,
        213,3,6,1,4,3,0,
        214,8,6,1,5,2,0,
        215,8,6,1,5,3,0,
        216,1,6,4,7,2,0,
        217,1,6,4,7,3,0,
        218,1,6,5,8,2,0,
        219,1,6,5,8,3,0,
        220,4,6,6,0,2,0,
        221,4,6,6,0,3,0,
        222,8,6,7,9,2,0,
        223,8,6,7,9,3,0,
        224,7,6,7,10,2,0,
        225,7,6,7,10,3,0,
        226,1,6,8,13,2,0,
        227,1,6,8,13,3,0,
        228,8,6,9,14,2,0,
        229,8,6,9,14,3,0,
        230,7,6,9,15,3,0,
        231,7,6,9,15,2,0,
        232,8,7,2,5,2,0,
        233,8,7,2,5,3,0,
        234,1,7,5,8,3,0,
        235,1,7,5,8,2,0,
        236,8,7,7,9,2,0,
        237,8,7,7,9,3,0,
        238,7,7,7,10,3,0,
        239,7,7,7,10,2,0,
        240,8,8,2,5,2,0,
        241,8,8,2,5,3,0,
        242,8,9,2,5,3,0,
        243,3,10,1,4,2,0,
        244,3,10,1,4,3,0,
        245,8,10,1,5,3,0,
        246,8,10,1,5,2,0,
        247,1,10,4,7,2,0,
        248,1,10,4,7,3,0,
        249,1,10,5,8,3,0,
        250,1,10,5,8,2,0,
        251,4,10,6,0,2,0,
        252,4,10,6,0,3,0,
        253,8,10,7,9,2,0,
        254,8,10,7,9,3,0,
        255,7,10,7,10,3,0,
        256,7,10,7,10,2,0,
        257,1,10,8,13,2,0,
        258,1,10,8,13,3,0,
        259,3,11,1,4,2,0,
        260,3,11,1,4,3,0,
        261,8,11,1,5,3,0,
        262,8,11,1,5,2,0,
        263,1,11,5,8,2,0,
        264,1,11,5,8,3,0,
        265,3,13,1,4,2,0,
        266,3,13,1,4,3,0,
        267,3,12,1,4,2,0,
        268,3,12,1,4,3,0,
        269,8,12,1,5,3,0,
        270,8,12,1,5,2,0,
        271,5,12,12,0,2,0,
        272,5,12,12,0,3,0,
        273,1,12,4,7,3,0,
        274,1,12,4,7,2,0,
        275,1,12,5,8,2,0,
        276,1,12,5,8,3,0,
        277,4,12,6,0,3,0,
        278,4,12,6,0,2,0,
        279,8,12,7,9,2,0,
        280,8,12,7,9,3,0,
        281,7,12,7,10,3,0,
        282,7,12,7,10,2,0,
        283,1,12,8,13,2,0,
        284,1,12,8,13,3,0,
        285,5,12,12,0,1,0,
        286,5,12,4,6,2,0,
        287,1,12,4,7,1,0,
        288,1,12,5,8,1,0,
        289,4,12,6,0,1,0,
        290,8,12,7,9,1,0,
        291,7,12,7,10,1,0,
        292,9,12,8,11,2,0,
        293,2,12,8,11,3,0,
        294,2,12,8,12,3,0,
        295,9,12,8,12,2,0,
        296,1,12,8,13,1,0,
        297,2,12,10,0,2,0,
        298,10,12,11,0,3,0,
        299,8,13,1,5,2,0,
        300,8,13,1,5,3,0,
        301,1,13,4,7,2,0,
        302,1,13,4,7,3,0,
        303,1,13,5,8,3,0,
        304,1,13,5,8,2,0,
        305,1,14,5,8,2,0,
        306,1,14,5,8,3,0,
        307,3,15,1,4,3,0,
        308,3,15,1,4,2,0,
        309,4,15,6,0,2,0,
        310,4,15,6,0,3,0,
        311,11,16,1,4,2,0,
        312,12,16,5,8,2,0,
        313,13,16,6,0,2,0,
        314,12,16,8,11,2,0,
        315,12,16,8,12,2,0,
        316,12,16,8,13,2,0,
        317,1,14,5,16,1,0,
        318,1,14,5,16,2,0,
        319,1,14,5,16,3,0,
        320,1,3,5,16,1,0,
        321,1,3,5,16,2,0,
        322,1,3,5,16,3,0,
        323,1,5,5,16,1,0,
        324,1,5,5,16,2,0,
        325,1,5,5,16,3,0,
        326,1,6,5,16,3,0,
        327,1,6,5,16,2,0,
        328,1,6,5,16,1,0,
        329,1,7,5,16,1,0,
        330,1,7,5,16,2,0,
        331,1,7,5,16,3,0,
        332,1,10,5,16,1,0,
        333,1,10,5,16,2,0,
        334,1,10,5,16,3,0,
        335,1,11,5,16,1,0,
        336,1,11,5,16,2,0,
        337,1,11,5,16,3,0,
        338,1,12,5,16,1,0,
        339,1,12,5,16,2,0,
        340,1,12,5,16,3,0,
        341,1,13,5,16,1,0,
        342,1,13,5,16,2,0,
        343,1,13,5,16,3,0
    };
    NSDictionary * aMethod1;
    NSDictionary * aMethod2;
    unsigned long myCount;
    NSString * m1;
    NSString * m2;
    NSString * myMethod;
    int myIndex1, myIndex2, myQuadr1, myQuadr2, myNumCores1, myNumCores2;
    
    NSLog(@"VERIFICARE CALCOLO sMetINT");
    NSLog(@"CS è:%@", sharedData.CS);
    NSLog(@"Methods contiene: %@", sharedData.Methods);
    aMethod1 = [sharedData.Methods objectAtIndex:0];
    if ((myCount = [sharedData.Methods count]) > 1) aMethod2 = [sharedData.Methods objectAtIndex:1];
    
    m1 = [aMethod1.allKeys objectAtIndex:0];
    if (myCount > 1) m2 = [aMethod2.allKeys objectAtIndex:0];
    
    myQuadr1=myNumCores1=myIndex1=0;
    for(int i=0;i<450;i++)
    {
        if (([sharedData.s1 intValue] == quadr[i][2]) && ([sharedData.s2 intValue] == quadr[i][3]) && ([sharedData.s3 intValue] == quadr[i][4]) && ([m1 intValue] == quadr[i][1]))
        {
            myIndex1 = i;
            myQuadr1 = quadr[i][0];
            myNumCores1 = quadr[i][5];
            break;
        }
    }
    
    myQuadr2=myNumCores2=myIndex2=0;
    if (myCount > 1) for(int i=0;i<450;i++)
    {
        if (([sharedData.s1 intValue] == quadr[i][2]) && ([sharedData.s2 intValue] == quadr[i][3]) && ([sharedData.s3 intValue] == quadr[i][4]) && ([m2 intValue] == quadr[i][1]))
        {
            myIndex2 = i;
            myQuadr2 = quadr[i][0];
            myNumCores2 = quadr[i][5];
            break;
        }
    }
    
    if (myCount == 1)
    {
        if (myNumCores1 == 1)      sharedData.sNumCores = [NSString stringWithFormat: @"Both"];        // Both - Vedo dai dati che può essere BOTH, quindi lo lascio uguale, anche se mi puzza
        else if (myNumCores1 == 2) sharedData.sNumCores = [NSString stringWithFormat: @"Multi"];       // Multi - Lascio uguale
        else if (myNumCores1 == 3) sharedData.sNumCores = [NSString stringWithFormat: @"Single"];      // Single - Lascio uguale
        myMethod = m1;
    }
    else if (myCount == 2)
    {
        if (myNumCores1 == 1)      sharedData.sNumCores = [NSString stringWithFormat: @"Both"];        // Both - Vedo dai dati che può essere BOTH, quindi lo lascio uguale, anche se mi puzza
        else if (myNumCores1 == 2) sharedData.sNumCores = [NSString stringWithFormat: @"Multi"];       // Multi - Lascio uguale
        else if (myNumCores1 == 3) sharedData.sNumCores = [NSString stringWithFormat: @"Single"];      // Single - Lascio uguale
        
        if (myNumCores2 == 1)      sharedData.sNumCores = [NSString stringWithFormat: @"Both"];                 // Both - Lo lascio uguale, anche se mi puzza. COmunque se count è 2, sono venute fuori due sclelte, quindi diciamo Ok
        else if (myNumCores2 == 2) sharedData.sNumCores = [NSString stringWithFormat: @"%@-Multi",sharedData.sNumCores];  // Multi - Se count è 2, che il secondo sia Single o Multi poco importa, sempre BOTH devo mettere
        else if (myNumCores2 == 3) sharedData.sNumCores = [NSString stringWithFormat: @"%@-Single",sharedData.sNumCores]; // Single - Se count è 2, che il secondo sia Single o Multi poco importa, sempre BOTH devo mettere
        myMethod = [NSString stringWithFormat: @"%@-%@", m1,m2];
    }
    else
    {
        NSLog(@"ERRORE NUM CORES!!!!!!!!!!!!!!!!!!!! myCount=%d myIndex1=%d myQuadr1=%d myNumCores1=%d myIndex2=%d myQuadr2=%d myNumCores2=%d\n",myCount,myIndex1,myQuadr1,myNumCores1,myIndex2,myQuadr2,myNumCores2);
    }
    
    sharedData.sMetINT = myMethod; // DEN - E' tornata "normale", non più un metodo come aveva fatto WLF. La convenzione è che sia nella forma "ID1-ID2" (o ovviamente "ID" se ce ne è uno solo)
    dispatch_async(dispatch_get_main_queue(), ^{
        [_activity stopAnimating];
        [self performSegueWithIdentifier:@"calcCompost" sender:self];
    });
    
}

@end
