//
//  Calculator.h
//  CableApp
//
//  Created by Matteo Centro on 14/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculator : NSObject
@property (nonatomic, retain) NSString *installationMethod;
@property (nonatomic, retain) NSString *voltage;
@property (nonatomic, retain) NSString *tc;
@property (nonatomic, retain) NSString *cr;
@property (nonatomic, retain) NSString *current;
@property (nonatomic, retain) NSString *cosy;
@property (nonatomic, retain) NSString *vd;

@property (nonatomic, retain) NSString *insulation;
@property (nonatomic, retain) NSString *cond;
@property (nonatomic, retain) NSString *conductivity;


-(int)calculate;
@end
