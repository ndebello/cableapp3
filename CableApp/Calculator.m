//
//  Calculator.m
//  CableApp
//
//  Created by Matteo Centro on 14/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "Calculator.h"
#import "DataSelected.h"
//
//  main.c
//  algo
//
//  Created by Nicola De Bello on 15/01/16.
//  Copyright © 2016 WaRE'S ME srl. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CSV_FILES   34
#define MAX_TAB_RECORD  128

char  *meth = {"A1"};
float volt = 1.5;
char  *tc;                // Non so cosa sia
int   numCores = 3;       // 1=Both 2=Multicore 3=Singlecore
float current = 1.5;
float cosfi = 0.9;
char  *insul = {"PVC"};
char  *conductor = {"C"};
float conductivity = 1.5;
float tcs1 = 1.5;
float tcs2 = 1.5;
float tcs3 = 1.5;
float lcs1 = 1.5;
float lcs2 = 1.5;

float temp = 25;
float resist = 2.5;

int   sunExpo = 0;

int   inst1;
int   inst2;
int   inst3;

char  *numConductPerCable = {"2"}; // Non ho capito cosa sia, ultima riga di pag 12
int   numCablesConduit = 1;
float distBetwCables = 0.2;
int   circuitPerConduit = 1;
int   numLayers;
int   numTrays;
int   position; // Possible values: horizontal=1 vertical=2 not_in_line=3
float depth;    // Possible values: 0,5, 0,6, 0,8, 1 (default), 1,25, 1,5, 1,75, 2, 2,5, 3
float tcf=1;
float rcf=1;
float ccf=1;
float lcf=1;
float gcf=1;
float scf=1;
float fcf=1;
float cit=1;
float crt=1;
float oc=1;

float sez;

char tabNames[MAX_CSV_FILES+1][80] = {"Pag2_RCF_MethD1.csv",
    "Pag2_RCF_MethD2.csv",
    "Pag2_TCF_MethD1D2_PVC.csv",
    "Pag2_TCF_MethD1D2_XLPE.csv",
    "Pag3_Table_B_52_18.csv",
    "Pag3_Table_B_52_19.csv",
    "Pag3_Table_B_52_20.csv",
    "Pag3_TCF_MethOthers_PVC.csv",
    "Pag3_TCF_MethOthers_XLPE.csv",
    "Pag4_SunExposition.csv",
    "Pag9_GCF_SingleCore.csv",
    "Pag9_LCF_Inst2Layer.csv",
    "Pag10_RCF_MethH.csv",
    "Pag10_RCF_MethI.csv",
    "Pag10_TCF_MethHI_HEPR.csv",
    "Pag10_TCF_MethHI_XLPE.csv",
    "Pag11_CCF_MethH.csv",
    "Pag11_CCF_MethI.csv",
    "Pag11_DCF_MethHI.csv",
    "Pag12_Tab1.csv",
    "Pag12_Tab10.csv",
    "Pag12_Tab2.csv",
    "Pag12_Tab3.csv",
    "Pag12_Tab4.csv",
    "Pag12_Tab5.csv",
    "Pag12_Tab6.csv",
    "Pag12_Tab7.csv",
    "Pag12_Tab8.csv",
    "Pag12_Tab9.csv",
    "Pag12_TCF_MethJ_HEPR.csv",
    "Pag12_TCF_MethJ_XLPE.csv",
    "Pag13_TableCalcSez1.csv",
    "Pag13_TableCalcSez2_Copper.csv",
    "Pag13_TableCalcSez3_Alumin.csv"};

char mat[MAX_CSV_FILES+1][512][128][128];

const char* getfield(char* line, int num)
{
    const char* tok;
    for (tok = strtok(line, ";");
         tok && *tok;
         tok = strtok(NULL, ";\n"))
    {
        if (!--num)
            return tok;
    }
    return NULL;
}

int searchTable(char *tabName)
{
    int i;
    
    for (i=0;i<MAX_CSV_FILES;i++)
    {
        if (strcmp(tabName,tabNames[i]) == 0) break;
    }
    return(i);
}
@implementation Calculator

/***********************************************************************************************
 *
 *    Main
 *
 **********************************************************************************************/
-(int) calculate
{
    int i,j;
//    char myPath[1024];
    char myChar;
    int cfTab;
//    char *cc;
    
    //**** importante per prendere i dati***//
//    NSString *tempString = [[DataSelected sharedManager]sTemp];
//    temp = [tempString floatValue];
    
    NSString *mainPath = [[NSBundle mainBundle] resourcePath];
    
    char* basePath = strcat([mainPath cStringUsingEncoding:NSUTF8StringEncoding], "/");
    
    for(i=0;i<MAX_CSV_FILES;i++)
    {
        FILE *fstream;
        char buffer[1024] ;
        char *record,*line;
        int ii=0,jj=0;
        char aux[1024];
        //        char filepath[1024];
        
        sprintf(aux,"%s%s",basePath,tabNames[i]);
        fstream = fopen(aux ,"r");
        if (fstream == NULL)
        {
            printf("File opening failed %s\n",aux);
            return -1 ;
        }
        printf("File opening OK %s\n",aux);
        
        ii=jj=0;
        while((line=fgets(buffer,sizeof(buffer),fstream))!=NULL)
        {
            printf("Record %d %d %s %s\n",i,ii,buffer,line);
            record = strtok(line,";");
            jj=0;
            while(record != NULL)
            {
                char aux[1024];
                strcpy (aux,&record[1]);
                aux[strlen(aux)-1]='\0';
                strcpy(record,aux);
                printf("Record field: %s %s",aux,record) ;    //here you can put the record into the array as per your requirement.
                strcpy(mat[i][ii][jj++], record);
                record = strtok(NULL,";");
            }
            ++ii ;
        }
        fclose(fstream);
    }
    //=================================================
    // Inizio Step 4 del Doc "DRAFT FlowChart"
    //=================================================
    
    // La sua gestione dei concetti di if-then-else e di uniformità di scrittura è inquietante - Beware!
    if ((strcmp(meth,"D1") == 0) || (strcmp(meth,"D2") == 0))
    {
        // Asks Tenperature and Resistivity
        
        if (strcmp(insul,"PVC") == 0)
        {
            cfTab = searchTable("Pag2_TCF_MethD1D2_PVC.csv");
            for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe saltare i nomi dei campi
            {
                if (temp == strtod(mat[cfTab][i][0],NULL)) { tcf = strtof(mat[cfTab][i][1],NULL); break; }
            }
            printf("TabIndex = %d  Index = %d  TCF = %f\n",cfTab,i,tcf);
        }
        else if (strcmp(insul,"XLPE") == 0)
        {
            cfTab = searchTable("Pag2_TCF_MethD1D2_XLPE.csv");
            for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe saltare i nomi dei campi
            {
                if (temp == strtod(mat[cfTab][i][0],NULL)) { tcf = strtof(mat[cfTab][i][1],NULL); break; }
            }
            printf("TabIndex = %d  Index = %d  TCF = %f\n",cfTab,i,tcf);
        }
        if (strcmp(meth,"D1") == 0)
        {
            cfTab = searchTable("Pag2_RCF_MethD1.csv");
            for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe saltare i nomi dei campi
            {
                if (resist == strtod(mat[cfTab][i][0],NULL)) { rcf = strtof(mat[cfTab][i][1],NULL); break; }
            }
            printf("TabIndex = %d  Index = %d  RCF = %f\n",cfTab,i,rcf);
        }
        if (strcmp(meth,"D2") == 0)
        {
            cfTab = searchTable("Pag2_RCF_MethD2.csv");
            for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe saltare i nomi dei campi
            {
                if (resist == strtod(mat[cfTab][i][0],NULL)) { rcf = strtof(mat[cfTab][i][1],NULL); break; }
            }
            printf("TabIndex = %d  Index = %d  RCF = %f\n",cfTab,i,rcf);
        }
        if (strcmp(meth,"D1") == 0)
        {
            // Asks Number of Cables in Conduit and Distance between Cables, and uses Number of Cores
            if (numCablesConduit >1)
            {
                if ((numCores==3) || ((numCores==2)&&(circuitPerConduit==1)))
                {
                    // ccf dalla table b 52 19 part A
                    ccf=1;
                }
                else if ((numCores==2) && (circuitPerConduit==1))
                {
                    // ccf dalla table b 52 19 part B
                    ccf=1;
                }
            }
            else ccf=1;
            
        }
        else if (strcmp(meth,"D2") == 0) // Qua lei ha messo solo else, chissà che cazzo perchè
        {
            // Asks Number of Cables in Conduit and Distance between Cables
            if (numCablesConduit >1)
            {
                // ccf dalla table b 52 18
                ccf=1;
            }
            else ccf=1;
        }
    }
    else if ((strcmp(meth,"A1") == 0) || (strcmp(meth,"A2") == 0) || (strcmp(meth,"B1") == 0) || (strcmp(meth,"B2") == 0) || (strcmp(meth,"C") == 0) || (strcmp(meth,"E") == 0) || (strcmp(meth,"F") == 0) || (strcmp(meth,"G") == 0))
    {
        // Asks Tenperature and Resistivity
        
        if (strcmp(insul,"PVC") == 0)
        {
            cfTab = searchTable("Pag3_TCF_MethOthers_PVC.csv");
            for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe saltare i nomi dei campi
            {
                if (temp == strtod(mat[cfTab][i][0],NULL)) { tcf = strtof(mat[cfTab][i][1],NULL); break; }
            }
            printf("TabIndex = %d  Index = %d  TCF = %f\n",cfTab,i,tcf);
        }
        else if (strcmp(insul,"XLPE") == 0)
        {
            cfTab = searchTable("Pag3_TCF_MethOthers_PVC.csv");
            for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe saltare i nomi dei campi
            {
                if (temp == strtod(mat[cfTab][i][0],NULL)) { tcf = strtof(mat[cfTab][i][1],NULL); break; }
            }
            printf("TabIndex = %d  Index = %d  TCF = %f\n",cfTab,i,tcf);
        }
        
        if (sunExpo == 1) scf = 0.9;
        else scf = 1;
        
        // Calcolo del GCF
        if (inst1 != 1) // Se Installation1 è en bandejas
        {
            // Asks Number of Cables in Conduit, Distance between Cables and Position
            
            // Qua usa le tabelle grandi che non ho ancora messo dentro
        }
        else
        {
            // Asks Number of Layers
            
            if (numCores == 2) // 1=Both 2=Multicore 3=Singlecore
            {
                // Qua ragione su installation3 e il numero di Trays
                
            }
            else
            {
                // Qua ragione su installation3 e il numero di Trays
            }
        }
    }
    else if ((strcmp(meth,"H") == 0) || (strcmp(meth,"I") == 0) || (strcmp(meth,"J") == 0))
    {
        if ((strcmp(meth,"H") == 0) || (strcmp(meth,"I") == 0))
        {
            // Asks Tenperature and Resistivity
            
            if (strcmp(insul,"XLPE") == 0)
            {
                cfTab = searchTable("Pag10_TCF_MethHI_XLPE.csv");
                for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe saltare i nomi dei campi
                {
                    if (temp == strtod(mat[cfTab][i][0],NULL)) { tcf = strtof(mat[cfTab][i][1],NULL); break; }
                }
                printf("TabIndex = %d  Index = %d  TCF = %f\n",cfTab,i,tcf);
            }
            else if (strcmp(insul,"HEPR") == 0)
            {
                cfTab = searchTable("Pag10_TCF_MethHI_XLPE.csv");
                for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe saltare i nomi dei campi
                {
                    if (temp == strtod(mat[cfTab][i][0],NULL)) { tcf = strtof(mat[cfTab][i][1],NULL); break; }
                }
                printf("TabIndex = %d  Index = %d  TCF = %f\n",cfTab,i,tcf);
            }
            if (strcmp(meth,"I") == 0)
            {
                cfTab = searchTable("Pag10_RCF_MethI.csv");
                // Qua non ho ancora capito come calcola
            }
            if (strcmp(meth,"H") == 0) // Qua lei non ha messo else, come sotto, chissà che cazzo perchè
            {
                cfTab = searchTable("Pag10_RCF_MethH.csv");
                // Qua non ho ancora capito come calcola
            }
            if (strcmp(meth,"I") == 0)
            {
                // Asks Number of Cables in Conduit and Distance between Cables
                if (numCablesConduit > 1)
                {
                    cfTab = searchTable("Pag11_CCF_MethI.csv");
                }
                else ccf = 1;
            }
            else if (strcmp(meth,"H") == 0) // Qua lei ha messo solo else, chissà che cazzo perchè
            {
                // Asks Number of Cables in Conduit and Distance between Cables
                if (numCablesConduit > 1)
                {
                    cfTab = searchTable("Pag11_CCF_MethH.csv");
                }
                else ccf = 1;
            }
            if ((strcmp(meth,"H") == 0) || (strcmp(meth,"I") == 0))
            {
                // Asks Depth
                cfTab = searchTable("Pag11_DCF_MethHI.csv");
            }
            else if (strcmp(meth,"J") == 0) // Qua lei ha messo solo else, chissà che cazzo perchè
            {
                // Asks Tenperature
                
                if (strcmp(insul,"HEPR") == 0)
                {
                    cfTab = searchTable("Pag12_TCF_MethJ_HEPR.csv");
                    for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe salare i nomi dei campi
                    {
                        if (temp == strtod(mat[cfTab][i][0],NULL)) { tcf = strtof(mat[cfTab][i][1],NULL); break; }
                    }
                    printf("TabIndex = %d  Index = %d  TCF = %f\n",cfTab,i,tcf);
                }
                else if (strcmp(insul,"XLPE") == 0)
                {
                    cfTab = searchTable("Pag12_TCF_MethJ_XLPE.csv");
                    for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe salare i nomi dei campi
                    {
                        if (temp == strtod(mat[cfTab][i][0],NULL)) { tcf = strtof(mat[cfTab][i][1],NULL); break; }
                    }
                    printf("TabIndex = %d  Index = %d  TCF = %f\n",cfTab,i,tcf);
                }
                if (sunExpo == 1) scf = 0.9;
                else scf = 1;
            }
        }
        
    }
    
    //=================================================
    // Inizio Step 5 del Doc "DRAFT FlowChart"
    //=================================================
    if ((strcmp(meth,"A1") == 0) || (strcmp(meth,"A2") == 0) || (strcmp(meth,"B1") == 0) || (strcmp(meth,"B2") == 0) || (strcmp(meth,"C") == 0) || (strcmp(meth,"E") == 0) || (strcmp(meth,"F") == 0) || (strcmp(meth,"G") == 0) || (strcmp(meth,"D1") == 0) || (strcmp(meth,"D2") == 0))
    {
        
    }
    else // Qua lei ha messo solo else, chissà che cazzo perchè, comunque lo metto anche io così è uguale
    {
        
    }
    
    //=================================================
    // Inizio Step 6 del Doc "DRAFT FlowChart"
    //=================================================
    
    fcf=lcf*gcf*scf*tcf*ccf*rcf*cit*crt*oc;
    
    if ((strcmp(meth,"A1") == 0) || (strcmp(meth,"A2") == 0) || (strcmp(meth,"B1") == 0) || (strcmp(meth,"B2") == 0) || (strcmp(meth,"C") == 0) || (strcmp(meth,"E") == 0) || (strcmp(meth,"F") == 0) || (strcmp(meth,"G") == 0) || (strcmp(meth,"D1") == 0) || (strcmp(meth,"D2") == 0))
    {
        int colIndex;
        char aux[80];
        
        strcpy(aux, insul);
        strcat(aux,numConductPerCable);
        cfTab = searchTable("Pag13_TableCalcSez1.csv");
        for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe saltare i nomi dei campi
        {
            if (strcmp(meth,mat[cfTab][i][0]) == 0)
            {
                for(j=0;j<31;j++) // Inizio da zero per avere l'indice "giusto"
                {
                    if (strcmp(mat[cfTab][i][j],aux) == 0)
                    {
                        colIndex = j;
                        break;
                    }
                }
                break;
            }
        }
        printf("rowIndex = %d  colIndex = %d  fcf = %f\n",i,j,fcf);
        
        if (strcmp(conductor,"C") == 0)
        {
            cfTab = searchTable("Pag13_TableCalcSez2_Copper.csv");
            for(i=0;i<MAX_TAB_RECORD;i++)
            {
                if (fcf*strtod(mat[cfTab][i][colIndex],NULL) > current)
                {
                    sez = strtod(mat[cfTab][i][colIndex],NULL);
                    printf("Sezione = %f  rowIndex = %d\n",sez,i);
                    break;
                }
            }
        }
        else if (strcmp(conductor,"A") == 0)
        {
            cfTab = searchTable("Pag13_TableCalcSez2_Alumin.csv");
            for(i=0;i<MAX_TAB_RECORD;i++)
            {
                if (fcf*strtod(mat[cfTab][i][colIndex],NULL) > current)
                {
                    sez = strtod(mat[cfTab][i][colIndex],NULL);
                    printf("Sezione = %f  rowIndex = %d\n",sez,i);
                    break;
                }
            }
        }
        else
        {
            
        }
    }
    else if ((strcmp(meth,"H") == 0) || (strcmp(meth,"I") == 0))
    {
        
    }
    else if (strcmp(meth,"J") == 0) // Qua lei ha messo solo else, chissà che cazzo perchè
    {
        
    }
    
    //=================================================
    // Inizio Step 7 del Doc "DRAFT FlowChart"
    //=================================================
    return 0;
}


@end

