//
//  DataProvider.h
//  CableApp
//
//  Created by Matteo Centro on 14/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataProvider : NSObject

+ (id)sharedInstance;

@end
