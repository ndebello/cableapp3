//
//  DataProvider.m
//  CableApp
//
//  Created by Matteo Centro on 14/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "DataProvider.h"

@interface NSDictionary(JSONCategories)

+(NSDictionary*)dictionaryWithContentsOfJSONFile:(NSString*)fileName;

@end

@implementation NSDictionary(JSONCategories)

+(NSDictionary*)dictionaryWithContentsOfJSONFile:(NSString*)fileName{
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

@end

@interface DataProvider(){
    NSMutableDictionary *cache;
}
@end

@implementation DataProvider


+ (id)sharedInstance {
    static DataProvider *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil)
            sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        cache = [NSMutableDictionary new];
        [self preloadData];
    }
    return self;
}
- (void) preloadData {
    [cache setObject:[NSDictionary dictionaryWithContentsOfJSONFile:@"2-1"] forKey:@"2-1"];
    [cache setObject:[NSDictionary dictionaryWithContentsOfJSONFile:@"2-2"] forKey:@"2-2"];
    [cache setObject:[NSDictionary dictionaryWithContentsOfJSONFile:@"2-3"] forKey:@"2-3"];
    [cache setObject:[NSDictionary dictionaryWithContentsOfJSONFile:@"2-4"] forKey:@"2-4"];
}

@end
