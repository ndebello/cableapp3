//
//  DataSelected.h
//  CableApp
//
//  Created by Welington Barichello on 19/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CableAttributeElemets.h"

@interface DataSelected : NSObject

@property(retain, readwrite, nonatomic) CableAttributeElemets * cableAttE;
@property(retain, readwrite, nonatomic) NSMutableArray * Inst1;
@property(retain, readwrite, nonatomic) NSMutableArray * Inst2;
@property(retain, readwrite, nonatomic) NSMutableArray * Inst3;
@property(retain, readwrite, nonatomic) NSMutableArray * Cables;
@property(retain, readwrite, nonatomic) NSMutableArray * Methods;
@property(retain, readwrite, nonatomic) NSString * s1;
@property(retain, readwrite, nonatomic) NSString * s2;
@property(retain, readwrite, nonatomic) NSString * s3;
@property(retain, readwrite, nonatomic) NSDictionary * CS;

// Page 1
@property int currentType;
@property(retain, readwrite, nonatomic) NSString * sMet;
@property(retain, readwrite, nonatomic) NSString * sMetINT;
// @property(retain, readwrite, nonatomic) NSString * sMetInt;
@property(retain, readwrite, nonatomic) NSString * sNumCores;
@property(retain, readwrite, nonatomic) NSString * sInsulation;
@property(retain, readwrite, nonatomic) NSString * sConductor;
@property(retain, readwrite, nonatomic) NSString * sCurType;

// Page 2
@property(retain, readwrite, nonatomic) NSString * sVolt;
@property(retain, readwrite, nonatomic) NSString * sCurI;
@property(retain, readwrite, nonatomic) NSString * sCOS;
@property(retain, readwrite, nonatomic) NSString * sCondu;
@property(retain, readwrite, nonatomic) NSString * sSunExp;

// Page3
@property(retain, readwrite, nonatomic) NSString * sTemp;
@property(retain, readwrite, nonatomic) NSString * sResis;
@property(retain, readwrite, nonatomic) NSString * sNumCondu;
@property(retain, readwrite, nonatomic) NSString * sNumCableC;
@property(retain, readwrite, nonatomic) NSString * sDistance;
@property(retain, readwrite, nonatomic) NSString * sCircCon;
@property(retain, readwrite, nonatomic) NSString * sNumLayer;
@property(retain, readwrite, nonatomic) NSString * sNumTrays;
@property(retain, readwrite, nonatomic) NSString * sPosition;
@property(retain, readwrite, nonatomic) NSString * sDepth;

// calc result
@property(retain, readwrite, nonatomic) NSDictionary * calResult;

// WLF questi non sono compatibili con key value coding
@property BOOL eDepth;
@property BOOL ePosition;
@property BOOL eSun;
@property BOOL eNumCableC;
@property BOOL eNumCon;
@property BOOL eDistance;
@property BOOL eTrays;
// WLF questi dovrebbero sostituirli
@property(retain, readwrite, nonatomic) NSNumber * eDepthObj;
@property(retain, readwrite, nonatomic) NSNumber * ePositionObj;
@property(retain, readwrite, nonatomic) NSNumber * eSunObj;
@property(retain, readwrite, nonatomic) NSNumber * eNumCableCObj;
@property(retain, readwrite, nonatomic) NSNumber * eNumConObj;
@property(retain, readwrite, nonatomic) NSNumber * eDistanceObj;
@property(retain, readwrite, nonatomic) NSNumber * eTraysObj;
// DEN variabili nuove per il passaggio parametri calc sez
@property(retain, readwrite, nonatomic) NSString * methDen;
@property(retain, readwrite, nonatomic) NSString * numCoreDen;
@property(retain, readwrite, nonatomic) NSString * citDen;
@property(retain, readwrite, nonatomic) NSString * crtDen;
@property(retain, readwrite, nonatomic) NSString * currTypeDen;
@property(retain, readwrite, nonatomic) NSString * voltageDen;
@property(retain, readwrite, nonatomic) NSString * intensDen1;
@property(retain, readwrite, nonatomic) NSString * intensDen2;
@property(retain, readwrite, nonatomic) NSString * intensDen3;
@property(retain, readwrite, nonatomic) NSString * mechDen;
@property(retain, readwrite, nonatomic) NSString * perfDen;
@property(retain, readwrite, nonatomic) NSString * cosenoDen;
@property(retain, readwrite, nonatomic) NSString * vdropDen;
@property(retain, readwrite, nonatomic) NSString * vdrop1Den;
@property(retain, readwrite, nonatomic) NSString * llengthDen;
@property(retain, readwrite, nonatomic) NSString * shortCircDen;
@property(retain, readwrite, nonatomic) NSString * timeShootDen;
@property(retain, readwrite, nonatomic) NSString * tempDen;
@property(retain, readwrite, nonatomic) NSString * sunExpoDen;
@property(retain, readwrite, nonatomic) NSString * groupingDen;
@property(retain, readwrite, nonatomic) NSString * numCircuitsDen;
@property(retain, readwrite, nonatomic) NSString * numCablesDen;
@property(retain, readwrite, nonatomic) NSString * distDen;
@property(retain, readwrite, nonatomic) NSString * positionDen;
@property(retain, readwrite, nonatomic) NSString * numTraysDen;
@property(retain, readwrite, nonatomic) NSString * numLayersDen;
@property(retain, readwrite, nonatomic) NSString * resDen;
@property(retain, readwrite, nonatomic) NSString * depthDen;
@property(retain, readwrite, nonatomic) NSString * ocDen;
@property(retain, readwrite, nonatomic) NSString * dummy1Den;
@property(retain, readwrite, nonatomic) NSString * cableCommNameDen;
@property(retain, readwrite, nonatomic) NSString * restrictionDen;
@property(retain, readwrite, nonatomic) NSString * armoredDen;
@property(retain, readwrite, nonatomic) NSString * savingDen;
@property(retain, readwrite, nonatomic) NSString * moneyDen;
@property(retain, readwrite, nonatomic) NSString * saving1Den;
@property(retain, readwrite, nonatomic) NSString * money1Den;
@property(retain, readwrite, nonatomic) NSString * saving2Den;
@property(retain, readwrite, nonatomic) NSString * money2Den;
@property(retain, readwrite, nonatomic) NSString * DataSaveCal;

//
@property(readwrite, nonatomic) int attOrEl;

+ (id) sharedManager;
- (NSString *) dummyDen;
// - (NSString *) sMetINT;

@end
