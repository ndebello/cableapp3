//
//  DisplaCalcViewController.h
//  CableA
//
//  Created by Welington Barichello on 08/03/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//
#import "DataSelected.h"
#import <UIKit/UIKit.h>

@interface DisplaCalcViewController : UIViewController
@property(readwrite,atomic) DataSelected * sharedCalc;
@end
