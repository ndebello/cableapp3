//
//  DisplaCalcViewController.m
//  CableA
//
//  Created by Welington Barichello on 08/03/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "DisplaCalcViewController.h"
#import "DataSelected.h"

@interface DisplaCalcViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgCable;
@property (weak, nonatomic) IBOutlet UILabel *genericNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UILabel *NameCable;
@property (weak, nonatomic) IBOutlet UILabel *nCondPhase;
@property (weak, nonatomic) IBOutlet UILabel *savingCO2;
@property (weak, nonatomic) IBOutlet UILabel *savingYearling;
@property (weak, nonatomic) IBOutlet UILabel *Saving2;
@property (weak, nonatomic) IBOutlet UILabel *money;
@property (weak, nonatomic) IBOutlet UILabel *sectionlabel;
@property (weak, nonatomic) IBOutlet UILabel *dataSaved;

@end

@implementation DisplaCalcViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = [_sharedCalc.CS objectForKey:@"commercial_name"];
    _viewContainer.layer.cornerRadius = 7;
    [self setLabels];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLabels{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[_sharedCalc.CS objectForKey:@"image_url"]]];
        UIImage *image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            _imgCable.image = image;
        });  
    });
    
    _NameCable.text = [_sharedCalc.CS objectForKey:@"commercial_name"];
    _genericNameLabel.text = [_sharedCalc.CS objectForKey:@"generic_name"];
    _descLabel.text = [_sharedCalc.CS objectForKey:@"description"];
    _nCondPhase.text = [_sharedCalc.calResult objectForKey:@"NumberCore"];
    _savingCO2.text = [_sharedCalc.calResult objectForKey:@"saving1"];
    _savingYearling.text = [_sharedCalc.calResult objectForKey:@"money2"];
    _savingYearling.text = [_sharedCalc.calResult objectForKey:@"saving2"];
    _money.text = [_sharedCalc.calResult objectForKey:@"money1"];
    _sectionlabel.text = [NSString stringWithFormat:@"%@",[_sharedCalc.calResult objectForKey:@"section"]];
    
    _dataSaved.text =[NSString stringWithFormat:@"Savato il %@", _sharedCalc.DataSaveCal];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
