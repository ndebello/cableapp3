//
//  DocumetationViewController.m
//  CableA
//
//  Created by Welington Barichello on 07/03/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "DocumetationViewController.h"

@interface DocumetationViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@end

@implementation DocumetationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Documentation";
    NSString *urlString = @"http://46.252.150.235:8085/docs";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil){
             [_webView loadRequest:request];
             
         }else if (error != nil){
             NSLog(@"Error: %@", error);
         }
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [_activity stopAnimating];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
