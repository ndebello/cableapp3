//
//  Install23ViewController.h
//  CableApp
//
//  Created by Welington Barichello on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Install23ViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *insta1SelTextField;
@property (weak, nonatomic) IBOutlet UITextField *inst2SelTextflied;
@property (weak, nonatomic) IBOutlet UIImageView *imageInst2;
@property (weak, nonatomic) IBOutlet UITextField *inst3SelTextfied;
@property (weak, nonatomic) IBOutlet UIImageView *imageInst3;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;


@end
