//
//  Install23ViewController.m
//  CableApp
//
//  Created by Welington Barichello on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "Install23ViewController.h"
#import "ResultSearch123ViewController.h"
#import "DataSelected.h"

@interface Install23ViewController (){
    UIAlertController * alertController;
    DataSelected * sharedData;
}

@end

@implementation Install23ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Cable Search";
    sharedData = [DataSelected sharedManager];
    self.inst2SelTextflied.alpha = 0;
    self.imageInst2.alpha = 0 ;
    self.inst3SelTextfied.alpha = 0;
    self.imageInst3.alpha = 0;
    self.confirmButton.alpha = 0;
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (self.insta1SelTextField == textField) {
        [self createUIAlertController:sharedData.Inst1 TextField:textField TitleAlert:@"Select Installation 1" MessageAlert:@""];
        [self.view endEditing:YES];
    }
    
    if (self.inst2SelTextflied == textField) {
        [self createUIAlertController:sharedData.Inst2 TextField:textField TitleAlert:@"Select Installation 2" MessageAlert:@""];
        [self.view endEditing:YES];
    }
    if (self.inst3SelTextfied == textField) {
        [self createUIAlertController3:sharedData.Inst3 TextField:textField TitleAlert:@"Select Installation 3" MessageAlert:@""];
        [self.view endEditing:YES];
    }
    
}

-(void)createUIAlertController3:(NSMutableArray *)arrayAlert TextField:(UITextField *)textField TitleAlert:(NSString *)titleAlert MessageAlert:(NSString *)messageAlert{
    NSMutableArray * ap3 = [[NSMutableArray alloc]init];
    alertController = [UIAlertController alertControllerWithTitle:titleAlert
                                                          message:messageAlert
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    for (int  i = 0; i<[arrayAlert count]; i++) {
        
        [ap3 addObject:[arrayAlert[i] allKeys]];
    }
    
    for (int i= 0; i< [arrayAlert count]; i++) {
        
        [alertController addAction:[UIAlertAction
                                    actionWithTitle:[NSString stringWithFormat:@"%@",[arrayAlert[i] valueForKey:[ap3[i] objectAtIndex:0]]]
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        [UIView animateWithDuration:0.5
                                                         animations:^{
                                                             self.confirmButton.alpha = 1;
                                        }];
                                        sharedData.s3 = [NSString stringWithFormat:@"%@",[ap3[i] objectAtIndex:0]];
                                        textField.text = [NSString stringWithFormat:@"%@",[arrayAlert[i] valueForKey:[ap3[i] objectAtIndex:0]]];
                                    }]];
    }
    
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Annulla", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}


-(void)createUIAlertController:(NSMutableArray *)arrayAlert TextField:(UITextField *)textField TitleAlert:(NSString *)titleAlert MessageAlert:(NSString *)messageAlert{
    NSMutableArray * ap = [[NSMutableArray alloc]init];
    alertController = [UIAlertController alertControllerWithTitle:titleAlert
                                                          message:messageAlert
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    for (int  i = 0; i<[arrayAlert count]; i++) {
        
        [ap addObject:[arrayAlert[i] allKeys]];
    }
    
    for (int i= 0; i< [arrayAlert count]; i++) {
        
        [alertController addAction:[UIAlertAction
                                    actionWithTitle:[NSString stringWithFormat:@"%@",[arrayAlert[i] valueForKey:[ap[i] objectAtIndex:0]]]
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        if (self.insta1SelTextField == textField) {
                                            sharedData.s1 = [NSString stringWithFormat:@"%@",[ap[i] objectAtIndex:0]];
                                            sharedData.s2 = [NSString stringWithFormat:@""];
                                            [self install2];
                                            //[self instalation3:sharedData.s1];
                                            textField.text = [NSString stringWithFormat:@"%@",[arrayAlert[i] valueForKey:[ap[i] objectAtIndex:0]]];
                                        }else{
                                            sharedData.s2 = [NSString stringWithFormat:@"%@",[ap[i] objectAtIndex:0]];
                                            sharedData.s3 = [NSString stringWithFormat:@""];
                                            [self instalation3:sharedData.s2];
                                            textField.text = [NSString stringWithFormat:@"%@",[arrayAlert[i] valueForKey:[ap[i] objectAtIndex:0]]];
                                        }
                                        
                                    }]];
    }
    
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Annulla", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)instalation3:(NSString *)idInstallation2{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.confirmButton.alpha = 0;
                             self.inst3SelTextfied.alpha = 0;
                             self.imageInst3.alpha = 0;
                         }];
    });
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://46.252.150.235:8085/api/installation3s/index/%@/%@.json",idInstallation2,sharedData.s1]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             NSLog(@"Error,%@", [error localizedDescription]);
         }
         else
         {
             [self ParsingJsonInstallation1:data];
             
         }
     }];
}

-(void)ParsingJsonInstallation1:(NSData *)data{
    sharedData.Inst3 = [NSMutableArray new];
    dispatch_async(dispatch_get_main_queue(), ^{
         self.inst3SelTextfied.text = [NSString stringWithFormat:@""];
    });
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          options:kNilOptions
                          error:&error];
    int i =0;
    for (NSDictionary * inst1 in json) {
        i++;
        NSDictionary *method = [inst1 objectForKey:@"Installation3"];
        if (![method objectForKey:@"status"]){
            NSDictionary *p = @{
                                [[inst1 objectForKey:@"Installation3"] objectForKey:@"id"]: [[inst1 objectForKey:@"Installation3"] objectForKey:@"description"]
                                };
            [sharedData.Inst3 addObject:p];
            if (i == [json count] ) {
                NSLog(@"%d,%lu",i,(unsigned long)[json count]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:0.5
                                     animations:^{
                                         self.inst3SelTextfied.alpha = 1;
                                         self.imageInst3.alpha = 1;
                                     }];
                });
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     sharedData.s3 =@"";
                                     self.confirmButton.alpha = 1;
                                 }];
            });
        }
    }
   
}


-(void)install2{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.inst2SelTextflied.alpha = 1;
                             self.imageInst2.alpha = 1;
                             //
                             self.inst2SelTextflied.text = @"";
                             self.confirmButton.alpha = 0;
                             self.inst3SelTextfied.alpha = 0;
                             self.imageInst3.alpha = 0;
                         }];
    });
    if (!sharedData.Inst2) {
        sharedData.Inst2 = [NSMutableArray new];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://46.252.150.235:8085/api/installation2s/index/%@.json",sharedData.s1]];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 NSLog(@"Error,%@", [error localizedDescription]);
             }
             else
             {
                 [self ParsingJsonInstallation2:data];
             }
         }];
    }
}

-(void)ParsingJsonInstallation2:(NSData *)data{
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          options:kNilOptions
                          error:&error];
    
    for (NSDictionary *inst2 in json) {
        
        NSDictionary *p = @{
                            [[inst2 objectForKey:@"Installation2"] objectForKey:@"id"]: [[inst2 objectForKey:@"Installation2"] objectForKey:@"description"]
                            };
        [sharedData.Inst2 addObject:p];
        
    }
    
}


-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        //sharedData.Inst2 = nil;
    }
    [super viewWillDisappear:animated];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
