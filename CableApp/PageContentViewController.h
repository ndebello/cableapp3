//
//  PageContentViewController.h
//  CableApp
//
//  Created by Welington Barichello on 18/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController<UITextFieldDelegate>


//@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *titleText1;
@property NSString *titleText2;
@property NSString *titleText3;
@property NSString *titleText4;
@property NSString *titleText5;
@property NSString *titleText6;
@property NSString *titleText7;
@property NSString *titleText8;
@property NSString *titleText9;
@property NSString *titleText10;
@property NSString *imageFile;

//Labels View
@property (weak, nonatomic) IBOutlet UILabel *Label1;
@property (weak, nonatomic) IBOutlet UILabel *Label2;
@property (weak, nonatomic) IBOutlet UILabel *Label3;
@property (weak, nonatomic) IBOutlet UILabel *Label4;
@property (weak, nonatomic) IBOutlet UILabel *Label5;
@property (weak, nonatomic) IBOutlet UILabel *Label6;
@property (weak, nonatomic) IBOutlet UILabel *Label7;
@property (weak, nonatomic) IBOutlet UILabel *Label8;
@property (weak, nonatomic) IBOutlet UILabel *Label9;
@property (weak, nonatomic) IBOutlet UILabel *Label10;
//TextFields
@property (weak, nonatomic) IBOutlet UITextField *TextFields1;
@property (weak, nonatomic) IBOutlet UITextField *TextFields2;
@property (weak, nonatomic) IBOutlet UITextField *TextFields3;
@property (weak, nonatomic) IBOutlet UITextField *TextFields4;
@property (weak, nonatomic) IBOutlet UITextField *TextFields5;
@property (weak, nonatomic) IBOutlet UITextField *TextFields6;
@property (weak, nonatomic) IBOutlet UITextField *TextFields7;
@property (weak, nonatomic) IBOutlet UITextField *TextFields8;
@property (weak, nonatomic) IBOutlet UITextField *TextFields9;
@property (weak, nonatomic) IBOutlet UITextField *TextFields10;

@property (weak, nonatomic) IBOutlet UIImageView *arrowL;
@property (weak, nonatomic) IBOutlet UIImageView *arrowR;



- (IBAction)actionCalc:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *sectionCalc;

@end
