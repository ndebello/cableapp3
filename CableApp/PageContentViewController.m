//
//  PageContentViewController.m
//  CableApp
//
//  Created by Welington Barichello on 18/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "PageContentViewController.h"
#import "StartPageViewController.h"
#import "DataSelected.h"
#import "Calculator.h"

@interface PageContentViewController (){
    DataSelected * sharedData;
    UIAlertController *alertController;
    
}

@end

@implementation PageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    sharedData = [DataSelected sharedManager];
    [self hideElement];
    self.Label1.text = self.titleText1;
    self.Label2.text = self.titleText2;
    self.Label3.text = self.titleText3;
    self.Label4.text = self.titleText4;
    self.Label5.text = self.titleText5;
    
    self.Label6.text = self.titleText6;
    self.Label7.text = self.titleText7;
    self.Label8.text = self.titleText8;
    self.Label9.text = self.titleText9;
    self.Label10.text = self.titleText10;
    //self.Label5.text = self.titleText5;
    [self methodRequest];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)methodRequest{
    if ([sharedData.Methods count] == 0 ) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://46.252.150.235:8085/api/methods/index/installation1_id:%@/installation2_id:%@/installation3_id:%@/cable_id:%@.json",sharedData.s1,sharedData.s2,sharedData.s3,[sharedData.CS objectForKey:@"id"]]];
        NSLog(@"URL1 %@", url);
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 NSLog(@"Error,%@", [error localizedDescription]);
             }
             else
             {
                 [self ParsingJsonmethodRequest:data];
             }
         }];

    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}

-(void)ParsingJsonmethodRequest:(NSData*)data{
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          options:kNilOptions
                          error:&error];
    
    for (NSDictionary * inst1 in json) {
        NSDictionary *method = [inst1 objectForKey:@"Method"];
        if ([method objectForKey:@"id"]){
        NSDictionary *p = @{
                            [[inst1 objectForKey:@"Method"] objectForKey:@"id"]: [[inst1 objectForKey:@"Method"] objectForKey:@"code"]
                            };
        [sharedData.Methods addObject:p];
        } else {
            NSLog(@"NESSUN METODO ARRIVA DAL WEBSERVICE");
        }
    }
    
}

-(void)hideElement{
    if (_pageIndex == 0) {
        self.arrowL.hidden = YES;
        self.Label6.hidden = YES;
        self.Label7.hidden = YES;
        self.Label8.hidden = YES;
        self.Label9.hidden = YES;
        self.Label10.hidden = YES;
        
        self.TextFields6.hidden = YES;
        self.TextFields7.hidden = YES;
        self.TextFields8.hidden = YES;
        self.TextFields9.hidden = YES;
        self.TextFields10.hidden = YES;
        self.sectionCalc.hidden = NO;
        //insertTextElement
        sharedData.sNumCores = [NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"NumberCore"] objectForKey:@"name"]];
        self.TextFields2.text = sharedData.sNumCores;
        self.TextFields3.text = [NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"Insulation"] objectForKey:@"name"]];
        self.TextFields4.text = [NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"Conductor"] objectForKey:@"name"]];
        
        [self controlElements];
        
    }else if (_pageIndex ==1){
        self.arrowL.hidden = NO;
        self.Label6.hidden = NO;
        self.Label7.hidden = NO;
        self.Label8.hidden = NO;
        self.Label9.hidden = NO;
        self.Label10.hidden = NO;
        
        self.TextFields6.hidden = NO;
        self.TextFields7.hidden = NO;
        self.TextFields8.hidden = NO;
        self.TextFields9.hidden = NO;
        self.TextFields10.hidden = NO;
        self.sectionCalc.hidden = YES;
        
        if ([sharedData.sMet isEqualToString:@"A1"] || [sharedData.sMet isEqualToString:@"A2"] || [sharedData.sMet isEqualToString:@"B1"]
            || [sharedData.sMet isEqualToString:@"B2"] || [sharedData.sMet isEqualToString:@"C"] || [sharedData.sMet isEqualToString:@"E"]
            || [sharedData.sMet isEqualToString:@"F"] || [sharedData.sMet isEqualToString:@"G"] || [sharedData.sMet isEqualToString:@"J"]) {
            
            self.TextFields5.enabled = NO;
            sharedData.eSun = YES;
        }else{
            self.TextFields5.enabled = YES;
            sharedData.eSun = NO;
        }
        
        //insertElement
        self.TextFields3.text = @"0,9 φ";
        self.TextFields6.text = [NSString stringWithFormat:@"%@",[sharedData.CS objectForKey:@"tcsmcm"]];
        self.TextFields7.text = [NSString stringWithFormat:@"%@",[sharedData.CS objectForKey:@"tcsmtp"]];
        self.TextFields8.text = [NSString stringWithFormat:@"%@",[sharedData.CS objectForKey:@"tcsscmpt"]];
        self.TextFields9.text = [NSString stringWithFormat:@"%@",[sharedData.CS objectForKey:@"lcssc"]];
        self.TextFields10.text = [NSString stringWithFormat:@"%@",[sharedData.CS objectForKey:@"lcsmc"]];
    }else if (_pageIndex ==2){
        self.arrowR.hidden = YES;
        self.Label6.hidden = NO;
        self.Label7.hidden = NO;
        self.Label8.hidden = NO;
        self.Label9.hidden = NO;
        self.Label10.hidden = NO;
        
        self.TextFields6.hidden = NO;
        self.TextFields7.hidden = NO;
        self.TextFields8.hidden = NO;
        self.TextFields9.hidden = NO;
        //da finire
        if ([sharedData.sMet isEqualToString:@"A1"] || [sharedData.sMet isEqualToString:@"A2"] || [sharedData.sMet isEqualToString:@"B1"]
            || [sharedData.sMet isEqualToString:@"B2"] || [sharedData.sMet isEqualToString:@"C"] || [sharedData.sMet isEqualToString:@"E"]
            || [sharedData.sMet isEqualToString:@"F"] || [sharedData.sMet isEqualToString:@"G"]) {
            if (![sharedData.s2 isEqualToString:@"8"]) {
                self.TextFields4.enabled = YES;
                self.TextFields5.enabled = YES;
                self.TextFields9.enabled = YES;
                sharedData.eNumCableC = NO;
                sharedData.eDistance = NO;
                sharedData.ePosition = NO;
            }else{
                self.TextFields4.enabled = NO;
                self.TextFields5.enabled = NO;
                self.TextFields9.enabled = NO;
                sharedData.eNumCableC = YES;
                sharedData.eDistance = YES;
                sharedData.ePosition = YES;
            }
            
            if ([[NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"NumberCore"] objectForKey:@"name"]] isEqualToString:@"2"]) {
                self.TextFields8.enabled = YES;
                sharedData.eTrays = NO;
            }else{
                
                self.TextFields8.enabled = NO;
                sharedData.eTrays = YES;
            }
            
        }
        
        if ([sharedData.sMet isEqualToString:@"D1"] || [sharedData.sMet isEqualToString:@"D2"]) {
            self.TextFields4.enabled = NO;
            self.TextFields5.enabled = NO;
            sharedData.eNumCableC = YES;
            sharedData.eNumCon = YES;
        }else{
            self.TextFields4.enabled = YES;
            self.TextFields5.enabled = YES;
            sharedData.eNumCableC = NO;
            sharedData.eNumCon = NO;
        }
        
         self.TextFields10.hidden = NO;
        if ([sharedData.sMet isEqualToString:@"H"] || [sharedData.sMet isEqualToString:@"I"]) {
            self.TextFields10.enabled = NO;
            sharedData.eDepth = YES;
        }else{
            self.TextFields10.enabled = YES;
            sharedData.eDepth = NO;
        }
        
        self.sectionCalc.hidden = YES;
        [self controlElements3];
    }
    
}



-(void)controlElements{
    //control
    if (sharedData.sMet) {
        _TextFields1.text = sharedData.sMet;
    }
    if (sharedData.sNumCores){
        _TextFields2.text = sharedData.sNumCores;
    }
    if (sharedData.sInsulation){
        _TextFields3.text = sharedData.sInsulation;
    }
    if (sharedData.sConductor){
        _TextFields4.text = sharedData.sConductor;
    }
    if (sharedData.sCurType){
        _TextFields5.text = sharedData.sCurType;
    }
}
-(void)controlElements3{
    //control
    if (sharedData.sTemp) {
        _TextFields1.text = sharedData.sTemp;
    }
    if (sharedData.sResis){
        _TextFields2.text = sharedData.sResis;
    }
    if (sharedData.sNumCondu){
        _TextFields3.text = sharedData.sNumCondu;
    }
    if (sharedData.sNumCableC){
        _TextFields4.text = sharedData.sNumCableC;
    }
    if (sharedData.sDistance){
        _TextFields5.text = sharedData.sDistance;
    }
    if (sharedData.sCircCon){
        _TextFields6.text = sharedData.sCircCon;
    }
    if (sharedData.sNumLayer){
        _TextFields7.text = sharedData.sNumLayer;
    }
    if (sharedData.sNumTrays){
        _TextFields8.text = sharedData.sNumTrays;
    }
    if (sharedData.sPosition){
        _TextFields9.text = sharedData.sPosition;
    }
    if (sharedData.sDepth){
        _TextFields10.text = sharedData.sDepth;
    }
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (_pageIndex == 0) {
        if (_TextFields1 == textField) {
            NSMutableArray *pr = [NSMutableArray new];
            for (NSDictionary * e  in sharedData.Methods) {
                [pr addObject: [[e allValues] objectAtIndex:0]];
            }
            [self createUIAlertController:pr TextField:textField TitleAlert:@"Select Method" MessageAlert:@""];
            [self.view endEditing:YES];
        }else if (_TextFields2 == textField){
//            NSMutableArray  *P = [[NSMutableArray alloc] initWithObjects:@"Single Core",@"Multi Core",@"Both",nil];
//            [self createUIAlertController:P TextField:textField TitleAlert:@"Select Number Core" MessageAlert:@""];
             [self.view endEditing:YES];
        }else if(_TextFields3 == textField){
             [self.view endEditing:YES];
        }else if(_TextFields4 == textField){
            [self.view endEditing:YES];
        }else if(_TextFields5 == textField){
            if ([[NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"Restriction"] objectForKey:@"id"]] isEqualToString:@"1"]) {
                NSMutableArray  *P = [[NSMutableArray alloc] initWithObjects:@"AC three-phase",@"AC Monophasic",@"Direct Current",nil];
                [self createUIAlertController:P TextField:textField TitleAlert:@"Select Current Type" MessageAlert:@""];
                [self.view endEditing:YES];
            }else if ([[NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"Restriction"] objectForKey:@"id"]] isEqualToString:@"2"]){
                NSMutableArray  *P = [[NSMutableArray alloc] initWithObjects:@"AC Monophasic",@"Direct Current",nil];
                [self createUIAlertController:P TextField:textField TitleAlert:@"Select Current Type" MessageAlert:@""];
                [self.view endEditing:YES];
                
            }else if ([[NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"Restriction"] objectForKey:@"id"]] isEqualToString:@"3"] ||
                      [[NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"Restriction"] objectForKey:@"id"]] isEqualToString:@"4"]){
                NSMutableArray  *P = [[NSMutableArray alloc] initWithObjects:@"AC three-phase",nil];
                [self createUIAlertController:P TextField:textField TitleAlert:@"Select Current Type" MessageAlert:@""];
                [self.view endEditing:YES];
            }
        }
    }else if(_pageIndex ==1){
        if (_TextFields1 == textField) {
            if (sharedData.currentType ==0){
                NSMutableArray  *P = [[NSMutableArray alloc] initWithObjects:@"127/220",@"132/230",@"220/380",@"230/400",nil];
                [self createUIAlertController:P TextField:textField TitleAlert:@"Select AC three-phase (V)" MessageAlert:@""];
                [self.view endEditing:YES];
            }else if(sharedData.currentType == 1){
                NSMutableArray  *P = [[NSMutableArray alloc] initWithObjects:@"127",@"132",@"220",@"230",nil];
                [self createUIAlertController:P TextField:textField TitleAlert:@"Select AC Monophasic (V)" MessageAlert:@""];
                [self.view endEditing:YES];
            }else if(sharedData.currentType ==2){
                NSMutableArray  *P = [[NSMutableArray alloc] initWithObjects:@"110",@"220",nil];
                [self createUIAlertController:P TextField:textField TitleAlert:@"Select Direct Current (V)" MessageAlert:@""];
                [self.view endEditing:YES];
            }
        }else if (_TextFields5 == textField){
            NSMutableArray  *P = [[NSMutableArray alloc] initWithObjects:@"YES",@"NO",nil];
            [self createUIAlertController:P TextField:textField TitleAlert:@"Sun Exposition" MessageAlert:@""];
            [self.view endEditing:YES];
        }else if (_TextFields6 == textField){
            [self.view endEditing:YES];
        }else if (_TextFields7 == textField){
            [self.view endEditing:YES];
        }else if (_TextFields8 == textField){
            [self.view endEditing:YES];
        }else if (_TextFields9 == textField){
            [self.view endEditing:YES];
        }else if (_TextFields10 == textField){
            [self.view endEditing:YES];
        }
    }else if(_pageIndex == 2){
        if (_TextFields1 == textField) {
            NSMutableArray  *P = [[NSMutableArray alloc] initWithObjects:@"10",@"15",@"20",@"25",@"30",@"35",@"40",@"45",@"50",@"55",@"60",nil];
            [self createUIAlertController:P TextField:textField TitleAlert:@"Select Temperature (°C)" MessageAlert:@""];
            [self.view endEditing:YES];
        }else if (_TextFields4 == textField){
            NSMutableArray  *P = [[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"9",@"12",@"16",@"20",nil];
            [self createUIAlertController:P TextField:textField TitleAlert:@"Select n° cable condiut " MessageAlert:@""];
            [self.view endEditing:YES];
        }else if (_TextFields10 == textField){
            NSMutableArray  *P = [[NSMutableArray alloc] initWithObjects:@"0,5",@"0,6",@"0,8",@"1",@"1,25",@"1,5",@"1,75",@"2",@"2,5",@"3",nil];
            [self createUIAlertController:P TextField:textField TitleAlert:@"Select Depth (m) " MessageAlert:@""];
            [self.view endEditing:YES];
        }
        
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (_pageIndex == 1) {
        if (_TextFields2 == textField) {
            sharedData.sCurI = textField.text;
        }else if (_TextFields3 == textField){
            sharedData.sCOS = textField.text;
        }else if (_TextFields4 == textField){
            sharedData.sCondu = textField.text;
        }
    }
    
    if (_pageIndex == 2) {
        if (_TextFields5 == textField){
            sharedData.sDistance = textField.text;
        }else if (_TextFields6 == textField){
            sharedData.sCircCon = textField.text;
        }else if (_TextFields7 == textField){
            sharedData.sNumLayer = textField.text;
        }else if (_TextFields8 == textField){
            sharedData.sNumTrays = textField.text;
        }else if (_TextFields9 == textField){
            sharedData.sPosition = textField.text;
        }else if (_TextFields2 == textField){
            sharedData.sResis = textField.text;
        }else if (_TextFields3 == textField){
            sharedData.sNumCondu = textField.text;
        }
    }
    return YES;
}

-(void)createUIAlertController:(NSMutableArray *)arrayAlert TextField:(UITextField *)textField TitleAlert:(NSString *)titleAlert MessageAlert:(NSString *)messageAlert{
    
    alertController = [UIAlertController alertControllerWithTitle:titleAlert
                                                          message:messageAlert
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i= 0; i< [arrayAlert count]; i++) {
        [alertController addAction:[UIAlertAction
                                    actionWithTitle:[NSString stringWithFormat:@"%@",arrayAlert[i]]
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action){
                                        if (_pageIndex == 0) {
                                            if (_TextFields1 == textField) {
                                                sharedData.sMet = arrayAlert[i];
                                                // sharedData.sMetINT = [NSString stringWithFormat:@"%d",i+1];
                                            }else if (_TextFields2 == textField){
                                                sharedData.sNumCores = arrayAlert[i];
                                            }else if (_TextFields3 == textField){
                                                sharedData.sInsulation = arrayAlert[i];
                                            }else if (_TextFields4 == textField){
                                                sharedData.sConductor = arrayAlert[i];
                                            }else if (_TextFields5 == textField){
                                                sharedData.sCurType = arrayAlert[i];
                                                if ([[NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"Restriction"] objectForKey:@"id"]] isEqualToString:@"1"]) {
                                                    sharedData.currentType = i;
                                                }else if ([[NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"Restriction"] objectForKey:@"id"]] isEqualToString:@"2"]){
                                                    sharedData.currentType = i+1;
                                                    
                                                }else if ([[NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"Restriction"] objectForKey:@"id"]] isEqualToString:@"3"] ||
                                                          [[NSString stringWithFormat:@"%@",[[sharedData.CS objectForKey:@"Restriction"] objectForKey:@"id"]] isEqualToString:@"4"]){
                                                    sharedData.currentType = 0;
                                                }
                                                
                                            }
                                        }else if(_pageIndex == 2){
                                            if (_TextFields1 == textField) {
                                                sharedData.sTemp = arrayAlert[i];
                                            }else if (_TextFields4 == textField){
                                                sharedData.sNumCableC = arrayAlert[i];
                                            }else if (_TextFields10 == textField){
                                                sharedData.sDepth = arrayAlert[i];
                                            }
                                        }else if (_pageIndex ==1){
                                            if (_TextFields1 == textField) {
                                                sharedData.sVolt = arrayAlert[i];
                                            }else if(_TextFields5 == textField){
                                                if (i == 0) {
                                                    sharedData.sSunExp = [NSString stringWithFormat:@"%d",1];
                                                }else{
                                                    sharedData.sSunExp = [NSString stringWithFormat:@"%d",0];
                                                }
                                            }
                                        }
                                        
                                        
                                        //sharedData.s1 = [NSString stringWithFormat:@"%d",i+1];
                                        textField.text = arrayAlert[i];
                                    }]];
    }
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Annulla", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        //sharedData.Methods = [NSMutableArray new];
        //sharedData.sMet = nil;
    }
    [super viewWillDisappear:animated];
}

- (IBAction)actionCalc:(id)sender {
    if (sharedData.eDepth) {
        sharedData.sDepth = @"1";
    }
    if (sharedData.ePosition) {
        sharedData.sPosition = @"1";
    }
    
    if (sharedData.eSun) {
        sharedData.sSunExp = @"1";
    }
    if (sharedData.eNumCableC) {
        sharedData.sNumCableC = @"1";
    }
    if (sharedData.eNumCon) {
        sharedData.sNumCondu = @"1";
    }
    
    if (sharedData.eDistance) {
        sharedData.sDistance = @"1";
    }
    if (sharedData.eTrays) {
        sharedData.sNumTrays = @"1";
    }
    NSLog(@"Calcola");
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://46.252.150.235:8085/api/cables/compute/inst1:%@/inst2:%@/inst3:%@/method:%@/numCores:%@/insulation:%@/conductor:%@/temperature:%@/resistivity:%@/volt:1/conductivity:%@/current:%@/sunExpo:%@/numConductPerCable:%@/distBetwCables:%@/numCablesConduit:%@/circuitPerConduit:%@/numLayers:%@/numTrays:%@/position:%@/depth:%@/tcs1:%@/tcs2:%@/tcs3:%@/lcs1:%@/lcs2:%@.json"
                                       ,sharedData.s1,sharedData.s2,sharedData.s3,sharedData.sMetINT,
                                       [[sharedData.CS objectForKey:@"NumberCore"] objectForKey:@"id"],
                                       [[sharedData.CS objectForKey:@"Insulation"] objectForKey:@"id"],
                                       [[sharedData.CS objectForKey:@"Conductor"] objectForKey:@"id"],
                                       sharedData.sTemp?sharedData.sTemp:@"",
                                       sharedData.sResis?sharedData.sResis:@"",
                                       sharedData.sCondu?sharedData.sCondu:@"",
                                       sharedData.sCurI?sharedData.sCurI:@"",
                                       sharedData.sSunExp?sharedData.sSunExp:@"",
                                       sharedData.sNumCondu?sharedData.sNumCondu:@"",
                                       sharedData.sDistance?sharedData.sDistance:@"",
                                       sharedData.sNumCableC?sharedData.sNumCondu:@"",
                                       sharedData.sCircCon?sharedData.sCircCon:@"",
                                       sharedData.sNumLayer?sharedData.sNumLayer:@"",
                                       sharedData.sNumTrays?sharedData.sNumTrays:@"",
                                       sharedData.sPosition?sharedData.sPosition:@"",
                                       sharedData.sDepth?sharedData.sDepth:@"",
                                       [sharedData.CS objectForKey:@"tcsmcm"],
                                       [sharedData.CS objectForKey:@"tcsmtp"],
                                       [sharedData.CS objectForKey:@"tcsscmpt"],
                                       [sharedData.CS objectForKey:@"lcssc"],
                                       [sharedData.CS objectForKey:@"lcsmc"]]];
    NSLog(@"URL %@", url);
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             NSLog(@"Error,%@", [error localizedDescription]);
         }
         else
         {
             [self calcJsonParse:data];
         }
     }];
//    [[[Calculator alloc]init]calculate];
}

-(void)calcJsonParse:(NSData *)data{
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          options:kNilOptions
                          error:&error];
    dispatch_async (dispatch_get_main_queue(), ^{
    if ([json objectForKey:@"section"]) {
        sharedData.calResult = json;
        [self performSegueWithIdentifier:@"resultCalc" sender:nil];
    }else{
        
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Attention"
                                          message:@"All parameters must be entered"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Yes"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            //Handel your yes please button action here
                                            
                                            
                                        }];
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
    }
    });
}

@end
