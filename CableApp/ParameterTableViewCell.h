//
//  ParameterTableViewCell.h
//  CableA
//
//  Created by Matteo Centro on 17/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SinglePageViewController;

@interface ParameterTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (nonatomic, strong) NSDictionary * dictionary;
@property (weak, nonatomic) SinglePageViewController * controller;
@property (nonatomic, assign) NSUInteger row;

@end
