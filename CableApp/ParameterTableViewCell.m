//
//  ParameterTableViewCell.m
//  CableA
//
//  Created by Matteo Centro on 17/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "StartPageViewController.h"
#import "ParameterTableViewCell.h"
#import "DataSelected.h"
#import "SinglePageViewController.h"

#define kDisableNotificationName @"NTFDisable%@"
#define kDictKeyLabel            @"label"
#define kDictKeyKeypath          @"keypath"
#define kDictKeyType             @"type"
#define kDictKeyDisableItems     @"disableItems"
#define kDictKeyTitle            @"popupTitle"
#define kDictKeyPopupValues      @"popupValues"
#define kTypeYESNO               @"YESNO"
#define kTypePOPUP               @"POPUP"
#define MAX_TAB_RECORD           128

int myIntensityFlag = 0;

@interface ParameterTableViewCell () {
    DataSelected * sharedData;
    UIAlertController * alertController;
}

@property (nonatomic, strong) UILabel * label;
@property (nonatomic, strong) UITextField * textField;

@end

@implementation ParameterTableViewCell

- (void) awakeFromNib
{
    sharedData = [DataSelected sharedManager];
    self.label = (UILabel *) [self.contentView viewWithTag:10];
    self.textField = (UITextField *) [self.contentView viewWithTag:20];
}

- (void) setDictionary:(NSDictionary *)dictionary
{
    _dictionary = dictionary;
    self.textField.delegate = self;
    self.label.text = [_dictionary objectForKey:kDictKeyLabel];
    NSString * kp = [_dictionary objectForKey:kDictKeyKeypath];
    if (kp)
    {
        if ([[self.dictionary objectForKey:kDictKeyType]isEqualToString:kTypeYESNO])
        {
            self.textField.text = ([[sharedData valueForKeyPath:kp]boolValue] ? @"YES" : @"NO");
        }
        else
        {
            if ([sharedData valueForKeyPath:kp])
            {
                self.textField.text = [NSString stringWithFormat:@"%@", [sharedData valueForKeyPath:kp]];
            }
            else
            {
                self.textField.text = @"";
            }
        }
        NSArray * disableItems = [self.dictionary objectForKey:kDictKeyDisableItems]; // Per campi mutuamente esclusivi
        if (disableItems)
        {
            // Mi metto in ascolto su notifiche che mi disabilitano a me...
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emptyMe:) name:[NSString stringWithFormat:kDisableNotificationName, kp] object:nil];
        }
        
        if ([self.dictionary objectForKey:@"defaultDisabled"])
        {
            [self disableMe:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disableMe:) name:[NSString stringWithFormat:@"DIS%@", [self.dictionary objectForKey:@"defaultDisabled"]] object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enableMe: initValue:) name:[NSString stringWithFormat:@"ENA%@", [self.dictionary objectForKey:@"defaultDisabled"]] object:nil];
        }
        
        NSLog(@"VAR: %@ METODO SCELTO: %@ NUM CORES: %@ INST: %@ %@ %@\n", kp, sharedData.methDen, sharedData.sNumCores, sharedData.s1, sharedData.s2, sharedData.s3);
        
        // Temp default
        if (((([sharedData.methDen isEqualToString:@"3"]) || ([sharedData.methDen isEqualToString:@"4"]))) && ([kp isEqualToString:@"tempDen"])) // D1 D2
        {
            [self enableMe:nil initValue:@"25C"];
            sharedData.tempDen = @"25C";
        }
        else if ((([sharedData.methDen isEqualToString:@"5"]) || ([sharedData.methDen isEqualToString:@"6"]) || ([sharedData.methDen isEqualToString:@"7"]) || ([sharedData.methDen isEqualToString:@"8"]) || ([sharedData.methDen isEqualToString:@"1"]) || ([sharedData.methDen isEqualToString:@"9"]) || ([sharedData.methDen isEqualToString:@"2"]) || ([sharedData.methDen isEqualToString:@"10"])) && ([kp isEqualToString:@"tempDen"]))  // A1 ecc.
        {
            [self enableMe:nil initValue:@"40C"];
            sharedData.tempDen = @"40C";
        }
        else if (((([sharedData.methDen isEqualToString:@"13"]) || ([sharedData.methDen isEqualToString:@"11"]))) && ([kp isEqualToString:@"tempDen"]))  // H I
        {
            [self enableMe:nil initValue:@"25C"];
            sharedData.tempDen = @"25C";
        }
        else if (([sharedData.methDen isEqualToString:@"12"]) && ([kp isEqualToString:@"tempDen"])) // J
        {
            [self enableMe:nil initValue:@"40C"];
            sharedData.tempDen = @"40C";
        }
        
        // Res default
        if (((([sharedData.methDen isEqualToString:@"3"]) || ([sharedData.methDen isEqualToString:@"4"]))) && ([kp isEqualToString:@"resDen"])) // D1 D2
        {
            [self enableMe:nil initValue:@"2.5R"];
            sharedData.resDen = @"2.5R";
        }
        else if ((([sharedData.methDen isEqualToString:@"5"]) || ([sharedData.methDen isEqualToString:@"6"]) || ([sharedData.methDen isEqualToString:@"7"]) || ([sharedData.methDen isEqualToString:@"8"]) || ([sharedData.methDen isEqualToString:@"1"]) || ([sharedData.methDen isEqualToString:@"9"]) || ([sharedData.methDen isEqualToString:@"2"]) || ([sharedData.methDen isEqualToString:@"10"])) && ([kp isEqualToString:@"resDen"]))  // A1 ecc.
        {
            [self disableMe:nil];
        }
        else if (((([sharedData.methDen isEqualToString:@"13"]) || ([sharedData.methDen isEqualToString:@"11"]))) && ([kp isEqualToString:@"resDen"]))  // H I
        {
            [self enableMe:nil initValue:@"1.5R"];
            sharedData.resDen = @"1.5R";
        }
        else if (([sharedData.methDen isEqualToString:@"12"]) && ([kp isEqualToString:@"resDen"]))  // J
        {
            [self disableMe:nil];
        }
        
        // Coseno non lo chiedo in caso di corr continua, ma lo metto uguale a 1 per evitare problemi con le fornulette
        if (([sharedData.currTypeDen isEqualToString:@"Direct-Current"]) && ([kp isEqualToString:@"cosenoDen"])) { [self disableMe:nil]; sharedData.cosenoDen = @"1";}
        else if ([kp isEqualToString:@"cosenoDen"]) { [self enableMe:nil initValue:@"0.9"]; sharedData.cosenoDen = @"0.9"; }
        
        // Altre inizializzazioni
        if ([kp isEqualToString:@"vdropDen"]) { [self enableMe:nil initValue:@"0"]; sharedData.vdropDen = @"0"; }
        if ([kp isEqualToString:@"ocDen"]) { [self enableMe:nil initValue:@"1"]; sharedData.ocDen = @"1"; }
        if ([kp isEqualToString:@"perfDen"]) { [self enableMe:nil initValue:@"100"]; sharedData.perfDen = @"100"; }
        if ([kp isEqualToString:@"citDen"]) { [self enableMe:nil initValue:@"1-Other"]; sharedData.perfDen = @"1-Other"; }
        if ([kp isEqualToString:@"crtDen"]) { [self enableMe:nil initValue:@"1-Other"]; sharedData.perfDen = @"1-Other"; }
        
        // Depth lo chiede solo nel caso H e I, quindi nego
        if (!((([sharedData.methDen isEqualToString:@"13"]) || ([sharedData.methDen isEqualToString:@"11"]))) && ([kp isEqualToString:@"depthDen"]))
        {
            [self disableMe:nil];
        }
        else if ([kp isEqualToString:@"depthDen"])
        {
            [self enableMe:nil initValue:@"1"];
            sharedData.distDen = @"1";
        }
        
        // Position lo chiede solo nel caso A1 ecc. e se inst2 diverso da En Bandejas che ha codice 6, quindi nego
        if (!((([sharedData.methDen isEqualToString:@"5"]) || ([sharedData.methDen isEqualToString:@"6"]) || ([sharedData.methDen isEqualToString:@"7"]) || ([sharedData.methDen isEqualToString:@"8"]) || ([sharedData.methDen isEqualToString:@"1"]) || ([sharedData.methDen isEqualToString:@"9"]) || ([sharedData.methDen isEqualToString:@"2"]) || ([sharedData.methDen isEqualToString:@"10"])) && (!([sharedData.s2 isEqualToString:@"6"]))) && ([kp isEqualToString:@"positionDen"]))
        {
            [self disableMe:nil];
        }
        else if ([kp isEqualToString:@"positionDen"])
        {
            [self enableMe:nil initValue:@""];
        }
        
        // NumLayers lo chiede solo nel caso A1 ecc. e se inst2 uguale da En Bandejas che ha codice 8
        if ((([sharedData.methDen isEqualToString:@"5"]) || ([sharedData.methDen isEqualToString:@"6"]) || ([sharedData.methDen isEqualToString:@"7"]) || ([sharedData.methDen isEqualToString:@"8"]) || ([sharedData.methDen isEqualToString:@"1"]) || ([sharedData.methDen isEqualToString:@"9"]) || ([sharedData.methDen isEqualToString:@"2"]) || ([sharedData.methDen isEqualToString:@"10"])) && ([sharedData.s2 isEqualToString:@"8"]) && ([kp isEqualToString:@"numLayersDen"]))
        {
            [self enableMe:nil initValue:@""];
        }
        else if ([kp isEqualToString:@"numLayersDen"])
        {
            [self disableMe:nil];
        }
        
        // NumTrays lo chiede solo nel caso A1 ecc. se NumCores è uguale a Multi Core - No! Lo chiede per tutti i NumCores!!!!!!! Cambia solo nel calcolo cosa fa, non qua! Occhio che siamo nel ramo inst2 uguale a En Bandejas!!
        if ((([sharedData.methDen isEqualToString:@"5"]) || ([sharedData.methDen isEqualToString:@"6"]) || ([sharedData.methDen isEqualToString:@"7"]) || ([sharedData.methDen isEqualToString:@"8"]) || ([sharedData.methDen isEqualToString:@"1"]) || ([sharedData.methDen isEqualToString:@"9"]) || ([sharedData.methDen isEqualToString:@"2"]) || ([sharedData.methDen isEqualToString:@"10"])) && ([sharedData.s2 isEqualToString:@"8"]) && ([kp isEqualToString:@"numTraysDen"]))
        {
            [self enableMe:nil initValue:@""];
        }
        else if ([kp isEqualToString:@"numTraysDen"])
        {
            [self disableMe:nil];
        }
        
        // SunExpo lo chiede solo nei casi A1 ecc. e J, quindi nego
        if (!((([sharedData.methDen isEqualToString:@"5"]) || ([sharedData.methDen isEqualToString:@"6"]) || ([sharedData.methDen isEqualToString:@"7"]) || ([sharedData.methDen isEqualToString:@"8"]) || ([sharedData.methDen isEqualToString:@"1"]) || ([sharedData.methDen isEqualToString:@"9"]) || ([sharedData.methDen isEqualToString:@"2"]) || ([sharedData.methDen isEqualToString:@"10"]) || ([sharedData.methDen isEqualToString:@"12"]))) && ([kp isEqualToString:@"sunExpoDen"]))
        {
            [self disableMe:nil];
        }
        else if ([kp isEqualToString:@"sunExpoDen"])
        {
            [self enableMe:nil initValue:@"NO"];
        }
        
        // NumCables lo chiede solo nei casi D1 D2 H I, quindi lo disattivo negli altri (e includo anche J) - Occhio che forse diventa NumCablesPerTray nel caso A1 ecc non NultiCore!!!!!!!!!!!!
        if ((([sharedData.methDen isEqualToString:@"5"]) || ([sharedData.methDen isEqualToString:@"6"]) || ([sharedData.methDen isEqualToString:@"7"]) || ([sharedData.methDen isEqualToString:@"8"]) || ([sharedData.methDen isEqualToString:@"1"]) || ([sharedData.methDen isEqualToString:@"9"]) || ([sharedData.methDen isEqualToString:@"2"]) || ([sharedData.methDen isEqualToString:@"10"]) || ([sharedData.methDen isEqualToString:@"12"])) && ([kp isEqualToString:@"numCablesDen"]))
        {
            [self disableMe:nil];
        }
        else if ([kp isEqualToString:@"numCablesDen"])
        {
            [self enableMe:nil initValue:@""];
        }
        
        // NUmCircuits lo chiede solo nel caso A1 ecc, quindi lo disattivo negli altri (e includo anche J)
        if ((([sharedData.methDen isEqualToString:@"3"]) || ([sharedData.methDen isEqualToString:@"4"]) || ([sharedData.methDen isEqualToString:@"11"]) || ([sharedData.methDen isEqualToString:@"13"]) || ([sharedData.methDen isEqualToString:@"12"])) && ([kp isEqualToString:@"numCircuitsDen"]))
        {
            [self disableMe:nil];
        }
        else if ([kp isEqualToString:@"nunCircuitsDen"])
        {
            [self enableMe:nil initValue:@""];
        }
        
        // Distance la chiede solo nel caso H e I e cablesInConduit > 1 quindi nego
        if (!((([sharedData.methDen isEqualToString:@"13"]) || ([sharedData.methDen isEqualToString:@"11"]))) && ([kp isEqualToString:@"distDen"]))
        {
            [self disableMe:nil];
        }
        else if (([kp isEqualToString:@"distDen"]) && ([sharedData.numCablesDen floatValue] > 1))
        {
            [self enableMe:nil initValue:@"0"];
            sharedData.distDen = @"0";
        }
        
        // Grouping lo chiede solo nel caso inst2 diverso da En Bandejas e solo nel caso A1 ecc.
        if ((([sharedData.methDen isEqualToString:@"5"]) || ([sharedData.methDen isEqualToString:@"6"]) || ([sharedData.methDen isEqualToString:@"7"]) || ([sharedData.methDen isEqualToString:@"8"]) || ([sharedData.methDen isEqualToString:@"1"]) || ([sharedData.methDen isEqualToString:@"9"]) || ([sharedData.methDen isEqualToString:@"2"]) || ([sharedData.methDen isEqualToString:@"10"])) && (!([sharedData.s2 isEqualToString:@"8"])) && ([kp isEqualToString:@"groupingDen"]))
        {
            [self enableMe:nil initValue:@""];
            sharedData.distDen = @"";
        }
        else if ([kp isEqualToString:@"groupingDen"])
        {
            [self disableMe:nil];
        }
                
        // Depth la chiede solo nel caso H e I, quindi nego
        /*if (!(((([sharedData.methDen isEqualToString:@"13"]) || ([sharedData.methDen isEqualToString:@"11"]))) && ([kp isEqualToString:@"depthDen"])))
        {
            [self disableMe:nil];
        }
        else if ([kp isEqualToString:@"depthDen"])
        {
            [self enableMe:nil initValue:@"1"];
            sharedData.distDen = @"1";
        }*/
        
        if ([kp isEqualToString:@"numCoreDen"])
        {
            /*NSArray * items = [_dictionary objectForKey:kDictKeyPopupValues];
             NSMutableArray *items1 = [[NSMutableArray alloc]initWithArray:items copyItems:YES];
             
             if ([sharedData.sNumCores isEqualToString:@"Multi Core"])
             {
             // [[items objectAtIndex:0] stringWithString:@"-"];
             // [[items objectAtIndex:1] stringWithString:@"Multi"];
             }
             else if ([sharedData.sNumCores isEqualToString:@"Single Core"])
             {
             [items1 replaceObjectAtIndex:0 withObject:@"Single"];
             [items1 replaceObjectAtIndex:1 withObject:@"-"];
             // DEN E ora?
             StartPageViewController *viewContr = [[StartPageViewController alloc]init];
             [viewContr myInitWithArray:items1];
             }
             else
             {
             // [[items objectAtIndex:0] stringWithString:@"Single"];
             // [[items objectAtIndex:1] stringWithString:@"Multi"];
             }*/
            
        }
    }
}

- (void) emptyMe:(NSNotification *)notification
{
    float I, P, S;
    float C=[sharedData.cosenoDen floatValue];
    NSArray *myArray = [sharedData.voltageDen componentsSeparatedByString:@"-"];
    float V;
    if ([myArray count] > 1) V=[[myArray objectAtIndex:1] floatValue];
    else V=[[myArray objectAtIndex:0] floatValue];
    int PERC = [sharedData.perfDen intValue];
    
    if (((C == 0) || (V == 0)) && (([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen1"]) || ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen2"]) || ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen3"])))
    {
        self.textField.text = @"";
        [sharedData setValue:self.textField.text forKeyPath:[_dictionary objectForKey:kDictKeyKeypath]];
    }
    else if ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen1"])
    {
        if (!([sharedData.intensDen2 isEqualToString:@""]))
        {
            P=[sharedData.intensDen2 floatValue];
            I = (P / (V*C))*1000; // *(PERC/100.0);
            sharedData.intensDen1 = [NSString stringWithFormat:@"%f", I];
            self.textField.text = [NSString stringWithFormat:@"%f", I];
        }
        else if (!([sharedData.intensDen3 isEqualToString:@""]))
        {
            S=[sharedData.intensDen3 floatValue];
            I = (S / V)*1000; // *(PERC/100.0);
            sharedData.intensDen1 = [NSString stringWithFormat:@"%f", I];
            self.textField.text = [NSString stringWithFormat:@"%f", I];
        }
        else self.textField.text = @"";
    }
    else if ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen2"])
    {
        if (!([sharedData.intensDen1 isEqualToString:@""]))
        {
            I=[sharedData.intensDen1 floatValue];
            P = (V * I * C)/1000; // *(PERC/100.0);
            sharedData.intensDen2 = [NSString stringWithFormat:@"%f", P];
            self.textField.text = [NSString stringWithFormat:@"%f", P];
        }
        else if (!([sharedData.intensDen3 isEqualToString:@""]))
        {
            S=[sharedData.intensDen3 floatValue];
            P = (S * C); // *(PERC/100.0);
            sharedData.intensDen2 = [NSString stringWithFormat:@"%f", P];
            self.textField.text = [NSString stringWithFormat:@"%f", P];
        }
        else self.textField.text = @"";
    }
    else if ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen3"])
    {
        if (!([sharedData.intensDen2 isEqualToString:@""]))
        {
            P=[sharedData.intensDen2 floatValue];
            S = (P / C); // *(PERC/100.0);
            sharedData.intensDen3 = [NSString stringWithFormat:@"%f", S];
            self.textField.text = [NSString stringWithFormat:@"%f", S];
        }
        else if (!([sharedData.intensDen1 isEqualToString:@""]))
        {
            I=[sharedData.intensDen1 floatValue];
            S = (I * V)/1000; // *(PERC/100.);
            sharedData.intensDen3 = [NSString stringWithFormat:@"%f", S];
            self.textField.text = [NSString stringWithFormat:@"%f", S];
        }
        else self.textField.text = @"";
    }
    else if (([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"cosenoDen"]) || ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"perfDen"]))
    {
        I = [sharedData.intensDen1 floatValue] * (PERC/100.0);
        P = [sharedData.intensDen2 floatValue] * (PERC/100.0);
        S = [sharedData.intensDen3 floatValue] * (PERC/100.0);
        
        sharedData.intensDen1 = [NSString stringWithFormat:@"%f", I];
        sharedData.intensDen2 = [NSString stringWithFormat:@"%f", P];
        sharedData.intensDen3 = [NSString stringWithFormat:@"%f", S];
        
        // [self.controller.tableView reloadData];
        
        // [sharedData setValue:self.textField.text forKeyPath:[_dictionary objectForKey:kDictKeyKeypath]];
    }
    else
    {
        self.textField.text = @"";
        [sharedData setValue:self.textField.text forKeyPath:[_dictionary objectForKey:kDictKeyKeypath]];
    }
}

- (void) disableMe:(NSNotification *)notification
{
    /*float I, P, S;
    float C=[sharedData.cosenoDen floatValue];
    NSArray *myArray = [sharedData.voltageDen componentsSeparatedByString:@"-"];
    float V;
    if ([myArray count] > 1) V=[[myArray objectAtIndex:1] floatValue];
    else V=[[myArray objectAtIndex:0] floatValue];
    int PERC = [sharedData.perfDen intValue];
    
    if (((C == 0) || (V == 0)) && (([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen1"]) || ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen2"]) || ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen3"])))
    {
        NSLog(@"CASO 0 1");
        self.textField.text = @"";
        [sharedData setValue:self.textField.text forKeyPath:[_dictionary objectForKey:kDictKeyKeypath]];
    }
    else if ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen1"])
    {
        if (!([sharedData.intensDen2 isEqualToString:@""]))
        {
            NSLog(@"CASO 1 1");
            P=[sharedData.intensDen2 floatValue];
            I = (P / (V*C))*(PERC/100.0);
            sharedData.intensDen1 = [NSString stringWithFormat:@"%f", I];
            self.textField.text = [NSString stringWithFormat:@"%f", I];
        }
        else if (!([sharedData.intensDen3 isEqualToString:@""]))
        {
            NSLog(@"CASO 1 2");
            S=[sharedData.intensDen3 floatValue];
            I = (S / V)*(PERC/100.0);
            sharedData.intensDen1 = [NSString stringWithFormat:@"%f", I];
            self.textField.text = [NSString stringWithFormat:@"%f", I];
        }
        else self.textField.text = @"";
    }
    else if ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen2"])
    {
        if (!([sharedData.intensDen1 isEqualToString:@""]))
        {
            NSLog(@"CASO 2 1");
            I=[sharedData.intensDen1 floatValue];
            P = (V * I * C)*(PERC/100.0);
            sharedData.intensDen2 = [NSString stringWithFormat:@"%f", P];
            self.textField.text = [NSString stringWithFormat:@"%f", P];
        }
        else if (!([sharedData.intensDen3 isEqualToString:@""]))
        {
            NSLog(@"CASO 2 2");
            S=[sharedData.intensDen3 floatValue];
            P = (S * C)*(PERC/100.0);
            sharedData.intensDen2 = [NSString stringWithFormat:@"%f", P];
            self.textField.text = [NSString stringWithFormat:@"%f", P];
        }
        else self.textField.text = @"";
    }
    else if ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen3"])
    {
        if (!([sharedData.intensDen2 isEqualToString:@""]))
        {
            NSLog(@"CASO 3 1");
            P=[sharedData.intensDen2 floatValue];
            S = (P / C)*(PERC/100.0);
            sharedData.intensDen3 = [NSString stringWithFormat:@"%f", S];
            self.textField.text = [NSString stringWithFormat:@"%f", S];
        }
        else if (!([sharedData.intensDen1 isEqualToString:@""]))
        {
            NSLog(@"CASO 3 2");
            I=[sharedData.intensDen1 floatValue];
            S = (I * V)*(PERC/100.);
            sharedData.intensDen3 = [NSString stringWithFormat:@"%f", S];
            self.textField.text = [NSString stringWithFormat:@"%f", S];
        }
        else self.textField.text = @"";
    }*/
    
    /*if ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen1"])
    {
        self.textField.text = sharedData.intensDen1;
        // self.textField.enabled = NO;
    }
    else if ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen2"])
    {
        self.textField.text = sharedData.intensDen2;
        // self.textField.enabled = NO;
    }
    else if ([[_dictionary objectForKey:kDictKeyKeypath] isEqualToString:@"intensDen3"])
    {
        self.textField.text = sharedData.intensDen3;
        // self.textField.enabled = NO;
    }*/
    
    self.textField.text = @"";
    self.textField.enabled = NO;
}

- (void) enableMe:(NSNotification *)notification initValue:(NSString *)initValue
{
    self.textField.text = initValue;
    self.textField.enabled = YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    //    NSLog(@"%lu textFieldDidBeginEditing", (unsigned long) _row);
    
    if ([[self.dictionary objectForKey:kDictKeyType] isEqualToString:kTypePOPUP])
    {
        [self.controller performSelectorOnMainThread:@selector(hideKeyboard) withObject:nil waitUntilDone:NO];
        [self createUIAlertController];
    }
    else if ([[self.dictionary objectForKey:kDictKeyType]isEqualToString:kTypeYESNO])
    {
        NSString * title = [_dictionary objectForKey:kDictKeyTitle];
        [self.controller performSelectorOnMainThread:@selector(hideKeyboard) withObject:nil waitUntilDone:NO];
        [self createUIAlertController:@[@"YES", @"NO"] titleAlert:title messageAlert:@"" ];
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    NSString * kp = [_dictionary objectForKey:kDictKeyKeypath];
    
    if (kp && [[self.dictionary objectForKey:kDictKeyType] isEqualToString:@"VALUE"])
    {
        [sharedData setValue:textField.text forKeyPath:kp];
        NSArray * disableItems = [self.dictionary objectForKey:kDictKeyDisableItems];
        if (disableItems)
        {
            for (NSString * itemToDisable in disableItems)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:kDisableNotificationName, itemToDisable] object:nil];
            }
        }
    }
    
    /*float I, P, S;
    float C=[sharedData.cosenoDen floatValue];
    NSArray *myArray = [sharedData.voltageDen componentsSeparatedByString:@"-"];
    float V;
    if ([myArray count] > 1) V=[[myArray objectAtIndex:1] floatValue];
    else V=[[myArray objectAtIndex:0] floatValue];
    int PERC = [sharedData.perfDen intValue];
    
    if (((C == 0) || (V == 0)) && (([kp isEqualToString:@"intensDen1"]) || ([kp isEqualToString:@"intensDen2"]) || ([kp isEqualToString:@"intensDen3"])))
    {
        NSLog(@"CASO 0 1");
        textField.text = @"";
        [sharedData setValue:self.textField.text forKeyPath:[_dictionary objectForKey:kDictKeyKeypath]];
    }
    else if ([kp isEqualToString:@"intensDen1"])
    {
        if (!([sharedData.intensDen2 isEqualToString:@""]))
        {
            NSLog(@"CASO 1 1");
            P=[sharedData.intensDen2 floatValue];
            I = (P / (V*C))*(PERC/100.0);
            sharedData.intensDen1 = [NSString stringWithFormat:@"%f", I];
            textField.text = [NSString stringWithFormat:@"%f", I];
        }
        else if (!([sharedData.intensDen3 isEqualToString:@""]))
        {
            NSLog(@"CASO 1 2");
            S=[sharedData.intensDen3 floatValue];
            I = (S / V)*(PERC/100.0);
            sharedData.intensDen1 = [NSString stringWithFormat:@"%f", I];
            textField.text = [NSString stringWithFormat:@"%f", I];
        }
        else { NSLog(@"CASO 3 3");textField.text = @""; }
    }
    else if ([kp isEqualToString:@"intensDen2"])
    {
        if (!([sharedData.intensDen1 isEqualToString:@""]))
        {
            NSLog(@"CASO 2 1");
            I=[sharedData.intensDen1 floatValue];
            P = (V * I * C)*(PERC/100.0);
            sharedData.intensDen2 = [NSString stringWithFormat:@"%f", P];
            textField.text = [NSString stringWithFormat:@"%f", P];
        }
        else if (!([sharedData.intensDen3 isEqualToString:@""]))
        {
            NSLog(@"CASO 2 2");
            S=[sharedData.intensDen3 floatValue];
            P = (S * C)*(PERC/100.0);
            sharedData.intensDen2 = [NSString stringWithFormat:@"%f", P];
            textField.text = [NSString stringWithFormat:@"%f", P];
        }
        else { NSLog(@"CASO 3 3"); textField.text = @""; }
    }
    else if ([kp isEqualToString:@"intensDen3"])
    {
        if (!([sharedData.intensDen2 isEqualToString:@""]))
        {
            NSLog(@"CASO 3 1");
            P=[sharedData.intensDen2 floatValue];
            S = (P / C)*(PERC/100.0);
            sharedData.intensDen3 = [NSString stringWithFormat:@"%f", S];
            textField.text = [NSString stringWithFormat:@"%f", S];
        }
        else if (!([sharedData.intensDen1 isEqualToString:@""]))
        {
            NSLog(@"CASO 3 2");
            I=[sharedData.intensDen1 floatValue];
            S = (I * V)*(PERC/100.);
            sharedData.intensDen3 = [NSString stringWithFormat:@"%f", S];
            textField.text = [NSString stringWithFormat:@"%f", S];
        }
        else { NSLog(@"CASO 3 3"); textField.text = @""; }
    }
    else*/ if ([kp isEqualToString:@"perfDen"])
    {
        // sharedData.perfDen = textField.text;
        [self emptyMe:nil];
        // [self.controller.tableView reloadData];
    }
}


- (void) createUIAlertController
{
    NSArray * items = [_dictionary objectForKey:kDictKeyPopupValues];
    NSString * title = [_dictionary objectForKey:kDictKeyTitle];
    
    if (items)
    {
        [self createUIAlertController:items titleAlert:title messageAlert:@""];
    }
    else
    {
        NSLog(@"Manca la sorgente dei dati...");
    }
    
}

- (void) createUIAlertController:(NSArray *)arrayAlert titleAlert:(NSString *)titleAlert messageAlert:(NSString *)messageAlert
{
    
    alertController = [UIAlertController alertControllerWithTitle:titleAlert
                                                          message:messageAlert
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    for (int i = 0; i < [arrayAlert count]; i++)
    {
        NSString * title = [arrayAlert objectAtIndex:i];
        
        //---------------------------------------------------------------------------
        // DEN Check Coseno e cambia gli altri valori
        //---------------------------------------------------------------------------
        if ([titleAlert isEqualToString:@"Select Coseno Phi"])
        {
            [self emptyMe:nil];
            // [self.controller.tableView reloadData];
        }
        
        //---------------------------------------------------------------------------
        // DEN Check NumCores
        //---------------------------------------------------------------------------
        if ([sharedData.sNumCores isEqualToString:@"Multi"])
        {
            if ([title isEqualToString:@"Single"]) title=@"-";
            else if ([title isEqualToString:@"Multi"]) title=@"Multi";
            else if ([title isEqualToString:@"Single(B)"]) title=@"-";
            else if ([title isEqualToString:@"Multi(B)"]) title=@"-";
        }
        else if ([sharedData.sNumCores isEqualToString:@"Single"])
        {
            if ([title isEqualToString:@"Single"]) title=@"Single";
            else if ([title isEqualToString:@"Multi"]) title=@"-";
            else if ([title isEqualToString:@"Single(B)"]) title=@"-";
            else if ([title isEqualToString:@"Multi(B)"]) title=@"-";
        }
        else if ([sharedData.sNumCores isEqualToString:@"Multi-Single"])
        {
            if ([title isEqualToString:@"Single"]) title=@"Single";     // Pensavo che si dovesse differenziare anche a livello del menu, ma basta nella variabile sNumCores
            else if ([title isEqualToString:@"Multi"]) title=@"Multi";
            else if ([title isEqualToString:@"Single(B)"]) title=@"-";
            else if ([title isEqualToString:@"Multi(B)"]) title=@"-";
        }
        else if ([sharedData.sNumCores isEqualToString:@"Single-Multi"])
        {
            if ([title isEqualToString:@"Single"]) title=@"Single";
            else if ([title isEqualToString:@"Multi"]) title=@"Multi";
            else if ([title isEqualToString:@"Single(B)"]) title=@"-";
            else if ([title isEqualToString:@"Multi(B)"]) title=@"-";
        }
        else
        {
            if ([title isEqualToString:@"Single"]) title=@"-";
            else if ([title isEqualToString:@"Multi"]) title=@"-";
            else if ([title isEqualToString:@"Single(B)"]) title=@"Single(B)";
            else if ([title isEqualToString:@"Multi(B)"]) title=@"Multi(B)";
        }
        
        //---------------------------------------------------------------------------
        // DEN Check Current Type
        //---------------------------------------------------------------------------
        if ((([sharedData.methDen isEqualToString:@"13"]) || ([sharedData.methDen isEqualToString:@"11"]) || ([sharedData.methDen isEqualToString:@"12"])))  // H, I e J
        {
            if ([title isEqualToString:@"AC-Monophasic"]) title=@"-";
            else if ([title isEqualToString:@"Direct-Current"]) title=@"-";
            else if ([title isEqualToString:@"AC-Threephasic"]) title=@"AC-Threephasic";
        }
        else
        {
            if ([sharedData.restrictionDen isEqualToString:@"1"]) // NO
            {
                if ([title isEqualToString:@"AC-Monophasic"]) title=@"AC-Monophasic";
                else if ([title isEqualToString:@"Direct-Current"]) title=@"Direct-Current";
                else if ([title isEqualToString:@"AC-Threephasic"]) title=@"AC-Threephasic";
            }
            else if ([sharedData.restrictionDen isEqualToString:@"2"]) // Solo XLPE2
            {
                if ([title isEqualToString:@"AC-Monophasic"]) title=@"AC-Monophasic";
                else if ([title isEqualToString:@"Direct-Current"]) title=@"Direct-Current";
                else if ([title isEqualToString:@"AC-Threephasic"]) title=@"-";
            }
            else
            {
                if ([title isEqualToString:@"AC-Monophasic"]) title=@"-";
                else if ([title isEqualToString:@"Direct-Current"]) title=@"-";
                else if ([title isEqualToString:@"AC-Threephasic"]) title=@"AC-Threephasic";
            }
        }
        
        //---------------------------------------------------------------------------
        // DEN Check Voltage according to Current Type
        //---------------------------------------------------------------------------
        if ([sharedData.currTypeDen isEqualToString:@"AC-Threephasic"])
        {
            if ([title isEqualToString:@"127-220"]) title=@"127-220";
            else if ([title isEqualToString:@"132-230"]) title=@"132-230";
            else if ([title isEqualToString:@"220-380"]) title=@"220-380";
            else if ([title isEqualToString:@"230-400"]) title=@"230-400";
            else if ([title isEqualToString:@"127"]) title=@"-";
            else if ([title isEqualToString:@"132"]) title=@"-";
            else if ([title isEqualToString:@"220"]) title=@"-";
            else if ([title isEqualToString:@"230"]) title=@"-";
            else if ([title isEqualToString:@"DC-110"]) title=@"-";
            else if ([title isEqualToString:@"DC-220"]) title=@"-";
            else if ([title isEqualToString:@"Oher"]) title=@"Other";
        }
        else if ([sharedData.currTypeDen isEqualToString:@"AC-Monophasic"])
        {
            if ([title isEqualToString:@"127-220"]) title=@"-";
            else if ([title isEqualToString:@"132-230"]) title=@"-";
            else if ([title isEqualToString:@"220-380"]) title=@"-";
            else if ([title isEqualToString:@"230-400"]) title=@"-";
            else if ([title isEqualToString:@"127"]) title=@"127";
            else if ([title isEqualToString:@"132"]) title=@"132";
            else if ([title isEqualToString:@"220"]) title=@"220";
            else if ([title isEqualToString:@"230"]) title=@"230";
            else if ([title isEqualToString:@"DC-110"]) title=@"-";
            else if ([title isEqualToString:@"DC-220"]) title=@"-";
            else if ([title isEqualToString:@"Oher"]) title=@"Other";
        }
        else if ([sharedData.currTypeDen isEqualToString:@"Direct-Current"])
        {
            if ([title isEqualToString:@"127-220"]) title=@"-";
            else if ([title isEqualToString:@"132-230"]) title=@"-";
            else if ([title isEqualToString:@"220-380"]) title=@"-";
            else if ([title isEqualToString:@"230-400"]) title=@"-";
            else if ([title isEqualToString:@"127"]) title=@"-";
            else if ([title isEqualToString:@"132"]) title=@"-";
            else if ([title isEqualToString:@"220"]) title=@"-";
            else if ([title isEqualToString:@"230"]) title=@"-";
            else if ([title isEqualToString:@"DC-110"]) title=@"DC-110";
            else if ([title isEqualToString:@"DC-220"]) title=@"DC-220";
            else if ([title isEqualToString:@"Oher"]) title=@"Other";
        }
        else
        {
            if ([title isEqualToString:@"127-220"]) title=@"127-220";
            else if ([title isEqualToString:@"132-230"]) title=@"132-230";
            else if ([title isEqualToString:@"220-380"]) title=@"220-380";
            else if ([title isEqualToString:@"230-400"]) title=@"230-400";
            else if ([title isEqualToString:@"127"]) title=@"127";
            else if ([title isEqualToString:@"132"]) title=@"132";
            else if ([title isEqualToString:@"220"]) title=@"220";
            else if ([title isEqualToString:@"230"]) title=@"230";
            else if ([title isEqualToString:@"DC-110"]) title=@"DC-110";
            else if ([title isEqualToString:@"DC-220"]) title=@"DC-220";
            else if ([title isEqualToString:@"Oher"]) title=@"Other";
        }
        
        //---------------------------------------------------------------------------
        // DEN Check A B C D
        //---------------------------------------------------------------------------
        if ([titleAlert isEqualToString:@"Select Grouping"])
        {
            if (!([sharedData.s2 isEqualToString:@"8"])) // Se Installation2 non è en bandejas
            {
                float gcf;
                char aux[80];
                int ncc[] = {1,2,3,4,6,9,12,16,20};
                int i,j;
                int numCablesConduit = [sharedData.numCablesDen intValue];
                int inst1 = [sharedData.s1 intValue];
                int inst2 = [sharedData.s2 intValue];
                int inst3 = [sharedData.s3 intValue];
                char mat1[MAX_TAB_RECORD][4][40]={"INST1","INST2","INST3","Correspondent questions",
                    "14","5","8","B",
                    "14","5","16","C",
                    "14","10","","D",
                    "1","1","1","A",
                    "1","1","2","A",
                    "1","1","3","A  B  C",
                    "1","1","5","A",
                    "1","7","9","A",
                    "1","7","15","A",
                    "2","1","1","A",
                    "2","1","2","A",
                    "2","1","3","A  B  C",
                    "2","1","5","A",
                    "2","7","9","A",
                    "2","7","15","A",
                    "3","1","1","A",
                    "3","1","2","A",
                    "3","1","3","A  B  C",
                    "3","1","5","A",
                    "3","4","6","A",
                    "3","4","7","A",
                    "3","5","8","A  B",
                    "3","5","16","A  C",
                    "3","7","9","A",
                    "3","7","15","A",
                    "3","8","11","(diferent factors, see tray tables)",
                    "3","8","13","(diferent factors, see tray tables)",
                    "3","8","12","(diferent factors, see tray tables)",
                    "3","10","","D",
                    "3","11","","No correction factor necessary",
                    "3","12","","A",
                    "4","1","1","A",
                    "4","1","2","A",
                    "4","1","3","A  B  C",
                    "4","1","5","A",
                    "5","1","1","A",
                    "5","1","2","A",
                    "5","1","3","A  B  C",
                    "5","1","5","A",
                    "5","5","8","A  B",
                    "5","5","16","A  C",
                    "5","7","9","A",
                    "5","7","15","A",
                    "5","8","11","(diferent factors, see tray tables)",
                    "5","8","13","(diferent factors, see tray tables)",
                    "5","8","12","(diferent factors, see tray tables)",
                    "6","1","1","A",
                    "6","1","2","A",
                    "6","1","3","A  B  C",
                    "6","1","5","A",
                    "6","4","6","A",
                    "6","4","7","A",
                    "6","5","8","A  B",
                    "6","5","16","A  C",
                    "6","7","9","A",
                    "6","7","15","A",
                    "6","8","11","(diferent factors, see tray tables)",
                    "6","8","13","(diferent factors, see tray tables)",
                    "6","8","12","(diferent factors, see tray tables)",
                    "6","9","9","A",
                    "6","9","15","A",
                    "7","2","1","A",
                    "7","2","2","A",
                    "7","2","3","A  B  C",
                    "7","2","5","A",
                    "7","3","","A",
                    "7","5","8","A  B",
                    "7","5","16","A  C",
                    "7","7","9","A",
                    "7","7","15","A",
                    "8","2","1","A",
                    "8","2","2","A",
                    "8","2","3","A  B  C",
                    "8","2","5","A",
                    "8","3","","A",
                    "9","2","1","A",
                    "9","2","2","A",
                    "9","2","3","A  B  C",
                    "9","2","5","A",
                    "9","3","","A",
                    "10","1","1","A",
                    "10","1","2","A",
                    "10","1","3","A  B  C",
                    "10","1","5","A",
                    "10","4","6","A",
                    "10","4","7","A",
                    "10","5","8","A  B",
                    "10","5","16","A  C",
                    "10","7","9","A",
                    "10","7","15","A",
                    "10","8","11","(diferent factors, see tray tables)",
                    "10","8","13","(diferent factors, see tray tables)",
                    "10","8","12","(diferent factors, see tray tables)",
                    "10","10","","D",
                    "11","2","1","A",
                    "11","2","2","A",
                    "11","2","3","A  B  C",
                    "11","2","5","A",
                    "11","5","8","A  B",
                    "11","5","16","A  C",
                    "12","1","1","A",
                    "12","1","2","A",
                    "12","1","3","A  B  C",
                    "12","1","5","A",
                    "12","4","6","A",
                    "12","4","7","A",
                    "12","5","8","A  B",
                    "12","5","16","A  C",
                    "12","7","9","A",
                    "12","7","15","A",
                    "12","8","11","(diferent factors, see tray tables)",
                    "12","8","13","(diferent factors, see tray tables)",
                    "12","8","12","(diferent factors, see tray tables)",
                    "12","10","","D",
                    "12","11","","No correction factor necessary",
                    "12","12","","A",
                    "13","1","1","A",
                    "13","1","2","A",
                    "13","1","3","A  B  C",
                    "13","1","5","A",
                    "13","4","6","A",
                    "13","4","7","A",
                    "13","5","8","A  B",
                    "13","5","16","A  C" };
                char mat2[5][10][40] = { "1","2","3","4","6","9","12","16","20","X",
                    "1","0.8","0.7","0.7","0.55","0.5","0.45","0.4","0.4","A",
                    "1","0.85","0.8","0.75","0.7","0.7","0.7","0.7","0.7","B",
                    "0.95","0.8","0.7","0.7","0.65","0.6","0.6","0.6","0.6","C",
                    "1.00","0.89","0.80","0.75","0.75","0.75","0.75","0.75","0.75","D" };
                
                j=0;
                for(i=0;i<9;i++)  if (numCablesConduit == ncc[i]) { j=i; break; }
                
                // Asks Number of Cables in Conduit, Distance between Cables and Position
                
                // Qua usa le tabelle grandi: Pag4_smallTable.csv e Pag4_bigTable.csv
                // cfTab = searchTable("Pag4_bigTable.csv");
                for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe saltare i nomi dei campi
                {
                    // fprintf(stderr,"A1 ecc. BigTable - %d %d %d %d %d %d\n",atoi(mat[cfTab][i][0]),atoi(mat[cfTab][i][1]),atoi(mat[cfTab][i][2]),inst1,inst2,inst3);
                    // if ((inst1 == strtod(mat[cfTab][i][0],NULL)) && (inst2 == strtod(mat[cfTab][i][1],NULL)) && (inst3 == strtod(mat[cfTab][i][2],NULL))) { strcpy(aux,mat[cfTab][i][3]); aux[1]='\0'; break; }
                    if ((inst1 == atoi(mat1[i][0])) && (inst2 == atoi(mat1[i][1])) && (inst3 == atoi(mat1[i][2]))) { strcpy(aux,mat1[i][3]); aux[1]='\0'; break; }
                }
                // fprintf(stderr,"A1 ecc. BigTable - TabIndex = 0  Index = %d %d aux = %s\n",i,j,aux);
                
                // cfTab = searchTable("Pag4_smallTable.csv");
                // for(i=1;i<MAX_TAB_RECORD;i++) // Inizio da 1 pe saltare i nomi dei campi
                // {
                //    if (strcmp(aux,mat2[i][9]) == 0) { gcf = strtof(mat2[i][j],NULL); break; }
                // }
                // fprintf(stderr,"A1 ecc. SmallTable - TabIndex = 1  Index = %d %d GCF = %f\n",i,j,gcf);
                
                if ((strcmp(aux,"A") == 0) || (strcmp(aux,"B") == 0) || (strcmp(aux,"C") == 0) || (strcmp(aux,"D") == 0))
                {
                    if (([title isEqualToString:@"A-Agrupados_al_aire"]) && ((strcmp(aux,"A") == 0))) title = @"A-Agrupados_al_aire";
                    // else title = @"-";
                    else if (([title isEqualToString:@"B-Capa_unica_sobre_muro"]) && ((strcmp(aux,"B") == 0))) title = @"B-Capa_unica_sobre_muro";
                    // else title = @"-";
                    else if (([title isEqualToString:@"C-Capa_unica_fijiada"]) && ((strcmp(aux,"C") == 0))) title = @"C-Capa_unica_fijiadae";
                    // else title = @"-";
                    else if (([title isEqualToString:@"D-Suspendido"]) && ((strcmp(aux,"D") == 0))) title = @"D-Suspendido";
                    // else title = @"-";
                    else if (([title isEqualToString:@"X-Error"])) title = @"-";
                    else title = @"-";
                }
                else
                {
                    if (([title isEqualToString:@"A-Agrupados_al_aire"])) title = @"-";
                    else if (([title isEqualToString:@"B-Capa_unica_sobre_muro"])) title = @"-";
                    else if (([title isEqualToString:@"C-Capa_unica_fijiada"])) title = @"-";
                    else if (([title isEqualToString:@"D-Suspendido"])) title = @"-";
                    else if (([title isEqualToString:@"X-Error"])) title = @"X-Error";
                }
            }
        }
        
        
        
        
        //---------------------------------------------------------------------------
        // DEN Check Various - 1 spazio per circuits, 2 spazi per cables, 3 spazi
        // per layers, R per res, T per temp, CIT, CRT depth, distance,
        //---------------------------------------------------------------------------
        if ((([sharedData.methDen isEqualToString:@"3"])) || ([sharedData.methDen isEqualToString:@"4"]))  // D1 e D2
        {
            // Qua devo mettere T Default 25
            // NSString * kp = [_dictionary objectForKey:kDictKeyKeypath];
            // if ([kp isEqualToString:@"tempDen"]) title = @"25";
            
            // Qua devo mettere R Default 2.5
            
            if (([sharedData.methDen isEqualToString:@"3"]))  // D1
            {
                if ([title isEqualToString:@"  1  "]) title=@"  1  ";
                else if ([title isEqualToString:@"  2  "]) title=@"  2  ";
                else if ([title isEqualToString:@"  3  "]) title=@"  3  ";
                else if ([title isEqualToString:@"  4  "]) title=@"  4  ";
                else if ([title isEqualToString:@"  5  "]) title=@"  5  ";
                else if ([title isEqualToString:@"  6  "]) title=@"  6  ";
                else if ([title isEqualToString:@"  7  "]) title=@"  7  ";
                else if ([title isEqualToString:@"  8  "]) title=@"  8  ";
                else if ([title isEqualToString:@"  9  "]) title=@"  9  ";
                else if ([title isEqualToString:@"  10  "]) title=@"  10  ";
                else if ([title isEqualToString:@"  11  "]) title=@"  11  ";
                else if ([title isEqualToString:@"  12  "]) title=@"  12  ";
                else if ([title isEqualToString:@"  13  "]) title=@"  13  ";
                else if ([title isEqualToString:@"  14  "]) title=@"  14  ";
                else if ([title isEqualToString:@"  15  "]) title=@"  15  ";
                else if ([title isEqualToString:@"  16  "]) title=@"  16  ";
                else if ([title isEqualToString:@"  17  "]) title=@"  17  ";
                else if ([title isEqualToString:@"  18  "]) title=@"  18  ";
                else if ([title isEqualToString:@"  19  "]) title=@"  19  ";
                else if ([title isEqualToString:@"  20  "]) title=@"  20  ";
                
                if (!([sharedData.numCablesDen isEqualToString:@"1"])) // Che è come dire che numCablesInConduit > 1
                {
                    if ([title isEqualToString:@"0m"]) title=@"0m";
                    else if ([title isEqualToString:@"Diameter"]) title=@"-";
                    else if ([title isEqualToString:@"0.125m"]) title=@"-";
                    else if ([title isEqualToString:@"0.2m"]) title=@"-";
                    else if ([title isEqualToString:@"0.25m"]) title=@"0.25m";
                    else if ([title isEqualToString:@"0.4m"]) title=@"-";
                    else if ([title isEqualToString:@"0.5m"]) title=@"0.5m";
                    else if ([title isEqualToString:@"0.6m"]) title=@"-";
                    else if ([title isEqualToString:@"0.8m"]) title=@"-";
                    else if ([title isEqualToString:@"1m"]) title=@"1m";
                }
                
                // Qua devo mettere il Check sul CIT
                if (([sharedData.armoredDen isEqualToString:@"1"]) && ([sharedData.sNumCores isEqualToString:@"Single Core"])) // Armored & SingleCore
                {
                    if ([title isEqualToString:@"1-Other"]) title=@"-";
                    else if ([title isEqualToString:@"1.8-RiskExpl-Unip"]) title=@"1.8-RiskExpl-Unip";
                    else if ([title isEqualToString:@"1.25-InstBT"]) title=@"-";
                }
                else if ([sharedData.s1 isEqualToString:@"12"]) // 'ITC-BT 40', 'Instalaciones generadoras de baja tension
                {
                    if ([title isEqualToString:@"1-Other"]) title=@"1-Other";
                    else if ([title isEqualToString:@"1.8-RiskExpl-Unip"]) title=@"1.8-RiskExpl-Unip";
                    else if ([title isEqualToString:@"1.25-InstBT"]) title=@"-";
                }
                else
                {
                    if ([title isEqualToString:@"1-Other"]) title=@"1-Other";
                    else if ([title isEqualToString:@"1.8-RiskExpl-Unip"]) title=@"1.8-RiskExpl-Unip";
                    else if ([title isEqualToString:@"1.25-InstBT"]) title=@"1.25-InstBT";
                }
            }
            else
            {
                if ([title isEqualToString:@"  1  "]) title=@"  1  ";
                else if ([title isEqualToString:@"  2  "]) title=@"  2 ";
                else if ([title isEqualToString:@"  3  "]) title=@"  3  ";
                else if ([title isEqualToString:@"  4  "]) title=@"  4  ";
                else if ([title isEqualToString:@"  5  "]) title=@"  5  ";
                else if ([title isEqualToString:@"  6  "]) title=@"  6  ";
                else if ([title isEqualToString:@"  7  "]) title=@"  7  ";
                else if ([title isEqualToString:@"  8  "]) title=@"  8  ";
                else if ([title isEqualToString:@"  9  "]) title=@"  9  ";
                else if ([title isEqualToString:@"  10  "]) title=@"-";
                else if ([title isEqualToString:@"  11  "]) title=@"-";
                else if ([title isEqualToString:@"  12  "]) title=@"  12  ";
                else if ([title isEqualToString:@"  13  "]) title=@"-";
                else if ([title isEqualToString:@"  14  "]) title=@"-";
                else if ([title isEqualToString:@"  15  "]) title=@"-";
                else if ([title isEqualToString:@"  16  "]) title=@"  16  ";
                else if ([title isEqualToString:@"  17  "]) title=@"-";
                else if ([title isEqualToString:@"  18  "]) title=@"-";
                else if ([title isEqualToString:@"  19  "]) title=@"-";
                else if ([title isEqualToString:@"  20  "]) title=@"  20  ";
                
                if (!([sharedData.numCablesDen isEqualToString:@"1"])) // Che è come dire che numCablesInConduit > 1
                {
                    if ([title isEqualToString:@"0m"]) title=@"0m";
                    else if ([title isEqualToString:@"Diameter"]) title=@"Diameter";
                    else if ([title isEqualToString:@"0.125m"]) title=@"0.2m";
                    else if ([title isEqualToString:@"0.2m"]) title=@"-";
                    else if ([title isEqualToString:@"0.25m"]) title=@"0.25m";
                    else if ([title isEqualToString:@"0.4m"]) title=@"-";
                    else if ([title isEqualToString:@"0.5m"]) title=@"0.5m";
                    else if ([title isEqualToString:@"0.6m"]) title=@"-";
                    else if ([title isEqualToString:@"0.8m"]) title=@"-";
                    else if ([title isEqualToString:@"1m"]) title=@"-";
                }
                
                // Qua devo mettere il Check sul CIT
                if (([sharedData.armoredDen isEqualToString:@"1"]) && ([sharedData.sNumCores isEqualToString:@"Single Core"])) // Armored & SingleCore
                {
                    if ([title isEqualToString:@"1-Other"]) title=@"-";
                    else if ([title isEqualToString:@"1.8-RiskExpl-Unip"]) title=@"1.8-RiskExpl-Unip";
                    else if ([title isEqualToString:@"1.25-InstBT"]) title=@"-";
                }
                else if ([sharedData.s1 isEqualToString:@"12"]) // 'ITC-BT 40', 'Instalaciones generadoras de baja tension
                {
                    if ([title isEqualToString:@"1-Other"]) title=@"1-Other";
                    else if ([title isEqualToString:@"1.8-RiskExpl-Unip"]) title=@"1.8-RiskExpl-Unip";
                    else if ([title isEqualToString:@"1.25-InstBT"]) title=@"-";
                }
                else
                {
                    if ([title isEqualToString:@"1-Other"]) title=@"1-Other";
                    else if ([title isEqualToString:@"1.8-RiskExpl-Unip"]) title=@"1.8-RiskExpl-Unip";
                    else if ([title isEqualToString:@"1.25-InstBT"]) title=@"1.25-InstBT";
                }
            }
        }
        else if (([sharedData.methDen isEqualToString:@"5"]) || ([sharedData.methDen isEqualToString:@"6"]) || ([sharedData.methDen isEqualToString:@"7"]) || ([sharedData.methDen isEqualToString:@"8"]) || ([sharedData.methDen isEqualToString:@"1"]) || ([sharedData.methDen isEqualToString:@"9"]) || ([sharedData.methDen isEqualToString:@"2"]) || ([sharedData.methDen isEqualToString:@"10"]))
        {
            // Qua devo mettere T Default 40
            // NSString * kp = [_dictionary objectForKey:kDictKeyKeypath];
            // if ([kp isEqualToString:@"tempDen"]) title = @"40";
            
            // NumCircuitsInConduit è sempre lo stesso nel caso A1 ecc.
            if ([title isEqualToString:@" 1  "]) title=@"  1  ";
            else if ([title isEqualToString:@" 2 "]) title=@" 2 ";
            else if ([title isEqualToString:@" 3 "]) title=@" 3 ";
            else if ([title isEqualToString:@" 4 "]) title=@" 4 ";
            else if ([title isEqualToString:@" 5 "]) title=@" 5 ";
            else if ([title isEqualToString:@" 6 "]) title=@" 6 ";
            else if ([title isEqualToString:@" 7 "]) title=@" 7 ";
            else if ([title isEqualToString:@" 8 "]) title=@" 8 ";
            else if ([title isEqualToString:@" 9 "]) title=@" 9 ";
            else if ([title isEqualToString:@" 10 "]) title=@"-";
            else if ([title isEqualToString:@" 11 "]) title=@"-";
            else if ([title isEqualToString:@" 12 "]) title=@" 12 ";
            else if ([title isEqualToString:@" 13 "]) title=@"-";
            else if ([title isEqualToString:@" 14 "]) title=@"-";
            else if ([title isEqualToString:@" 15 "]) title=@"-";
            else if ([title isEqualToString:@" 16 "]) title=@" 16 ";
            else if ([title isEqualToString:@" 17 "]) title=@"-";
            else if ([title isEqualToString:@" 18 "]) title=@"-";
            else if ([title isEqualToString:@" 19 "]) title=@"-";
            else if ([title isEqualToString:@" 20 "]) title=@" 20 ";
        }
        else if (([sharedData.methDen isEqualToString:@"13"]) || ([sharedData.methDen isEqualToString:@"11"])) // H e I
        {
            // Qua devo mettere T Default 25
            
            // Qua devo mettere R Default 1.5
            
            // Qua devo settare il popup di R
            
            // NumCircuitsInConduit è sempre lo stesso nel caso A1 ecc.
            if ([title isEqualToString:@" 1  "]) title=@"  1  ";
            else if ([title isEqualToString:@" 2 "]) title=@" 2 ";
            else if ([title isEqualToString:@" 3 "]) title=@" 3 ";
            else if ([title isEqualToString:@" 4 "]) title=@" 4 ";
            else if ([title isEqualToString:@" 5 "]) title=@" 5 ";
            else if ([title isEqualToString:@" 6 "]) title=@" 6 ";
            else if ([title isEqualToString:@" 7 "]) title=@" 7 ";
            else if ([title isEqualToString:@" 8 "]) title=@" 8 ";
            else if ([title isEqualToString:@" 9 "]) title=@" 9 ";
            else if ([title isEqualToString:@" 10 "]) title=@"10";
            else if ([title isEqualToString:@" 11 "]) title=@"-";
            else if ([title isEqualToString:@" 12 "]) title=@"-";
            else if ([title isEqualToString:@" 13 "]) title=@"-";
            else if ([title isEqualToString:@" 14 "]) title=@"-";
            else if ([title isEqualToString:@" 15 "]) title=@"-";
            else if ([title isEqualToString:@" 16 "]) title=@"-";
            else if ([title isEqualToString:@" 17 "]) title=@"-";
            else if ([title isEqualToString:@" 18 "]) title=@"-";
            else if ([title isEqualToString:@" 19 "]) title=@"-";
            else if ([title isEqualToString:@" 20 "]) title=@"-";
        }
        else // J
        {
            // Qua devo mettere T Default 40
        }
        
        //---------------------------------------------------------------------------
        // DEN If - then skip
        //---------------------------------------------------------------------------
        if ([title isEqualToString:@"-"])
        {
             continue; // DEN per gestire le voci di popup con il trattino dinamicamente modificate sopra, in modo che non compaiano
        }

        [alertController addAction:[UIAlertAction
                                    actionWithTitle:title
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action){
                                        if (![title isEqualToString:@"-"])
                                        {
                                            if ([_dictionary objectForKey:@"fieldDisables"])
                                            {
                                                if (i == [arrayAlert count] - 1)
                                                {
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"ENA%@", [_dictionary objectForKey:kDictKeyLabel]] object:nil];
                                                }
                                                else
                                                {
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"DIS%@", [_dictionary objectForKey:kDictKeyLabel]] object:nil];
                                                }
                                            }
                                            // sharedData.s1 = [NSString stringWithFormat:@"%d",i+1];
                                            
                                            self.textField.text = arrayAlert[i];
                                            NSString * kp = [_dictionary objectForKey:kDictKeyKeypath];
                                            if (kp)
                                            {
                                                if ([[self.dictionary objectForKey:kDictKeyType] isEqualToString:kTypePOPUP])
                                                {
                                                    [sharedData setValue:[arrayAlert objectAtIndex:i] forKeyPath:kp];
                                                    
                                                    if ([titleAlert isEqualToString:@"Select NumCores"]) // Faccio il check sul campo titolo
                                                    {
                                                        NSArray *subStrings = [sharedData.sMetINT componentsSeparatedByString:@"-"];
                                                        if ([subStrings count] > 1)
                                                        {
                                                            // Devo gestire il doppio num cores corrispondente al doppio metodo
                                                            if ((i == 0) && ([sharedData.sNumCores isEqualToString:@"Multi-Single"]))
                                                            {
                                                                sharedData.numCoreDen = @"Multi"; // numCoreDen qua è quella giusta, percheè corrisponde alla Key del plist e viene poi mandta via nel WebService
                                                            }
                                                            else if ((i == 1) && ([sharedData.sNumCores isEqualToString:@"Multi-Single"]))
                                                            {
                                                                sharedData.numCoreDen = @"Single"; // numCoreDen qua è quella giusta, percheè corrisponde alla Key del plist e viene poi mandta via nel WebService
                                                            }
                                                            if (i == 0) sharedData.methDen = [subStrings objectAtIndex:0];
                                                            else sharedData.methDen = [subStrings objectAtIndex:1];
                                                        }
                                                        else sharedData.methDen = sharedData.sMetINT;
                                                    }
                                                }
                                                else if ([[self.dictionary objectForKey:kDictKeyType]isEqualToString:kTypeYESNO])
                                                {
                                                    [sharedData setValue:[NSNumber numberWithBool:[[arrayAlert objectAtIndex:i] boolValue] ] forKeyPath:kp];
                                                }
                                            }
                                        }
                                    }]];
    }
    
    UIAlertAction * cancelAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Annulla", @"Cancel action")
                                    style:UIAlertActionStyleCancel
                                    handler:^(UIAlertAction * action)
                                    {
                                        NSLog(@"Cancel action");
                                    }];
    [alertController addAction:cancelAction];
    [self.controller presentViewController:alertController animated:YES completion:nil];
    
}



@end
