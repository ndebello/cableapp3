//
//  RegistrationViewController.m
//  CableA
//
//  Created by Welington Barichello on 22/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "RegistrationViewController.h"

@interface RegistrationViewController (){
    NSDictionary* jsonCountry;
    NSMutableArray * countries;
    NSMutableArray * userType;
    UIAlertController * alertController;
}
@property (weak, nonatomic) IBOutlet UITextField *emailUser;
@property (weak, nonatomic) IBOutlet UITextField *pswUser;
@property (weak, nonatomic) IBOutlet UITextField *countryUser;
@property (weak, nonatomic) IBOutlet UITextField *typeUser;
@property (weak, nonatomic) IBOutlet UITextField *otherUserTyper;

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    countries = [NSMutableArray new];
    userType = [[NSMutableArray alloc] initWithObjects:@"installer",@"professional",@"engineer",@"consultant",@"technician",@"salesperson",@"other", nil];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"];
    [self requestCountry];
    
    // Do any additional setup after loading the view.
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)requestCountry{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://46.252.150.235:8085/api/countries.json"]];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:@"Error"
                                               message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction* yesButton = [UIAlertAction
                                             actionWithTitle:@"Ok"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action)
                                             {
                                                 [self viewDidAppear:YES];
                                             }];
                 
                 [alert addAction:yesButton];
                 [self presentViewController:alert animated:YES completion:nil];
             });
             
         }
         else
         {
             NSError* error;
             jsonCountry = [NSJSONSerialization
                                   JSONObjectWithData:data //1
                                   options:kNilOptions
                                   error:&error];
             for (NSDictionary * country in jsonCountry) {
                 [countries addObject:[[country valueForKey:@"Country"] objectForKey:@"name"]];
             }
         }
     }];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField== _countryUser) {
        [self.view endEditing:YES];
        [self createUIAlertController:countries TextField:textField TitleAlert:@"Cerca Paese" MessageAlert:@"Seleziona Paese desiderato"];
        
    }
    if (textField == _typeUser) {
        [self.view endEditing:YES];
        [self createUIAlertController:userType TextField:textField TitleAlert:@"Inserisci la tipologia di utente" MessageAlert:@"Seleziona il tipo di utente"];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)registration:(id)sender {
    if ([self validateEmail:_emailUser.text]) {
        if (![_pswUser.text isEqualToString:@""]) {
            if (![_countryUser.text isEqualToString:@""]) {
                if ((![_typeUser.text isEqualToString:@""] && !self.otherUserTyper.enabled) || (self.otherUserTyper.enabled && ![self.otherUserTyper.text isEqualToString:@"other"])) {
                    [self registerUser];
                    
                }else{
                    [self callAlertMethod:@"Attenzione" Message:@"Inserire la tipologia di utente"];
                }
            } else {
                [self callAlertMethod:@"Attenzione" Message:@"Inserire Paese di apparteneza"];
            }
        }else{
            [self callAlertMethod:@"Attenzione" Message:@"Inserire password"];
        }
    }else{
        [self callAlertMethod:@"Attenzione" Message:@"Email non valida"];
    }
}

-(void)registerUser{
    NSString * countryId;
    for (NSDictionary * c in jsonCountry) {
        if ([[[c objectForKey:@"Country"]objectForKey:@"name"] isEqualToString:_countryUser.text]) {
            countryId =[NSString stringWithFormat:@"%@",[[c objectForKey:@"Country"] objectForKey:@"id"]];
            break;
        }
    }
    
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@&type=%@&country_id=%@",_emailUser.text,_pswUser.text,
                      [_otherUserTyper.text isEqualToString:@""] ? _typeUser.text : [NSString stringWithFormat:@"%@&other=%@",_typeUser.text,_otherUserTyper.text],countryId];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://46.252.150.235:8085/api/customers/add.json"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:@"Error"
                                               message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction* yesButton = [UIAlertAction
                                             actionWithTitle:@"Ok"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action)
                                             {
                                                 [self viewDidAppear:YES];
                                             }];
                 
                 [alert addAction:yesButton];
                 [self presentViewController:alert animated:YES completion:nil];
             });
             
         }
         else
         {
             NSError* error;
             NSDictionary* json = [NSJSONSerialization
                                   JSONObjectWithData:data 
                                   options:kNilOptions
                                   error:&error];
             if ([[json objectForKey:@"status"] intValue] == 1) {
                 dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                     //Background Thread
                     dispatch_async(dispatch_get_main_queue(), ^(void){
                         [self restoreStatusTextField];
                         [self performSegueWithIdentifier:@"backToLogin" sender:self];
                     });
                 });
                 
             }else{
                 dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                     dispatch_async(dispatch_get_main_queue(), ^(void){
                         if ([[json objectForKey:@"message"] objectForKey:@"email"]) {
                             [self callAlertMethod:@"Attenzione" Message:@"Email already in use"];
                         }else{
                             [self callAlertMethod:@"Attenzione" Message:[NSString stringWithFormat:@"%@",[json objectForKey:@"message"]]];
                         }
                         
                     });
                 });
             }
         }
     }];
}

-(void)restoreStatusTextField{
    _emailUser.text = @"";
    _pswUser.text = @"";
    _countryUser.text = @"";
    _typeUser.text = @"";
    _otherUserTyper.text = @"";
}

-(void)callAlertMethod:(NSString *)title Message:(NSString *)message{
    UIAlertController *actionSheet= [UIAlertController
                                     alertControllerWithTitle:title
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionSheetButton1 = [UIAlertAction
                                         actionWithTitle:@"Okay"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             
                                         }];
    
    [actionSheet addAction:actionSheetButton1];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

-(void)createUIAlertController:(NSMutableArray *)arrayAlert TextField:(UITextField *)textField TitleAlert:(NSString *)titleAlert MessageAlert:(NSString *)messageAlert{
    
    alertController = [UIAlertController alertControllerWithTitle:titleAlert
                                                          message:messageAlert
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i= 0; i< [arrayAlert count]; i++) {
        [alertController addAction:[UIAlertAction
                                    actionWithTitle:[NSString stringWithFormat:@"%@",arrayAlert[i]]
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action){
                                        if (textField == _typeUser) {
                                            if (i == 6) {
                                                _typeUser.text = arrayAlert[i];
                                                _otherUserTyper.enabled = YES;
                                            }else{
                                                _otherUserTyper.text = @"";
                                                _otherUserTyper.enabled = NO;
                                                 textField.text = arrayAlert[i];
                                            }
                                        }else{
                                            textField.text = arrayAlert[i];
                                           
                                        }
                                        
                                    }]];
    }
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Annulla", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                   }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
