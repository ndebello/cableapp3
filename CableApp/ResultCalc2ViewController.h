//
//  ResultCalc2ViewController.h
//  CableA
//
//  Created by Matteo Centro on 21/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ResultCalc2ViewController : UIViewController<MFMailComposeViewControllerDelegate>
- (IBAction)shareAction:(id)sender;

- (IBAction)saveCalc:(id)sender;
- (IBAction)contactUs:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *showCatalog;
@end
