//
//  ResultCalc3ViewController.m
//  CableA
//
//  Created by Matteo Centro on 21/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "ResultCalc3ViewController.h"
#import "DataSelected.h"

@interface ResultCalc3ViewController () {
    DataSelected * shared;
}
@property (weak, nonatomic) IBOutlet UILabel * nomeCavo;
@property (weak, nonatomic) IBOutlet UILabel * inst1Res;
@property (weak, nonatomic) IBOutlet UILabel * inst2Res;
@property (weak, nonatomic) IBOutlet UILabel * inst3Res;
@property (weak, nonatomic) IBOutlet UILabel * numCoresRes;
@property (weak, nonatomic) IBOutlet UILabel * insulRes;
@property (weak, nonatomic) IBOutlet UILabel * currRes;
@property (weak, nonatomic) IBOutlet UILabel * voltRes;
@property (weak, nonatomic) IBOutlet UILabel * savingRes;
@property (weak, nonatomic) IBOutlet UILabel * moneyRes;

@property (weak, nonatomic) IBOutlet UIView * container;
@property (weak, nonatomic) IBOutlet UILabel * resultSection;
@property (weak, nonatomic) IBOutlet UILabel * resultConductors;
@end

@implementation ResultCalc3ViewController
- (void) viewDidLoad
{
    [super viewDidLoad];
    shared = [DataSelected sharedManager];
    self.navigationItem.title = @"Section Calc Result 3";
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [_container.layer setCornerRadius:7.0];
    
    _resultSection.text = [NSString stringWithFormat:@"%@", [shared.calResult objectForKey:@"section"]];
    _resultConductors.text = [NSString stringWithFormat:@"%@", [shared.calResult objectForKey:@"NumberCore"]];
    // Do any additional setup after loading the view.
    
    _nomeCavo.text = shared.numCablesDen;
    _inst1Res.text = shared.s1;
    _inst2Res.text = shared.s2;
    _inst3Res.text = shared.s3;
    _numCoresRes.text = [NSString stringWithFormat:@"%@", [[shared.CS objectForKey:@"NumberCore"] objectForKey:@"name"]];
    _insulRes.text = shared.sInsulation;
    _currRes.text = shared.sCurType;
    _voltRes.text = shared.sVolt;
    
    _savingRes.text = @"---";
    _moneyRes.text = @"--- Euro";
    
}


- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) viewWillDisappear:(BOOL)animated
{
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound)
    {
        if (shared.eDepth)
        {
            shared.sDepth = nil;
            shared.eDepth = NO;
        }
        if (shared.ePosition)
        {
            shared.sPosition = nil;
            shared.ePosition = NO;
        }
        
        if (shared.eSun)
        {
            shared.sSunExp = nil;
            shared.eSun = NO;
        }
        if (shared.eNumCableC)
        {
            shared.sNumCableC = nil;
            shared.eNumCableC = NO;
        }
        if (shared.eNumCon)
        {
            shared.sNumCondu = nil;
            shared.eNumCon = NO;
        }
        
        if (shared.eDistance)
        {
            shared.sDistance = nil;
            shared.eDistance = NO;
        }
        if (shared.eTrays)
        {
            shared.sNumTrays = nil;
            shared.eTrays = NO;
        }
    }
    [super viewWillDisappear:animated];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction) shareAction:(id)sender
{
    NSLog(@"Share");
}

- (IBAction) saveCalc:(id)sender
{
    NSLog(@"Save email");
}

- (IBAction) contactUs:(id)sender
{
    //    if ([MFMailComposeViewController canSendMail])
    //    {
    //        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    //        mail.mailComposeDelegate = self;
    //        [mail setSubject:@"Sample Subject"];
    //        [mail setMessageBody:@"Here is some main text in the email!" isHTML:NO];
    //        [mail setToRecipients:@[@"testingEmail@example.com"]];
    //
    //        [self presentViewController:mail animated:YES completion:NULL];
    //    }
    //    else
    //    {
    //        NSLog(@"This device cannot send email");
    //    }
}

@end
