//
//  ResultCalcViewController.h
//  CableApp
//
//  Created by Welington Barichello on 02/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ResultCalcViewController : UIViewController<MFMailComposeViewControllerDelegate>
- (IBAction)shareAction:(id)sender;

- (IBAction)saveCalc:(id)sender;
- (IBAction)contactUs:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *showCatalog;
@end
