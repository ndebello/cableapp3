//
//  ResultCalcViewController.m
//  CableApp
//
//  Created by Welington Barichello on 02/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "ResultCalcViewController.h"
#import "NSUserDefaults+RMSaveCustomObject.h"
#import "DataSelected.h"

@interface ResultCalcViewController (){
    DataSelected * shared;
}
@property (weak, nonatomic) IBOutlet UILabel *nomeCavo;
@property (weak, nonatomic) IBOutlet UILabel *inst1Res;
@property (weak, nonatomic) IBOutlet UILabel *inst2Res;
@property (weak, nonatomic) IBOutlet UILabel *inst3Res;
@property (weak, nonatomic) IBOutlet UILabel *numCoresRes;
@property (weak, nonatomic) IBOutlet UILabel *insulRes;
@property (weak, nonatomic) IBOutlet UILabel *currRes;
@property (weak, nonatomic) IBOutlet UILabel *voltRes;
@property (weak, nonatomic) IBOutlet UILabel *saving1Res;
@property (weak, nonatomic) IBOutlet UILabel *money1Res;
@property (weak, nonatomic) IBOutlet UILabel *saving2Res;
@property (weak, nonatomic) IBOutlet UILabel *money2Res;

@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UILabel *resultSection;
@property (weak, nonatomic) IBOutlet UILabel *resultConductors;

@end

@implementation ResultCalcViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    shared = [DataSelected sharedManager];
    self.navigationItem.title = @"Summary";
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [_container.layer setCornerRadius:7.0];
    
    /*NSArray *myArray1 = [[NSString stringWithFormat:@"%@",[shared.calResult objectForKey:@"section"]] componentsSeparatedByString:@"_"];
    _resultSection.text = [myArray1 objectAtIndex:1];
    _savingRes.text = [myArray1 objectAtIndex:0];
    
    NSArray *myArray2 = [[NSString stringWithFormat:@"%@",[shared.calResult objectForKey:@"NumberCore"]] componentsSeparatedByString:@"_"];
    _resultConductors.text = [myArray2 objectAtIndex:1];
    _moneyRes.text = [myArray2 objectAtIndex:0];*/
    
    _resultSection.text = [NSString stringWithFormat:@"%@",[shared.calResult objectForKey:@"section"]];
    _resultConductors.text = [NSString stringWithFormat:@"%@",[shared.calResult objectForKey:@"NumberCore"]];
    _resultConductors.hidden = TRUE;
    
    _nomeCavo.text = shared.cableCommNameDen;
    _nomeCavo.hidden = TRUE;
    _inst1Res.text = shared.s1;
    _inst1Res.hidden = TRUE;
    _inst2Res.text = shared.s2;
    _inst2Res.hidden = TRUE;
    _inst3Res.text = shared.s3;
    _inst3Res.hidden = TRUE;
    _numCoresRes.text = [NSString stringWithFormat:@"%@",[[shared.CS objectForKey:@"NumberCore"] objectForKey:@"name"]];
    _numCoresRes.hidden = TRUE;
    _insulRes.text = shared.sInsulation;
    _insulRes.hidden = TRUE;
    _currRes.text = shared.sCurType;
    _currRes.hidden = TRUE;
    _voltRes.text = shared.sVolt;
    _voltRes.hidden = TRUE;
    
    _saving1Res.text = [NSString stringWithFormat:@"%@",[shared.calResult objectForKey:@"saving1"]];
    _money1Res.text = [NSString stringWithFormat:@"%@",[shared.calResult objectForKey:@"money2"]];    // Non so perchè Marco li ritorni a etichette invertite
    _saving2Res.text = [NSString stringWithFormat:@"%@",[shared.calResult objectForKey:@"saving2"]];
    _saving2Res.hidden = TRUE;
    _money2Res.text = [NSString stringWithFormat:@"%@",[shared.calResult objectForKey:@"money1"]];
    _money2Res.hidden = TRUE;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        if (shared.eDepth) {
            shared.sDepth = nil;
            shared.eDepth = NO;
        }
        if (shared.ePosition) {
            shared.sPosition = nil;
            shared.ePosition = NO;
        }
        
        if (shared.eSun) {
            shared.sSunExp = nil;
            shared.eSun = NO;
        }
        if (shared.eNumCableC) {
            shared.sNumCableC = nil;
            shared.eNumCableC = NO;
        }
        if (shared.eNumCon) {
            shared.sNumCondu = nil;
            shared.eNumCon = NO;
        }
        
        if (shared.eDistance) {
            shared.sDistance = nil;
            shared.eDistance = NO;
        }
        if (shared.eTrays) {
            shared.sNumTrays = nil;
            shared.eTrays = NO;
        }
    }
    [super viewWillDisappear:animated];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)shareAction:(id)sender {
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil) {
        MFMailComposeViewController * mailView = [[MFMailComposeViewController alloc] init];
        mailView.mailComposeDelegate = self;
        
        //Set the subject
        [mailView setSubject:@"Condivisione Cavo Prysmian"];
        
        //Set the mail body
        [mailView setMessageBody:[NSString stringWithFormat:@"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><title>Prysmian</title></head><body><div style=\"float: left\"><img src=\"http://www.prysmiangroup.com/images/contentimg/logo-prysmian-group.png\"><br>Benvenuto xxx, questi sono i risultati!</div><br><br><br><br><div style=\"border:1px solid black; text-align:center;font-size:30px\"><span style=\"color: #DE0B7E\"><b>%@</b></span></div><div style=\"border:1px solid black; text-align:center;\"><img style=\"width: 100%%;\"src=\"%@\"></div><br><div style=\"border:1px solid black; text-align:center; font-size:30px\"><span style=\"color: #DE0B7E;\"><b>Dettagli</b></span></div><div style=\"margin: 10px\"><div><span style=\"float: left;color: #DE0B7E\">Nome Generico</span><span style=\"float:right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">Descrizione</span><span style=\"float:right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">Nazione</span><span style=\"float:right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">Design</span><span style=\"float:right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">Insulation</span><span style=\"float:right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">Conductor</span><span style=\"float:right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">Restriction</span><span style=\"float:right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">CprClass</span><span style=\"float:right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">CableRange</span><span style=\"float:right;color: #666666\">%@</span></div><br><div></div><br><div><span style=\"float: left;color: #DE0B7E\">best_cable</span><span style=\"float:right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">NumberCore</span><span style=\"float:right;color: #666666\">%@</span></div><br></div><br><div><div style=\"text-align: center; border:1px solid black; font-size:30px\"><span style=\"color: #DE0B7E;\"><b>Quadrupla</b></span></div><div style=\"margin: 10px\"><div><span style=\"float: left;color: #DE0B7E\"> inst </span> <span style=\"float: right;color: #666666\"> %@ </span><br><span style=\"float: right;color: #666666\"> %@ </span><br> <span style=\"float: right;color: #666666\"> %@ </span></div><br><div><span style=\"float: left;color: #DE0B7E\"> numCore </span><span style=\"float: right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">method</span> <span style=\"float: right;color: #666666\">%@</span></div></div><br><div style=\"text-align: center;border:1px solid black; font-size:30px\"><span style=\"color: #DE0B7E\"><b>RESULTS</b></span></div><div style=\"margin: 10px\"><div><span style=\"float: left;color: #DE0B7E\">SECTION</span><span style=\"float: right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">n°conductor phase</span><span style=\"float: right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">yearly Saving C02</span><span style=\"float: right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">yearly Saving</span><span style=\"float: right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">Saving 2</span><span style=\"float: right;color: #666666\">%@</span></div><br><div><span style=\"float: left;color: #DE0B7E\">money 1</span><span style=\"float: right;color: #666666\">%@</span></div><br><div style=\"background-color: #666666;overflow:hidden\"><span style=\"float:right;color:white\">Prysmian group &#169; 2016</span></div></body></html>",[shared.CS objectForKey:@"commercial_name"],
                                  [shared.CS objectForKey:@"image_url" ],[shared.CS objectForKey:@"generic_name"],
                                  [shared.CS objectForKey:@"description"],[[shared.CS objectForKey:@"Country"] objectForKey:@"name"],[[shared.CS objectForKey:@"Design"] objectForKey:@"name"],
                                  [[shared.CS objectForKey:@"Insulation"] objectForKey:@"name"],[[shared.CS objectForKey:@"Conductor"] objectForKey:@"name"],[[shared.CS objectForKey:@"Restriction"] objectForKey:@"name"],[[shared.CS objectForKey:@"CprClass"] objectForKey:@"name"],
                                  [[shared.CS objectForKey:@"CableRange"] objectForKey:@"name"],[shared.CS objectForKey:@"best_cable"],[[shared.CS objectForKey:@"NumberCore"] objectForKey:@"name"],shared.s1,shared.s2,shared.s3,[[shared.CS objectForKey:@"NumberCore"] objectForKey:@"name"],shared.sMetINT,[shared.calResult objectForKey:@"section"],[shared.calResult objectForKey:@"NumberCore"],[shared.calResult objectForKey:@"saving1"],[shared.calResult objectForKey:@"money2"],[shared.calResult objectForKey:@"saving2"],[shared.calResult objectForKey:@"money1"]] isHTML:YES];
        
        
        //Display Email Composer
        if([mailClass canSendMail]) {
            [self presentViewController:mailView animated:YES completion:NULL];
        }
    }
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)saveCalc:(id)sender {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"indexSaved"]) {
        NSMutableArray *indexObj = [NSMutableArray new];
        NSLog(@"%lu",(unsigned long)[indexObj count]);
        [indexObj addObject:[NSString stringWithFormat:@"0"]];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:indexObj];
        [defaults setObject:data forKey:@"indexSaved"];
        [defaults synchronize];
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy  HH:mm:ss"];
        shared.DataSaveCal = [dateFormat stringFromDate:today];
        [defaults rm_setCustomObject:shared forKey:[NSString stringWithFormat:@"saved%lu",(unsigned long)[indexObj count]-1]];
    }else{
        NSData *data = [defaults objectForKey:@"indexSaved"];
        NSMutableArray *indexObj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy  HH:mm:ss"];
        shared.DataSaveCal = [dateFormat stringFromDate:today];
        [defaults rm_setCustomObject:shared forKey:[NSString stringWithFormat:@"saved%lu",(unsigned long)[indexObj count]+1]];
        [indexObj addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[indexObj count]+1]];
        NSData *data1 = [NSKeyedArchiver archivedDataWithRootObject:indexObj];
        [defaults setObject:data1 forKey:@"indexSaved"];
        [defaults synchronize];
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Complimenti" message:@"Calcolo salvato con successo" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }];
    
    [alertController addAction:action2];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)contactUs:(id)sender {
//    if ([MFMailComposeViewController canSendMail])
//    {
//        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
//        mail.mailComposeDelegate = self;
//        [mail setSubject:@"Sample Subject"];
//        [mail setMessageBody:@"Here is some main text in the email!" isHTML:NO];
//        [mail setToRecipients:@[@"testingEmail@example.com"]];
//        
//        [self presentViewController:mail animated:YES completion:NULL];
//    }
//    else
//    {
//        NSLog(@"This device cannot send email");
//    }
}
@end
