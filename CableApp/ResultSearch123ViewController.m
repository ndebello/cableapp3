//
//  ResultSearch123ViewController.m
//  CableApp
//
//  Created by Welington Barichello on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "ResultSearch123ViewController.h"
#import "CablesListViewController.h"
#import "DataSelected.h"

@interface ResultSearch123ViewController (){
    DataSelected * sharedData;
}

@end

@implementation ResultSearch123ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Search Cable";
    sharedData = [DataSelected sharedManager];
    [self addColorButton];
    
    // Do any additional setup after loading the view.
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    self.labelnsta1.text = [sharedData.Inst1[[sharedData.s1 intValue]-1] valueForKey:sharedData.s1];
    [self SInstal23];
}
-(void)addColorButton{
    _appl1Button.layer.borderColor=[[self colorWithHexString:@"c6c6c7"] CGColor];
    _appl23Button.layer.borderColor=[[self colorWithHexString:@"c6c6c7"] CGColor];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
-(void)SInstal23{
    
    for (NSDictionary * a in sharedData.Inst2) {
        if ([[NSString stringWithFormat:@"%@",[[a allKeys] objectAtIndex:0]] isEqualToString:[NSString stringWithFormat:@"%@",sharedData.s2]]) {
            self.Instal23.text = [NSString stringWithFormat:@"%@",[a objectForKey:[NSString stringWithFormat:@"%@",sharedData.s2]] ];
        }
    }
    for (NSDictionary * b in sharedData.Inst3) {
        if ([[NSString stringWithFormat:@"%@",[[b allKeys] objectAtIndex:0]] isEqualToString:[NSString stringWithFormat:@"%@",sharedData.s3]]) {
            self.inst3.text = [NSString stringWithFormat:@"%@",[b objectForKey:[NSString stringWithFormat:@"%@",sharedData.s3]]];
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (IBAction)searchCable:(id)sender {
    
}

@end
