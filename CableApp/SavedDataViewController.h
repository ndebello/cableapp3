//
//  SavedDataViewController.h
//  CableA
//
//  Created by Welington Barichello on 08/03/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedDataViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu;

@end
