//
//  SavedDataViewController.m
//  CableA
//
//  Created by Welington Barichello on 08/03/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "SavedDataViewController.h"
#import "SWRevealViewController.h"
#import "NSUserDefaults+RMSaveCustomObject.h"
#import "DisplaCalcViewController.h"
#import "DataSelected.h"

@interface SavedDataViewController (){
    NSUserDefaults * defauts;
    NSMutableArray * arrayDedaults;
    DataSelected * dataSelected;
    DataSelected * dataS;
}
@end

@implementation SavedDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    defauts = [NSUserDefaults standardUserDefaults];
    NSData *data = [defauts objectForKey:@"indexSaved"];
    if (!arrayDedaults) {
        arrayDedaults = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    self.menu.target = self.revealViewController;
    self.menu.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    self.navigationItem.title = @"Saved Data";
    self.menu.image = [UIImage imageNamed:@"menu_icon"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrayDedaults count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CableCell" forIndexPath:indexPath];
//
//    NSLog(@"%@",[NSString stringWithFormat:@"saved%@",arrayDedaults[indexPath.row]]);
    dataSelected = [defauts rm_customObjectForKey:[NSString stringWithFormat:@"saved%@",arrayDedaults[indexPath.row]]];
//    cell.textLabel.text = [dataSelected.CS objectForKey:@"commercial_name"];
//    return cell;
    //UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CableCell" forIndexPath:indexPath];
    UILabel * nameLabel = (UILabel *) [cell viewWithTag:10];
    UILabel * descLabel = (UILabel *) [cell viewWithTag:11];
    UIImageView * imgCable  = (UIImageView *) [cell viewWithTag:12];
    
    //setting Style
    [imgCable.layer setBorderColor: [[self colorWithHexString:@"DE0B7E"] CGColor]];
    [imgCable.layer setBorderWidth: 1.0];
    [imgCable.layer setCornerRadius: 2.0];
    //*******
    
   
    nameLabel.text =  [NSString stringWithFormat:@"%@ %@ %@",[dataSelected.CS objectForKey:@"commercial_name"],[dataSelected.calResult objectForKey:@"NumberCore"],[NSString stringWithFormat:@"%@",[dataSelected.calResult objectForKey:@"section"]]];
    descLabel.text = [NSString stringWithFormat:@"%@ %@",dataSelected.DataSaveCal,[dataSelected.CS objectForKey:@"description"]];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [dataSelected.CS objectForKey:@"image_url"]]];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (imgCable)
                        imgCable.image = image;
                });
            }
        }
    }];
    [task resume];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    dataSelected = [defauts rm_customObjectForKey:[NSString stringWithFormat:@"saved%@",arrayDedaults[indexPath.row]]];
    [self performSegueWithIdentifier:@"displayData" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"displayData"]){
        DisplaCalcViewController *vc = [segue destinationViewController];
        vc.sharedCalc = dataSelected;
    }
}
-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

@end
