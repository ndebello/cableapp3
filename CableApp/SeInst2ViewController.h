//
//  SeInst2ViewController.h
//  CableApp
//
//  Created by Welington Barichello on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Install23ViewController.h"

@interface SeInst2ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelInst1;
@property (weak, nonatomic) IBOutlet UIButton *applicationButton;
@property (weak, nonatomic) IBOutlet UIButton *instButton;

@end
