//
//  SeInst2ViewController.m
//  CableApp
//
//  Created by Welington Barichello on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "SeInst2ViewController.h"
#import "DataSelected.h"


@interface SeInst2ViewController (){
    DataSelected * sharedData;
}

@end

@implementation SeInst2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Cable Search";
    sharedData = [DataSelected sharedManager];
    [self layoutViewSearch];
    
    // Do any additional setup after loading the view.
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    if (!sharedData.Inst2) {
        sharedData.Inst2 = [NSMutableArray new];
        self.labelInst1.text = [sharedData.Inst1[[sharedData.s1 intValue]-1] valueForKey:sharedData.s1];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://46.252.150.235:8085/api/installation2s/index/%@.json",sharedData.s1]];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 NSLog(@"Error,%@", [error localizedDescription]);
             }
             else
             {
                 [self ParsingJsonInstallation2:data];
             }
         }];
    }
}

-(void)ParsingJsonInstallation2:(NSData *)data{
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          options:kNilOptions
                          error:&error];
    
    for (NSDictionary *inst2 in json) {
        
        NSDictionary *p = @{
                            [[inst2 objectForKey:@"Installation2"] objectForKey:@"id"]: [[inst2 objectForKey:@"Installation2"] objectForKey:@"description"]
                            };
        [sharedData.Inst2 addObject:p];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)layoutViewSearch{
    
    _applicationButton.layer.borderColor=[[self colorWithHexString:@"c6c6c7"] CGColor];
    
    _instButton.layer.borderColor=[[self colorWithHexString:@"c6c6c7"] CGColor];
}


-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        sharedData.Inst2 = nil;
    }
    [super viewWillDisappear:animated];
}
@end
