//
//  SearchCableAttrViewController.h
//  CableApp
//
//  Created by Welington Barichello on 13/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCableAttrViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *selectGenericType;
@property (weak, nonatomic) IBOutlet UITextField *selectVoltage;
@property (weak, nonatomic) IBOutlet UITextField *selectApplication;
@property (weak, nonatomic) IBOutlet UITextField *selectInstMeth;
@property (weak, nonatomic) IBOutlet UITextField *selectLocalStandard;
@property (weak, nonatomic) IBOutlet UITextField *crpClass;

@property (weak, nonatomic) IBOutlet UIButton *searchElement;


- (IBAction)searchCable:(id)sender;
@end
