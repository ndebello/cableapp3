//
//  SearchCableAttrViewController.m
//  CableApp
//
//  Created by Welington Barichello on 13/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "SearchCableAttrViewController.h"
#import "CablesListTypesViewController.h"
#import "CablesListViewController.h"
#import "DataSelected.h"

@interface SearchCableAttrViewController ()
{
    DataSelected * sharedData;
    UIAlertController *alertController;
    NSArray * InsM;//Methods
    NSArray * V;// Voltage
    NSArray * Inst1; //Installazione 1
    NSArray * genericType;//generic range of cables
    NSArray * localStandard; //designs
    NSArray * cpr;// cpr class
    
    
    //selection element
    NSString * sGenType;
    NSString * sVoltClass;
    NSString * sApplication;
    NSString * sInstMeth;
    NSString * sLocalSt;
    NSString * sCprCl;
    
    NSArray *results;
    NSData * support;
}

@end

@implementation SearchCableAttrViewController

- (void)viewDidLoad
{
    sharedData  = [DataSelected sharedManager];
    self.navigationItem.title = @"Cable Search";
    //self.searchElement.alpha = 0.0;
    
//
//
//
//
//
//
//
//
//
    InsM = [[NSArray alloc] initWithObjects:@"C:cable mono o multiconductor fijado sobre una pared de mampostería (ladrillo, yeso…)",
            @"F:cable monoconductor al aire (bandeja perforada, rejilla, escalera, suspendido o grapado con separación a pared)",
            @"D1: cable mono o multiconductor en conducto enterrado",
            @"D2: cable mono o multiconductor directamente enterrado",
            @"A1: conductores aislados en un conducto en una pared térmicamente aislada",
            @"A2: cable multiconductor en un conducto en una pared térmicamente aislada",
            @"B1: conductores aislados en un conducto sobre una pared de mampostería (ladrillo, yeso…)",
            @"B2: cable multiconductor en un conducto sobre una pared de mampostería (ladrillo, yeso…)",
            @"E: cable multiconductor al aire (bandeja perforada, rejilla, escalera, suspendido o grapado con separación a pared o techo)",
            @"G: conductores desnudos o aislados sobre aisladores",
            @"I:Under conduit and buried (Enterrados bajo tubo)",
            @"J:On air (Al aire)",
            @"H:Directly buried (Directamente enterrados)", nil];
    V = [[NSArray alloc] initWithObjects:@"300/500 V",@"450/750 V",@"0,6/1 KV",@"1,8/3 KV",@"3,6/6KV",@"6/10KV",@"8,7/15KV",@"12/20KV",@"15/25KV",@"18/30KV",nil];
    Inst1 = [[NSArray alloc] initWithObjects:
             @"ITC-BT 14 Línea general de alimentación (5)",
             @"ITC-BT 15 Derivaciones individuales (6)",
             @"ITC-BT 20 Instalaciones interiores o receptoras en general (7)",
             @"ITC-BT 25 - 26  Viviendas (8)",
             @"ITC-BT 28 Locales de pública concurrencia (9)",
             @"ITC-BT 29 Locales con riesgo incendio o explosión (10)",
             @"ITC-BT 30 (I) Locales húmedos (11)",
             @"ITC-BT 30 (II) Locales mojados (incluidas instalaciones a la intemperie) (12)",
             @"ITC-BT 31 Piscinas y Fuentes (13)",
             @"ITC-BT 34 Ferias y Stands (14)",
             @"ITC-BT 40 Instalaciones generadoras de baja tensión (fotovoltaica, etc.) (14 b)",
             @"ITC-BT 42 Puertos y marinas para barcos de recreo (15)",
             @"ITC-BT 9 (I) Alumbrado exterior aereo (3)",
             @"ITC-BT 9 (II) Alumbrado exterior subterráneo (4)",
             @"ITC-LAT 06",
             nil];
    
    genericType = [[NSArray alloc] initWithObjects:@"AFUMEX",
                   @"NETWORKS",
                   @"PVC",
                   @"RUBBER",
                   @"FV",
                   @"MV", nil];
    
    localStandard = [[NSArray alloc]initWithObjects:@"UNE-EN 50525-3-31",
                     @"UNE 21027-9/1C",
                     @"UNE 21123-4",
                     @"UNE 211025",
                     @"UNE-EN 50525-3-21",
                     @"DKE/VDE AK 411.2.3",
                     @"UNE-EN 50525-2-31",
                     @"UNE 21123-2",
                     @"UNE-EN 50525-2-21",
                     @"IEC 60502-1",
                     @"UNE 21166",
                     @"HD 603-5X-1",
                     @"UNE 21030-1",
                     @"UNE 21030-2",
                     @"UNE-HD 620-9E",
                     @"UNE 211620",
                     @"UNE-HD 620-10E",nil];
    cpr = [[NSArray alloc]initWithObjects:@"none", nil];
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)downloadElement
{
    NSURL *url = [NSURL URLWithString:@"http://46.252.150.235:8085/api/designs.json"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:@"Error"
                                               message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction* yesButton = [UIAlertAction
                                             actionWithTitle:@"Ok"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action)
                                             {
                                                 [self viewDidAppear:YES];
                                             }];
                 
                 [alert addAction:yesButton];
                 [self presentViewController:alert animated:YES completion:nil];
             });
             
         }
         else
         {
             NSError* error;
             NSDictionary* json = [NSJSONSerialization
                                   JSONObjectWithData:data //1
                                   options:kNilOptions
                                   error:&error];
             sharedData.cableAttE.genType = json;
             NSLog(@"%@",json);
         }
     }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    alertController = nil;
    if (self.selectInstMeth == textField)
    {
        [self createUIAlertController:InsM TextField:textField TitleAlert:@"Select Application " MessageAlert:@""];
        [self.view endEditing:YES];
    }
    else if (self.selectVoltage == textField)
    {
        [self createUIAlertController:V TextField:textField TitleAlert:@"Select Voltage Class" MessageAlert:@""];
        [self.view endEditing:YES];
    }
    else if (self.selectApplication == textField)
    {
        [self createUIAlertController:Inst1 TextField:textField TitleAlert:@"Select Application" MessageAlert:@""];
        [self.view endEditing:YES];
    }
    else if (self.selectGenericType == textField)
    {
        [self createUIAlertController:genericType TextField:textField TitleAlert:@"Select Generic Type" MessageAlert:@""];
        [self.view endEditing:YES];
    }
    else if (self.selectLocalStandard == textField)
    {
        [self createUIAlertController:localStandard TextField:textField TitleAlert:@"Select Local Standard" MessageAlert:@""];
        [self.view endEditing:YES];
    }
    else if (self.crpClass == textField)
    {
        [self createUIAlertController:cpr TextField:textField TitleAlert:@"Select CPR Class" MessageAlert:@""];
        [self.view endEditing:YES];
    }
    
}

-(void)createUIAlertController:(NSArray *)arrayAlert TextField:(UITextField *)textField TitleAlert:(NSString *)titleAlert MessageAlert:(NSString *)messageAlert{
    
    alertController = [UIAlertController alertControllerWithTitle:titleAlert
                                          message:messageAlert
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i= 0; i< [arrayAlert count]; i++) {
        [alertController addAction:[UIAlertAction
                                    actionWithTitle:arrayAlert[i]
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        if (genericType == arrayAlert) {
                                            sGenType = [NSString stringWithFormat:@"%d",i+1];
                                        }else if (V == arrayAlert){
                                            sVoltClass = [NSString stringWithFormat:@"%d",i+1];
                                        }else if (Inst1 == arrayAlert){
                                            sApplication = [NSString stringWithFormat:@"%d",i+1];
                                        }else if (InsM == arrayAlert){
                                            sInstMeth = [NSString stringWithFormat:@"%d",i+1];
                                        }else if (localStandard == arrayAlert){
                                            sLocalSt = [NSString stringWithFormat:@"%d",i+1];
                                        }else if (cpr == arrayAlert){
                                            sCprCl = [NSString stringWithFormat:@"%d",i+1];
                                        }
                                        textField.text = arrayAlert[i];
                                    }]];
    }
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Annulla", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       textField.text = @"";
                                   }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

//# FIXME: - Ricerca concatenata OK
- (IBAction)searchCable:(id)sender{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://46.252.150.235:8085/api/cables/index%@%@%@%@%@%@.json",
                                       sApplication ? [NSString stringWithFormat:@"/installation1_id:%@",sApplication] : @"",
                                       sInstMeth ? [NSString stringWithFormat:@"/method_id:%@",[self rangElement:sInstMeth]] : @"",
                                       sGenType ? [NSString stringWithFormat:@"/cable_range_id:%@",sGenType] : @"",
                                       sVoltClass ? [NSString stringWithFormat:@"/voltage_id:%@",sVoltClass] : @"",
                                       sCprCl ? [NSString stringWithFormat:@"/cpr_class_id:%@",sCprCl] : @"",
                                       sLocalSt ? [NSString stringWithFormat:@"/design_id:%@",sLocalSt] : @""]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertController * alert=  [UIAlertController
                                              alertControllerWithTitle:@"Error"
                                              message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                              preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction* yesButton = [UIAlertAction
                                             actionWithTitle:@"Ok"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action)
                                             {
                                                 [self viewDidAppear:YES];
                                             }];
                 [alert addAction:yesButton];
                 [self presentViewController:alert animated:YES completion:nil];
             });
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self ParsingAtt:data];
             });
         }
     }];
    
    
}


// Passa i parametri alla view successiva
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    CablesListViewController *vc = [segue destinationViewController];
    sharedData.attOrEl = 1;
    [vc parseJson:support];
    //vc.results = results;
}

-(void)ParsingAtt:(NSData *)data
{
    
    NSError* error;
    NSArray* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          options:kNilOptions
                          error:&error];
    
    for (NSDictionary * inst1 in json) {
        if ([[inst1 objectForKey:@"Cable"] objectForKey:@"status"]) {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Attenzione"
                                          message:@"Non ci sono cavi per la ricerca, provare altra combinazione"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Okay"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            //Handel your yes please button action here
                                            
                                            
                                        }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            break;
        }else{
            support = data;
            [self performSegueWithIdentifier: @"GoToDetailsCables" sender: self];
            break;
        }
    }
    
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        sharedData.attOrEl = 0;
    }
    [super viewWillDisappear:animated];
}
-(NSString *)rangElement:(NSString *)idName{
    NSArray *subStrings = [idName componentsSeparatedByString:@":"];
    return subStrings[0];
}
@end
