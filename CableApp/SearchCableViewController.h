//
//  SearchCableViewController.h
//  CableApp
//
//  Created by Welington Barichello on 11/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCableViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu;
@property (weak, nonatomic) IBOutlet UIView *electricalView;
@property (weak, nonatomic) IBOutlet UIView *cableAttributeView;

@property (weak, nonatomic) IBOutlet UIButton *eletricalButton;
@property (weak, nonatomic) IBOutlet UIButton *cableButton;

@property (weak, nonatomic) IBOutlet UILabel *titileSearchView;

@end
