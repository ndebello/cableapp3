//
//  SearchCableViewController.m
//  CableApp
//
//  Created by Welington Barichello on 11/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SearchCableViewController.h"
#import "SWRevealViewController.h"
#import "DataSelected.h"

@interface SearchCableViewController (){
    DataSelected * sharedData;
}

@end

@implementation SearchCableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.menu.target = self.revealViewController;
    self.menu.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    self.navigationItem.title = @"Cable Search";
    self.menu.image = [UIImage imageNamed:@"menu_icon"];
    sharedData = [DataSelected sharedManager];
    
    // Do any additional setup after loading the view.
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    if (!sharedData.Inst1) {
        NSURL *url = [NSURL URLWithString:@"http://46.252.150.235:8085/api/installation1s.json"];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     UIAlertController * alert=   [UIAlertController
                                                   alertControllerWithTitle:@"Error"
                                                   message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                   preferredStyle:UIAlertControllerStyleAlert];
                     
                     UIAlertAction* yesButton = [UIAlertAction
                                                 actionWithTitle:@"Ok"
                                                 style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * action)
                                                 {
                                                     [self viewDidAppear:YES];
                                                     
                                                     
                                                 }];
                     
                     [alert addAction:yesButton];
                     
                     [self presentViewController:alert animated:YES completion:nil];
                 });
                 
                 //NSLog(@"Error,%@", [error localizedDescription]);
             }else
             {
                 sharedData.Inst1 = [NSMutableArray new];
                 [self ParsingJsonInstallation1:data];
             }
         }];
    }
    
}




-(void)ParsingJsonInstallation1:(NSData *)data{
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          options:kNilOptions
                          error:&error];
    
    for (NSDictionary * inst1 in json) {
        NSDictionary *p = @{
                            [[inst1 objectForKey:@"Installation1"] objectForKey:@"id"]: [[inst1 objectForKey:@"Installation1"] objectForKey:@"displayfield"]
                            };
        [sharedData.Inst1 addObject:p];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
