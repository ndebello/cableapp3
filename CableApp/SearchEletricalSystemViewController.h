//
//  SearchEletricalSystemViewController.h
//  CableApp
//
//  Created by Welington Barichello on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchEletricalSystemViewController : UIViewController<NSURLConnectionDelegate>

@property (weak, nonatomic) IBOutlet UIButton *selApplication;
@property (weak, nonatomic) IBOutlet UIButton *selInst;

@end
