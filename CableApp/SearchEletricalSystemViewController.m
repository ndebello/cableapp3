//
//  SearchEletricalSystemViewController.m
//  CableApp
//
//  Created by Welington Barichello on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "SearchEletricalSystemViewController.h"
#import "SelectInslation1ViewController.h"
#import "DataSelected.h"

@interface SearchEletricalSystemViewController (){
    DataSelected * sharedData;
}

@end

@implementation SearchEletricalSystemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Cable Search";
    sharedData = [DataSelected sharedManager];
    
    // Do any additional setup after loading the view.
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    if (!sharedData.Inst1) {
        NSURL *url = [NSURL URLWithString:@"http://46.252.150.235:8085/api/installation1s.json"];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     UIAlertController * alert=   [UIAlertController
                                                   alertControllerWithTitle:@"Error"
                                                   message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                   preferredStyle:UIAlertControllerStyleAlert];
                     
                     UIAlertAction* yesButton = [UIAlertAction
                                                 actionWithTitle:@"Ok"
                                                 style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * action)
                                                 {
                                                     [self viewDidAppear:YES];
                                                     
                                                     
                                                 }];
                     
                     [alert addAction:yesButton];
                     
                     [self presentViewController:alert animated:YES completion:nil];
                 });
                 
                 //NSLog(@"Error,%@", [error localizedDescription]);
             }else
             {
                 sharedData.Inst1 = [NSMutableArray new];
                 [self ParsingJsonInstallation1:data];
             }
         }];
    }
    
}




-(void)ParsingJsonInstallation1:(NSData *)data{
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          options:kNilOptions
                          error:&error];
    
    for (NSDictionary * inst1 in json) {
        NSDictionary *p = @{
                            [[inst1 objectForKey:@"Installation1"] objectForKey:@"id"]: [[inst1 objectForKey:@"Installation1"] objectForKey:@"displayfield"]
                            };
        [sharedData.Inst1 addObject:p];
    }
    
}

@end
