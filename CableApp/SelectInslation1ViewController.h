//
//  SelectInslation1ViewController.h
//  CableApp
//
//  Created by Welington Barichello on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectInslation1ViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textFielApplication;

@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@end
