//
//  SelectInslation1ViewController.m
//  CableApp
//
//  Created by Welington Barichello on 15/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "SelectInslation1ViewController.h"
#import "SeInst2ViewController.h"
#import "DataSelected.h"

@interface SelectInslation1ViewController (){
    UIAlertController *alertController;
    DataSelected * sharedData;
}

@end

@implementation SelectInslation1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Cable Search";
    sharedData = [DataSelected sharedManager];
    if ([self.textFielApplication.text isEqualToString:@""]) {
        self.confirmButton.alpha = 0;
    }
    
    // Do any additional setup after loading the view.
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self createUIAlertController:sharedData.Inst1 TextField:textField TitleAlert:@"Select Installation " MessageAlert:@""];
    [self.view endEditing:YES];
}

-(void)createUIAlertController:(NSMutableArray *)arrayAlert TextField:(UITextField *)textField TitleAlert:(NSString *)titleAlert MessageAlert:(NSString *)messageAlert{
    
    alertController = [UIAlertController alertControllerWithTitle:titleAlert
                                                          message:messageAlert
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i= 0; i< [arrayAlert count]; i++) {
        //NSLog(@"%@",[arrayAlert[i] valueForKey:@"1"]);
        [alertController addAction:[UIAlertAction
                                    actionWithTitle:[arrayAlert[i] valueForKey:[NSString stringWithFormat:@"%d",i+1]]
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        sharedData.s1 = [NSString stringWithFormat:@"%d",i+1];
                                        textField.text = [arrayAlert[i] valueForKey:[NSString stringWithFormat:@"%d",i+1]];
                                        [UIView animateWithDuration:.5
                                                         animations:^{
                                                             self.confirmButton.alpha = 1;
                                                         }];
                                    }]];
    }
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Annulla", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
