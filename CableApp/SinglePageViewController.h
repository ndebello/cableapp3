//
//  SinglePageViewController.h
//  CableA
//
//  Created by Matteo Centro on 17/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SinglePageViewController : UIViewController
@property (nonatomic, assign) NSUInteger pageIndex;
@property (nonatomic, assign) BOOL isLast;
@property (nonatomic, strong) NSArray * currentPage;
@property (weak, nonatomic) IBOutlet UITableView * tableView;

- (void) hideKeyboard;

@end
