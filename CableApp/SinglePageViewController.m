//
//  SinglePageViewController.m
//  CableA
//
//  Created by Matteo Centro on 17/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "SinglePageViewController.h"
#import "ParameterTableViewCell.h"
#import "StartPageViewController.h"
#import "DataSelected.h"
#import "Calculator.h"

@interface SinglePageViewController ()<UITableViewDataSource, UITableViewDelegate>{
    UITapGestureRecognizer * singleTapR;
    DataSelected * sharedData;
}

@property (weak, nonatomic) IBOutlet UIImageView * arrowL;
@property (weak, nonatomic) IBOutlet UIImageView * arrowR;
@property (weak, nonatomic) IBOutlet UIButton * sectionCalc;

@end

@implementation SinglePageViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    sharedData = [DataSelected sharedManager]; // DEN
    
    UITapGestureRecognizer * gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [_tableView addGestureRecognizer:gestureRecognizer];
    
    // [self methodRequest]; // DEN
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
	
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog(@"Appear %ld...", self.pageIndex);
    
    // [self.tableView reloadData]; // DEB
    
    if (self.isLast)
    {
        self.arrowR.hidden = YES;
    }
    else
    {
        self.sectionCalc.hidden = YES;
        if (self.pageIndex == 0)
        {
            self.arrowL.hidden = YES;
        }
    }
}

- (IBAction) calcButtonPressed:(id)sender
{
    NSLog(@"Ora scatena il calcolo...%@", sharedData.sMetINT);
    
    if (sharedData.eDepth)
    {
        sharedData.sDepth = @"1";
    }
    if (sharedData.ePosition)
    {
        sharedData.sPosition = @"1";
    }
    
    if (sharedData.eSun)
    {
        sharedData.sSunExp = @"1";
    }
    if (sharedData.eNumCableC)
    {
        sharedData.sNumCableC = @"1";
    }
    if (sharedData.eNumCon)
    {
        sharedData.sNumCondu = @"1";
    }
    
    if (sharedData.eDistance)
    {
        sharedData.sDistance = @"1";
    }
    if (sharedData.eTrays)
    {
        sharedData.sNumTrays = @"1";
    }

    NSString * numCablesDen1 = [sharedData.numCablesDen stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString * numCircuitsDen1 = [sharedData.numCircuitsDen stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString * numLayersDen1 = [sharedData.numLayersDen stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"Calcola");
    if ([sharedData.numCoreDen isEqualToString:@"Single(B)"]) sharedData.numCoreDen = @"SingleB";
    if ([sharedData.numCoreDen isEqualToString:@"Multi(B)"]) sharedData.numCoreDen = @"MultiB";
    NSString * urlString = [NSString stringWithFormat:@"http://46.252.150.235:8085/api/cables/compute/inst1:%@/inst2:%@/inst3:%@/method:%@/numCores:%@/insulation:%@/conductor:%@/temperature:%@/resistivity:%@/volt:1/conductivity:%@/current:%@/sunExpo:%@/numConductPerCable:%@/distBetwCables:%@/numCablesConduit:%@/circuitPerConduit:%@/numLayers:%@/numTrays:%@/position:%@/depth:%@/tcs1:%@/tcs2:%@/tcs3:%@/lcs1:%@/lcs2:%@/cit:%@/crt:%@/currentType:%@/otherVoltage:%@/intensity:%@/activePower:%@/apparentPower:%@/mechanicalPower:%@/performance:%@/coseno:%@/voltageDrop:%@/voltageDropPercentage:%@/lineLength:%@/shortCircuitIntensity:%@/timeShootProtection:%@/circuitSeparated:%@/otherCoeff:%@.json"
                            , sharedData.s1, sharedData.s2, sharedData.s3, sharedData.methDen,   // methDen sarebbe in effetti ridondante, ma dato che prima sMetINT era un metodo (by WLF) è rimasto così
                            // [[sharedData.CS objectForKey:@"NumberCore"] objectForKey:@"id"],  // Questo era sbagliato, perchè era il dato che arrivava dal WebService per il cavo
                            sharedData.numCoreDen,                                               // numCoreDen è quello che c'è nel plist
                            [[sharedData.CS objectForKey:@"Insulation"] objectForKey:@"id"],
                            [[sharedData.CS objectForKey:@"Conductor"] objectForKey:@"id"],
                            sharedData.tempDen ? sharedData.tempDen : @"1",
                            sharedData.resDen ? sharedData.resDen : @"1",
                            sharedData.sCondu ? sharedData.sCondu : @"1",
                            sharedData.sCurI ? sharedData.sCurI : @"1", // Non più usato???
                            sharedData.sunExpoDen ? sharedData.sunExpoDen : @"1",
                            sharedData.sNumCondu ? sharedData.sNumCondu : @"1", // Mauco???
                            sharedData.distDen && ![sharedData.distDen isEqualToString:@""] ? sharedData.distDen : @"1",
                            numCablesDen1 ? numCablesDen1 : @"1",
                            numCircuitsDen1 ? numCircuitsDen1 : @"1",
                            numLayersDen1 ? numLayersDen1 : @"1",
                            sharedData.numTraysDen ? sharedData.numTraysDen : @"1",
                            sharedData.positionDen ? sharedData.positionDen : @"1",
                            sharedData.depthDen ? sharedData.depthDen : @"1",
                            [sharedData.CS objectForKey:@"tcsmcm"],
                            [sharedData.CS objectForKey:@"tcsmtp"],
                            [sharedData.CS objectForKey:@"tcsscmpt"],
                            [sharedData.CS objectForKey:@"lcssc"],
                            [sharedData.CS objectForKey:@"lcsmc"],
                            sharedData.citDen ? sharedData.citDen : @"1",
                            sharedData.crtDen ? sharedData.crtDen : @"1",
                            sharedData.currTypeDen ? sharedData.currTypeDen : @"1",
                            sharedData.voltageDen ? sharedData.voltageDen : @"1",
                            sharedData.intensDen1 && ![sharedData.intensDen1 isEqualToString:@""] ? sharedData.intensDen1 : @"1",
                            sharedData.intensDen2 && ![sharedData.intensDen2 isEqualToString:@""] ? sharedData.intensDen2 : @"1",
                            sharedData.intensDen3 && ![sharedData.intensDen3 isEqualToString:@""] ? sharedData.intensDen3 : @"1",
                            sharedData.mechDen && ![sharedData.mechDen isEqualToString:@""] ? sharedData.mechDen : sharedData.intensDen2, // Se non ho niente, mando via lo stesso valore di Active Power
                            sharedData.perfDen && ![sharedData.perfDen isEqualToString:@""] ? sharedData.perfDen : @"1",
                            sharedData.cosenoDen ? sharedData.cosenoDen : @"1",
                            sharedData.vdropDen && ![sharedData.vdropDen isEqualToString:@""] ? sharedData.vdropDen : @"0",
                            sharedData.vdrop1Den && ![sharedData.vdrop1Den isEqualToString:@""] ? sharedData.vdrop1Den : @"0",
                            sharedData.llengthDen && ![sharedData.llengthDen isEqualToString:@""] ? sharedData.llengthDen : @"1",
                            sharedData.shortCircDen ? sharedData.shortCircDen : @"0",
                            sharedData.timeShootDen ? sharedData.timeShootDen : @"0",
                            [sharedData dummyDen], // Calcolato su intensDen1 2 e 3
                            sharedData.ocDen ? sharedData.ocDen : @"1"
                            ];
    
    NSURL * url = [NSURL URLWithString:urlString];
    
    // url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]; // DEN
    NSLog(@"URL String %@", urlString);
    NSLog(@"URL %@", url);
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue * queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse * response, NSData * data, NSError * error)
     {
         if (error)
         {
             NSLog(@"Error,%@", [error localizedDescription]);
         }
         else
         {
             [self calcJsonParse:data];
         }
     }];
}

- (void) hideKeyboard
{
    [self.view endEditing:YES];
}

#pragma  mark TABLE DELEGATES
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.currentPage.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ParameterTableViewCell * cell = (ParameterTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"ParameterCell" forIndexPath:indexPath];
    NSDictionary * currentParam = [self.currentPage objectAtIndex:indexPath.row];
    
    cell.dictionary = currentParam;
    cell.controller = self;
    cell.row = indexPath.row;
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void) calcJsonParse:(NSData *)data
{
    NSError * error;
    NSDictionary * json = [NSJSONSerialization
                           JSONObjectWithData:data // 1
                           options:kNilOptions
                           error:&error];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if([json objectForKey:@"section"]){
            sharedData.calResult = json;
            [self performSegueWithIdentifier:@"resultCalc" sender:nil];
        }
        else
        {
            
            UIAlertController * alert =   [UIAlertController
                                           alertControllerWithTitle:@"Attention"
                                           message:@"All parameters must be entered"
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * yesButton = [UIAlertAction
                                         actionWithTitle:@"Yes"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             // Handel your yes please button action here
                                             
                                             
                                         }];
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    });
}

@end
