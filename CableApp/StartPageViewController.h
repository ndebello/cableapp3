//
//  StartPageViewController.h
//  CableApp
//
//  Created by Welington Barichello on 18/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

@interface StartPageViewController : UIViewController<UIPageViewControllerDataSource>

- (IBAction) startWalkthrough:(id)sender;
// - (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index;
- (void) changePage:(UIPageViewControllerNavigationDirection)direction;
@property (strong, nonatomic) UIPageViewController * pageViewController;
@property (strong, nonatomic) NSArray * pageTitles;
@property (strong, nonatomic) NSArray * pageImages;

// ArrayWithLabelTitle
@property (strong, nonatomic) NSArray * labelTitles1;
@property (strong, nonatomic) NSArray * labelTitles2;
@property (strong, nonatomic) NSArray * labelTitles3;
@property (strong, nonatomic) NSArray * labelTitles4;
@property (strong, nonatomic) NSArray * labelTitles5;
@property (strong, nonatomic) NSArray * labelTitles6;
@property (strong, nonatomic) NSArray * labelTitles7;
@property (strong, nonatomic) NSArray * labelTitles8;
@property (strong, nonatomic) NSArray * labelTitles9;
@property (strong, nonatomic) NSArray * labelTitles10;

// textFields

- (void) myInitWithArray:(NSArray*)myInitArray;

@end
