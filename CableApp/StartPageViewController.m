//
//  StartPageViewController.m
//  CableApp
//
//  Created by Welington Barichello on 18/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "StartPageViewController.h"
#import "Calculator.h"
#import "SinglePageViewController.h"
#import "DataSelected.h"

@interface StartPageViewController () {
    UITapGestureRecognizer * singleTapR;
    DataSelected * sharedData;
}
@property (nonatomic, retain) NSArray * pages;

@end

@implementation StartPageViewController
    
- (void) viewDidLoad
{
    NSString * path;
    
    [super viewDidLoad];
    sharedData = [DataSelected sharedManager];
    singleTapR =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTapR setNumberOfTapsRequired:1];
    
    self.navigationItem.title = @"Section Calc";
    
    path = [[NSBundle mainBundle] pathForResource:@"CalculationParameters" ofType:@"plist"];
    
    self.pages = [[NSArray alloc]initWithContentsOfFile:path];
    
    // [self methodRequest]; // DEN spostato molto prima
    
    _pageTitles = @[@"Param Input 1 ", @"Param Input 2", @"Param Input 3"];
    _labelTitles1 = @[@"Method", @"Voltage", @"Temperature"];
    _labelTitles2 = @[@"Num Cores", @"Current Int.", @"Resistivity"];
    _labelTitles3 = @[@"Insulation", @"Coseno", @"NumConduct"];
    _labelTitles4 = @[@"Conductor", @"Conductivity", @"NumCablesC"];
    _labelTitles5 = @[@"Current Type", @"Sun Expos.", @"Distance"];
    
    _labelTitles6 = @[@"TCS1", @"CircConduit"];
    _labelTitles7 = @[@"TCS2", @"Num Layers"];
    _labelTitles8 = @[@"TCS3", @"Num Trays"];
    _labelTitles9 = @[@"LCS1", @"Position"];
    _labelTitles10 = @[@"LCS2", @"Depth"];
    // _pageImages = @[@"page1.png", @"page2.png", @"page3.png", @"page4.png"];
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    SinglePageViewController * startingViewController = [self viewControllerAtIndex:0];
    [_pageViewController setViewControllers:@[startingViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // WLF
    //    PageContentViewController * startingViewController = [self viewControllerAtIndex:0];
    //    NSArray * viewControllers = @[startingViewController];
    //    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

// DEN Spostata la chiamata a methodRequest a CableDetailViewController per essere più sicuro che venga eseguita in tempo per avere Method e tutte le variabili riempite per i conti relativi alla contestualità dei campi
/*- (void) methodRequest
{
    if ([sharedData.Methods count] == 0 )
    {
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"http://46.252.150.235:8085/api/methods/index/installation1_id:%@/installation2_id:%@/installation3_id:%@/cable_id:%@.json", sharedData.s1, sharedData.s2, sharedData.s3, [sharedData.CS objectForKey:@"id"]]];
        NSLog(@"URL %@", url);
        NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
        NSOperationQueue * queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse * response, NSData * data, NSError * error)
         {
             if (error)
             {
                 NSLog(@"Error,%@", [error localizedDescription]);
             }
             else
             {
                 [self ParsingJsonmethodRequest:data];
             }
         }];
        
    }
}

- (void) ParsingJsonmethodRequest:(NSData *)data
{
    NSError * error;
    NSDictionary * json = [NSJSONSerialization
                           JSONObjectWithData:data // 1
                           options:kNilOptions
                           error:&error];
    
    for (NSDictionary * inst1 in json)
    {
        NSDictionary * method = [inst1 objectForKey:@"Method"];
        if ([method objectForKey:@"id"])
        {
            NSDictionary * p = @{
                                 [[inst1 objectForKey:@"Method"] objectForKey:@"id"]: [[inst1 objectForKey:@"Method"] objectForKey:@"code"]
                                 };
            [sharedData.Methods addObject:p];
        }
        else
        {
            NSLog(@"NESSUN METODO ARRIVA DAL WEBSERVICE");
        }
    }
}*/

- (void) singleTapping:(UIGestureRecognizer *)recognizer
{
    NSLog(@"image click");
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) startWalkthrough:(id)sender
{
    SinglePageViewController * startingViewController = [self viewControllerAtIndex:0];
    // WLF
    // PageContentViewController * startingViewController = [self viewControllerAtIndex:0];
    NSArray * viewControllers = @[startingViewController];
    
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}

- (SinglePageViewController *) viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pages count] == 0) || (index >= [self.pages count]))
        // WLF
        // if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count]))
    {
        return nil;
    }
    SinglePageViewController * spvc = [self.storyboard instantiateViewControllerWithIdentifier:@"SinglePageViewController"];
    spvc.pageIndex = index;
    if (index == (self.pages.count - 1))
    {
        spvc.isLast = YES;
    }
    spvc.currentPage = [self.pages objectAtIndex:index];
    return spvc;
}

// WLF
// - (PageContentViewController *) viewControllerAtIndex:(NSUInteger)index
// {
//
//    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count]))
//    {
//        return nil;
//    }
//    // self.navigationItem.title = _pageTitles[index];
//    // Create a new view controller and pass suitable data.
//    PageContentViewController * pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
//    [pageContentViewController.arrowR addGestureRecognizer:singleTapR];
//    pageContentViewController.imageFile = self.pageImages[index];
//    // labels
//    // pageContentViewController.titleText = self.labelTitles1[index];
//    pageContentViewController.titleText1 = self.labelTitles1[index];
//    pageContentViewController.titleText2 = self.labelTitles2[index];
//    pageContentViewController.titleText3 = self.labelTitles3[index];
//    pageContentViewController.titleText4 = self.labelTitles4[index];
//    pageContentViewController.titleText5 = self.labelTitles5[index];
//
//    if (index > 0)
//    {
//        pageContentViewController.titleText6 = self.labelTitles6[index - 1];
//        pageContentViewController.titleText7 = self.labelTitles7[index - 1];
//        pageContentViewController.titleText8 = self.labelTitles8[index - 1];
//        pageContentViewController.titleText9 = self.labelTitles9[index - 1];
//        pageContentViewController.titleText10 = self.labelTitles10[index - 1];
//
//    }
//
//    pageContentViewController.pageIndex = index;
//
//
//    return pageContentViewController;
// }


#pragma mark - Page View Controller Data Source

- (UIViewController *) pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    // WLF
    //    NSUInteger index = ((PageContentViewController *) viewController).pageIndex;
    NSUInteger index = ((SinglePageViewController *) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound))
    {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *) pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    // WLF
    //    NSUInteger index = ((PageContentViewController *) viewController).pageIndex;
    NSUInteger index = ((SinglePageViewController *) viewController).pageIndex;
    
    if (index == NSNotFound)
    {
        return nil;
    }
    
    index++;
    if (index == [self.pages count])
        // WLF
        // if (index == [self.pageTitles count])
    {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger) presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pages count];
    // WLF
    // return [self.pageTitles count];
}

- (NSInteger) presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (void) changePage:(UIPageViewControllerNavigationDirection)direction
{
    // WLF
    //    NSUInteger pageIndex = ((PageContentViewController *) [_pageViewController.viewControllers objectAtIndex:0]).pageIndex;
    NSUInteger pageIndex = ((SinglePageViewController *) [_pageViewController.viewControllers objectAtIndex:0]).pageIndex;
    
    if (direction == UIPageViewControllerNavigationDirectionForward)
    {
        pageIndex++;
    }
    else
    {
        pageIndex--;
    }
    
    //    PageContentViewController * viewController = [self viewControllerAtIndex:pageIndex];
    SinglePageViewController * viewController = [self viewControllerAtIndex:pageIndex];
    
    if (viewController == nil)
    {
        return;
    }
    
    [_pageViewController setViewControllers:@[viewController]
                                  direction:direction
                                   animated:YES
                                 completion:nil];
}

- (void) myInitWithArray:(NSArray*)myInitArray
{
    // NSString * path = [[NSBundle mainBundle] pathForResource:@"CalculationParameters1" ofType:@"plist"];
    
    // self.pages = [[NSArray alloc]initWithContentsOfFile:path];
    
    [self viewDidLoad];
}

@end
