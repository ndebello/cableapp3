//
//  UserInfoViewController.h
//  CableA
//
//  Created by Welington Barichello on 26/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menu;
@end
