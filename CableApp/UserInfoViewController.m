//
//  UserInfoViewController.m
//  CableA
//
//  Created by Welington Barichello on 26/02/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "UserInfoViewController.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface UserInfoViewController (){
    UIBarButtonItem *faceBtn;
}

@end

@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.menu.target = self.revealViewController;
    self.menu.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    self.navigationItem.title = @"User preferences";
    self.menu.image = [UIImage imageNamed:@"menu_icon"];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
