//
//  ViewController.h
//  CableApp
//
//  Created by Welington Barichello on 11/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *emailText;

@end

