//
//  ViewController.m
//  CableApp
//
//  Created by Welington Barichello on 11/01/16.
//  Copyright © 2016 Welington Barichello. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
  
}

@property (weak, nonatomic) IBOutlet UITextField *emailUser;
@property (weak, nonatomic) IBOutlet UITextField *userPsw;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //login
    if ([userDefaults boolForKey:@"registered"]) {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self performSegueWithIdentifier:@"loginOk" sender:self];
            });
        });
    } else {
        NSLog(@"no user is register");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
- (IBAction)login:(id)sender {
    //Rimuovere
//    [self performSegueWithIdentifier:@"loginOk" sender:self];
    //fino a qui
    if ([self validateEmail:_emailUser.text]) {
        if (![_userPsw.text isEqualToString:@""]) {
            NSLog(@"email:%@ psw:%@",_emailUser.text,_userPsw.text);
            [self loginRequest];
        }else{
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                //Background Thread
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [self callAlertMethod:@"Attenzione" Message:@"inserire password"];
                });
            });
        }
        
    }else{
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self callAlertMethod:@"Attenzione" Message:@"email inserita non valida."];
            });
        });
        
    }
    
}

-(void)loginRequest{
    NSString * post = [NSString stringWithFormat:@"{\"Customer\":{\"email\":\"%@\",\"password\":\"%@\"}}",_emailUser.text,_userPsw.text];
    NSError *error;
    
    NSData *data = [post dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&error];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://46.252.150.235:8085/api/customers/login.json"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:@"Error"
                                               message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction* yesButton = [UIAlertAction
                                             actionWithTitle:@"Ok"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action)
                                             {
                                                 [self viewDidAppear:YES];
                                             }];
                 
                 [alert addAction:yesButton];
                 [self presentViewController:alert animated:YES completion:nil];
             });
             
         }
         else
         {
             NSError* error;
             NSDictionary* json = [NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:kNilOptions
                                   error:&error];
             NSLog(@"%@",json);
             if ([[json objectForKey:@"status"] intValue] == 1) {
                 dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                     dispatch_async(dispatch_get_main_queue(), ^(void){
                         NSUserDefaults *defaults = [ NSUserDefaults standardUserDefaults];
                         [defaults setObject:_userPsw.text forKey:@"password"];
                         [defaults setObject:[[[json objectForKey:@"message"] objectForKey:@"Customer"] objectForKey:@"email"] forKey:@"email"];
                         [defaults setObject:[[[json objectForKey:@"message"] objectForKey:@"Customer"] objectForKey:@"country_id"] forKey:@"countryId"];
                         [defaults setObject:[[[json objectForKey:@"message"] objectForKey:@"Country"] objectForKey:@"name"] forKey:@"countryName"];
                         [defaults setObject:[[[json objectForKey:@"message"] objectForKey:@"Customer"] objectForKey:@"type"] forKey:@"typeUser"];
                         [defaults setObject:[[[json objectForKey:@"message"] objectForKey:@"Customer"] objectForKey:@"other"] forKey:@"typeUserOther"];
                         [defaults setObject:[[[json objectForKey:@"message"] objectForKey:@"Customer"] objectForKey:@"id"] forKey:@"id"];
                         [defaults setBool:YES forKey:@"registered"];
                         [defaults synchronize];
                         dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                             dispatch_async(dispatch_get_main_queue(), ^(void){
                                 [self performSegueWithIdentifier:@"loginOk" sender:self];
                             });
                         });
                         
                     });
                 });
                 
             }else{
                 dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                     dispatch_async(dispatch_get_main_queue(), ^(void){
                         [self callAlertMethod:@"Attenzione" Message:[NSString stringWithFormat:@"%@",[json objectForKey:@"message"]]];
                     });
                 });
                 
             }
         }
     }];
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}
-(void)callAlertMethod:(NSString *)title Message:(NSString *)message{
    UIAlertController *actionSheet= [UIAlertController
                                     alertControllerWithTitle:title
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionSheetButton1 = [UIAlertAction
                                         actionWithTitle:@"Okay"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             
                                         }];
    
    [actionSheet addAction:actionSheetButton1];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

@end
